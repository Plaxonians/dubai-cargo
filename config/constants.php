<?php
   
return [
    'app_url'=>  env('APP_URL'),
    'no_user_image' => 'images/users-image.png',
    'not_found_image' => 'images/not-found.png',
    'website_logo' => 'images/logo.png',
    'website_favicon_image' => 'images/logo.png',
    'website_name' => 'Dubai Cargo',
    'book_an_appointment_link' => 'contact',
    'form_type'=> [
                        'contact'=>1,
                        'quote'=>2,
                        'feedback'=>3,
                        'get_quote'=>4,
                       ],  
    'activity_item_type'  => [
                            'login'=>1,
                            'logout'=>2,
                            'banner'=>3,
                           ]    , 
    'meta_type'  => [
                            'about_us_home_content'=>1,
                            'about_us_client_review'=>13,
                            'about_us_company_profile'=>14,
                            'sustainability_page'=>15,
                            'service_residential_page'=>17,
                            'service_office_page'=>18,
                            'service_furniture_page'=>19,
                            'service_storage_page'=>20,
                            'service_frieghtcargo_page'=>24,
                            'social_media_link'=>21,
                            'banner_data'=>22,
                            'home_page_content'=>23,
                           ]    ,   
                    				   
]
  
?>