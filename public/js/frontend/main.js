// Auther by Mohd AJAM ANSARI
var ajax = function (ajax_url, method, postData, is_loader = false) {
  let data = {};
  $.ajax({
    type: "POST",
    url: ajax_url,
    dataType: "json",
    async: false,
    data: postData,
    beforeSend: function () {
      if (is_loader) {
        loader(true);
      }
    },
    complete: function () {
      if (is_loader) {
        loader(false);
      }
    },
    success: function (res) {
      data = res;
    },
    error: function (jqXHR, exception) {
      data.status = 0;
      data.msg = exception;
      console.log(jqXHR, "jqXHR");
      console.log(exception, "exception");
    },
  });
  return data;
};

var isEmpty = function (obj) {
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty
  // Is it empty?  Depends on your application.
  if (typeof obj !== "object") return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }

  return true;
};

var serialize_string_to_object = function (postData) {
  let object_data = new Object();
  let array_data = postData.split("&");
  let len = array_data.length;
  for (let i = 0; i < len; i++) {
    let final_array = array_data[i].split("=");
    let key_name = final_array[0];
    let value = final_array[1];
    object_data[key_name] = value;
  }
  return object_data;
};

var flash_message = function (message, errorType) {
  $(".flash_message").show();
  if (errorType == "success") {
    let html = '<div class="alert alert-success success_flash_message alert-block" align="center">';
    html += '<a href="javascript:" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>';
    html += "<strong>" + message + "</strong> ";
    html += "</div>";
    $(".flash_message").html(html);
    setTimeout(function () {
      $(".flash_message").fadeOut(3000, 0);
    }, 4000);
  }
  if (errorType == "error") {
    let html = '<div class="alert alert-error error_flash_message alert-block" align="center">';
    html += '<a href="javascript:" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>';
    html += "<strong>" + message + "</strong> ";
    html += "</div>";
    $(".flash_message").html(html);
    setTimeout(function () {
      $(".flash_message").fadeOut(3000, 0);
    }, 4000);
  }
};


$(document).ready(function () {


  setTimeout(function(){ 
        $(".flash_message").fadeOut(3000,0);
     }, 2000); 



    /// Custom Function ///

   

      $(".is_alphabet").keypress(function (e) {
        var keyCode = e.keyCode || e.which;

        $("#lblError").html("");

        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z ]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));

        return isValid;
      });

      $(".is_number").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          //display error message
          //$("#errmsg").html("Digits Only").show().fadeOut("slow");
          return false;
        }
      });


      $(".service_type").click(function (e) {

          let service_type = $(this).attr('service_type'); 
          $('.show_services').hide();
          $('.'+service_type).show();
          $('#service_type').val(service_type);
        
      });

      $('.start_date').on('focusin', function() {
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            minDate: new Date(),
            locale: {
              format: "DD-MM-YYYY",
            },
        });
    });




});
