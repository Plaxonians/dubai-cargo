$.validator.addMethod(
  "phonenu",
  function (value, element) {
    if (/^\d{3}-?\d{3}-?\d{4}$/g.test(value)) {
      return true;
    } else {
      return false;
    }
  },
  "Please enter your valid phone number"
);
$.validator.addMethod(
  "emailvalid",
  function (value, element) {
    if (/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)) {
      return true;
    } else {
      return false;
    }
  },
  "Please enter your valid email id"
);

var v = $("#proposal-form").validate({
  rules: {
    service_date: {
      required: true,
    },
    residential_city_from: {
      required: true,
    },
    residential_city_to: {
      required: true,
    },
    residential_area_from: {
      required: true,
    },
    residential_area_to: {
      required: true,
    },
    office_city_from: {
      required: true,
    },
    office_city_to: {
      required: true,
    },
    office_area_from: {
      required: true,
    },
    office_area_to: {
      required: true,
    },
    storage_city_from: {
      required: true,
    },
    storage_city_to: {
      required: true,
    },
    storage_area_from: {
      required: true,
    },
    storage_area_to: {
      required: true,
    },
    cargo_country_from: {
      required: true,
    },
    cargo_country_to: {
      required: true,
    },
    cargo_city_from: {
      required: true,
    },
    cargo_city_to: {
      required: true,
    },
    first_name: {
      required: true,
    },
    email: {
      required: true,
      email: true,
      emailvalid: true,
    },
    phone: {
      required: true,
      phonenu: true,
    },

    proposer: {
      required: true,
    },
    address_origin: {
      required: true,
    },
    address_destination: {
      required: true,
    },
    shipped_by: {
      required: true,
    },
    packing: {
      required: true,
    },
    tick_cover: {
      required: true,
    },
  },
  messages: {
    service_date: "Please enter your service date.",
    first_name: "Please enter your first name.",
    email: {
      required: "Please enter your email address.",
      email: "Please enter your email address.",
    },
    phone: "Please enter your phone no.",
    proposer: "Please enter your proposer.",
    address_origin: "Enter your address origin.",
    address_destination: "Enter your address  destination.",
    shipped_by: "Select your ship.",
    packing: "Select your packing.",
    tick_cover: "Select your tick cover.",
  },

  errorElement: "span",
  errorClass: "error",
  errorPlacement: function (error, element) {
    $("#quote_result").html("");
    $("#quote_result").hide("");
    document.getElementById("quote_result").style.display = "none";
    if (element.attr("name") == "shipped_by") {
      error.appendTo(".shipped_by_error");
    } else {
      error.insertBefore(element);
    }

    // console.log(error, element);
  },
  invalidHandler: function (form, validator) {
    if (!validator.numberOfInvalids()) return;
    $("html, body").animate(
      {
        scrollTop: $(validator.errorList[0].element).offset().top - $(validator.errorList[0].element).innerHeight() * 2,
      },
      300
    );
  },
});

export default v;
