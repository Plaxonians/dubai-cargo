$("#client_feedback").validate({
  rules: {
    service_type: {
      required: true,
    },
    feedback_date: {
      required: true,
    },
    first_name: {
      required: true,
    },
    email: {
      required: true,
      email: true,
    },
    phone: {
      required: true,
      phonenu: true,
    },
  },
  errorElement: "span",
  errorClass: "error",
  errorPlacement: function (error, element) {
    // console.log(error, element);
    error.insertBefore(element);
  },
  invalidHandler: function (form, validator) {
    if (!validator.numberOfInvalids()) return;
    $("html, body").animate(
      {
        scrollTop: $(validator.errorList[0].element).offset().top - $(validator.errorList[0].element).innerHeight() * 2,
      },
      300
    );
  },
  messages: {
    service_type: "Please select service.",
    feedback_date: "Please enter date.",
    first_name: "Please enter your name.",
    email: {
      required: "Please enter your email address.",
      email: "Please enter your email address.",
    },
    phone: "Please enter your phone no.",
  },
  submitHandler: function (e) {
    e.preventDefault();
    console.log("ssss");
  },
});

$(".client_feedback")
  .unbind("click")
  .click(function (form) {
    form.preventDefault();

    $("#feedback_result").html("");
    $("#feedback_result").hide("");

    if ($("#client_feedback").valid()) {
      let postData = new FormData(document.getElementById("client_feedback"));
      let ajax_url = base_url + "aboutus/client_feedback";
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: ajax_url,
        dataType: "json",
        async: false,
        data: postData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
          $(".client_feedback").html("SENDING...");
        },
        complete: function () {
          $(".client_feedback").html("SEND QUERY");
        },
        success: function (response) {
          if (response.status) {
            $("#client_feedback")[0].reset();
            $("#feedback_result").show("");
            $("#feedback_result").html('<div class="alert alert-success">' + response.msg + "</div>");
          } else {
            if (!isEmpty(response.msg)) {
              $("#feedback_result").show("");
              $("#feedback_result").html('<div class="alert alert-danger">' + response.msg + "</div>");
            }
          }
        },
        error: function (jqXHR, exception) {
          $("#feedback_result").show("");
          $("#feedback_result").html('<div class="alert alert-danger">' + exception + "</div>");
        },
      });
    }
  });

$(document).ready(function () {
  $(".daterangepicker").daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    autoApply: true,
    locale: {
      format: "DD-MM-YYYY",
    },
  });
});
