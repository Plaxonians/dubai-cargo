
$.validator.addMethod(
  "phonenu",
  function (value, element) {
    if (/^\d{3}-?\d{3}-?\d{4}$/g.test(value)) {
      return true;
    } else {
      return false;
    }
  },
  "Please enter your valid contact number"
);


$("#contact_us_form").validate({
  rules: {
    
    first_name: {
      required: true,
    },
    email: {
      required: true,
      email: true,
    },
    contact_number: {
      required: true,
      phonenu: true,
    },
    
  },
  errorElement: "span",
  errorClass: "error",
  errorPlacement: function (error, element) {
    // console.log(error, element);
    error.insertBefore(element);
  },
  invalidHandler: function (form, validator) {
    if (!validator.numberOfInvalids()) return;
    $("html, body").animate(
      {
        scrollTop: $(validator.errorList[0].element).offset().top - $(validator.errorList[0].element).innerHeight() * 2,
      },
      300
    );
  },
  messages:
  {

    first_name: "Please enter your name.",
    email: {
            required: "Please enter your email address.",
            email: "Please enter your email address.",
           },
    contact_number: "Please enter your contact number.",
    
  },
  submitHandler: function(e){

       e.preventDefault();
       console.log('ssss');

   },
});

