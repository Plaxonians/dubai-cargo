import v from "./validate.js";

(function () {
  let step = 1;
  let prevStep = 1;
  let currentStep = 1;
  let stepBars = $(".progressbar-dots");
  let tabs = $(".tab-pane");
  let formBottomSpacing = 100;

  // window.onresize = function () {
  //   if (this.innerWidth <= 768) {
  //     formBottomSpacing = 200;
  //   }
  // };

  // handle scroll to top
  function handleScrollToTop(ele, delay = 500) {
    $("html, body").animate(
      {
        scrollTop: $(ele).offset().top - 25,
      },
      delay
    );
  }

  // handle form margin
  function calcFormMargin() {
    let arr = [];
    arr = window.location.pathname.split("/");
    let page = arr[arr.length - 1];

    if (window.innerWidth <= 480) {
      formBottomSpacing = 50;
    }

    let top =
      $(".floating-form").innerHeight() +
      parseInt($(".contact-form .floating-form").css("top")) +
      formBottomSpacing -
      $(".contact-form").innerHeight();

    if (page == "get-a-quote") {
      return top;
    }

    return top;
  }

  $(".contact-form").css({
    marginBottom: calcFormMargin(),
  });

  // handle next btn click
  const handleNext = function () {
    if (v.form()) {
      ++step;
      prevStep = step - 1;
      currentStep = prevStep + 1;

      // active progress
      stepBars.eq(currentStep - 1).addClass("active");

      // active step
      $(`#step-${prevStep}`).removeClass("active").fadeOut(100);
      $(`#step-${currentStep}`).addClass("active").fadeIn(100);

      // scroll to top
      handleScrollToTop(`.progress-wrap`);

      let timer = setInterval(() => {
        $(".contact-form").css({
          marginBottom: calcFormMargin(),
        });
        clearInterval(timer);
      }, 200);
    }
  };

  // handle prev btn click
  const handlePrevBtn = function () {
    --currentStep;
    prevStep = currentStep - 1;
    step = prevStep;

    if (step === 0 || prevStep === 0 || currentStep === 0) {
      step = 1;
      currentStep = 1;
    }

    // active progress
    stepBars.removeClass("active");
    Array.from(stepBars).forEach((ele, index) => {
      if (index <= prevStep) {
        $(ele).addClass("active");
      }
    });

    // active step
    $(`.tab-pane`).removeClass("active").fadeOut(100);
    $(`#step-${currentStep}`).addClass("active").fadeIn(100);

    // scroll to top
    handleScrollToTop(`.progress-wrap`);

    let timer = setInterval(() => {
      $(".contact-form").css({
        marginBottom: calcFormMargin(),
      });
      clearInterval(timer);
    }, 200);
  };

  // handle click on progress step list
  const handleProgressListBtn = function () {
    if (v.form()) {
      const listIndex = $(this).index() + 1;
      step = listIndex - 1;
      prevStep = step;
      currentStep = listIndex;

      // active progress
      stepBars.removeClass("active");
      const listArr = Array.from(stepBars);
      listArr.forEach((ele, index) => {
        if (index + 1 <= currentStep) {
          $(ele).addClass("active");
        }
      });

      // active step
      const tabArr = Array.from(tabs);
      tabs.removeClass("active").fadeOut(100);
      tabArr.forEach((ele, index) => {
        if (index + 1 === currentStep) {
          $(ele).addClass("active").fadeIn(100);
        }
      });

      // scroll to top
      handleScrollToTop(`.progress-wrap`);

      let timer = setInterval(() => {
        $(".contact-form").css({
          marginBottom: calcFormMargin(),
        });
        clearInterval(timer);
      }, 200);
    }
  };

  // handle submit
  const handleSubmit = function () {
    if (v.form()) {
      $("body").children().not("script").hide();
      $("#loader").show();
      let timer = setInterval(() => {
        $(".contact-form").css({
          marginBottom: calcFormMargin(),
        });
        clearInterval(timer);
      }, 200);
      return false;
    }
  };

  $(".next-btn").click(handleNext);
  $(".prev-btn").click(handlePrevBtn);
  // stepBars.click(handleProgressListBtn);
  //$(".submit-btn").click(handleSubmit);

  $(".quote_form_submit")
    .unbind("click")
    .click(function (form) {
      form.preventDefault();
      


      if (v.form()) {

        let postData = new FormData(document.getElementById("proposal-form"));
        let ajax_url = base_url + "quote_form_save";
        $.ajax({
          headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
          },
          type: "POST",
          url: ajax_url,
          dataType: "json",
          async: false,
          data: postData,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend: function () {
            $(".quote_form_submit").html("Requesting...");
          },
          complete: function () {
            $(".quote_form_submit").html("Request My Free Estimate");
          },
          success: function (response) {
            if (response.status) {
              $('meta[name="csrf-token"]').attr("content", response.csrf_token);
              $("#proposal-form")[0].reset();
              $("#quote_result").html('<div class="alert alert-success">' + response.msg + "</div>");
              $("#quote_result").show("");
              // $(".prev-btn2").trigger("click");
              // $(".prev-btn2").trigger("click");
              // $(".prev-btn2").trigger("click");
              step = 1;
              prevStep = 1;
              currentStep = 1;
              stepBars.removeClass("active");
              Array.from(stepBars).forEach((ele, index) => {
                if (index <= prevStep) {
                  $(ele).addClass("active");
                }
              });

              // active step
              $(`.tab-pane`).removeClass("active").fadeOut(100);
              $(`#step-${currentStep}`).addClass("active").fadeIn(100);

              handleScrollToTop(`.progress-wrap`);
            } else {
              if (!isEmpty(response.msg)) {
                $("#quote_result").html('<div class="alert alert-danger">' + response.msg + "</div>");
              }
            }
            let timer = setInterval(() => {
              $(".contact-form").css({
                marginBottom: calcFormMargin(),
              });
              clearInterval(timer);
            }, 200);
          },
          error: function (jqXHR, exception) {
            $("#quote_result").html('<div class="alert alert-danger">' + exception + "</div>");
          },
        });
      } else {
        console.log("else ");
        // $(".service_date_error").show();
        // $("#quote_date").focusin();
      }
    });
})();
