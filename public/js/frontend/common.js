AOS.init();

$(document).ready(function () {
  const option = {
    slidesToShow: 1,
    arrows: true,
    // prevArrow: `<button type='button' class='slick-prev pull-left'><img src="./assets/images/left.svg
    // "/></button>`,
    // nextArrow: `<button type='button' class='slick-next pull-right'><img src="./assets/images/right.svg
    // "/></button>`
  };
  $(".testimonial").slick(option);

  $(".select2-without-search").select2({
    minimumResultsForSearch: -1,
  });

  if ($(".scroll-to").length) {
    window.scrollBy({
      top: $(".scroll-to").position().top - 50,
      left: 100,
      behavior: "smooth",
    });
  }
});
