$(".daterangepicker").daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  autoApply: true,
  minDate: new Date(),
  locale: {
    format: "DD-MM-YYYY",
  },
});
