import v from "@js/frontend/home/validate.js";

let step = 1;
let prevStep = 1;
let currentStep = 1;
let stepBars = $(".progressbar-dots");
let tabs = $(".tab-pane");

// handle scroll to top
function handleScrollToTop(ele, delay = 500) {
  $("html, body").animate(
    {
      scrollTop: $(ele).offset().top - 25,
    },
    delay
  );
}

// handle next btn click
const handleNext = function () {
  if (v.form()) {
    ++step;
    prevStep = step - 1;
    currentStep = prevStep + 1;

    // active progress
    stepBars.eq(currentStep - 1).addClass("active");

    // active step
    $(`#step-${prevStep}`).removeClass("active").fadeOut(100);
    $(`#step-${currentStep}`).addClass("active").fadeIn(100);

    // scroll to top
    handleScrollToTop(`.progress-wrap`);

    setInterval(() => {
      $(".contact-form").css({ marginBottom: $(".floating-form").innerHeight() - $(".contact-form").innerHeight() + 325 });
    }, 100);
  }
};

// handle prev btn click
const handlePrevBtn = function () {
  --currentStep;
  prevStep = currentStep - 1;
  step = prevStep;

  if (step === 0 || prevStep === 0 || currentStep === 0) {
    step = 1;
    currentStep = 1;
  }

  // active progress
  stepBars.removeClass("active");
  Array.from(stepBars).forEach((ele, index) => {
    if (index <= prevStep) {
      $(ele).addClass("active");
    }
  });

  // active step
  $(`.tab-pane`).removeClass("active").fadeOut(100);
  $(`#step-${currentStep}`).addClass("active").fadeIn(100);

  // scroll to top
  handleScrollToTop(`.progress-wrap`);

  setInterval(() => {
    $(".contact-form").css({ marginBottom: $(".floating-form").innerHeight() - $(".contact-form").innerHeight() + 325 });
  }, 100);
};

// handle click on progress step list
const handleProgressListBtn = function () {
  if (v.form()) {
    const listIndex = $(this).index() + 1;
    step = listIndex - 1;
    prevStep = step;
    currentStep = listIndex;

    // active progress
    stepBars.removeClass("active");
    const listArr = Array.from(stepBars);
    listArr.forEach((ele, index) => {
      if (index + 1 <= currentStep) {
        $(ele).addClass("active");
      }
    });

    // active step
    const tabArr = Array.from(tabs);
    tabs.removeClass("active").fadeOut(100);
    tabArr.forEach((ele, index) => {
      if (index + 1 === currentStep) {
        $(ele).addClass("active").fadeIn(100);
      }
    });

    // scroll to top
    handleScrollToTop(`.progress-wrap`);

    setInterval(() => {
      $(".contact-form").css({ marginBottom: $(".floating-form").innerHeight() - $(".contact-form").innerHeight() + 325 });
    }, 100);
  }
};

// handle submit
const handleSubmit = function () {
  if (v.form()) {
    $("body").children().not("script").hide();
    $("#loader").show();
    setTimeout(function () {
      $("body").html("<h2>Your message was sent successfully. Thanks! We'll be in touch as soon as we can, which is usually like lightning.</h2>");
    }, 1000);
    return false;
  }
};

$(".next-btn").click(handleNext);
$(".prev-btn").click(handlePrevBtn);
stepBars.click(handleProgressListBtn);
$(".submit-btn").click(handleSubmit);
