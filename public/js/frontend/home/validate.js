var v = $("#proposal-form").validate({
  rules: {
    proposer: {
      required: true,
    },
    shipped: {
      required: true,
    },
    sofaNo: {
      required: true,
    },
    sofaCost: {
      required: true,
    },
  },

  errorElement: "span",
  errorClass: "error",
  errorPlacement: function (error, element) {
    console.log(error, element);
    error.insertBefore(element);
  },
  invalidHandler: function (form, validator) {
    if (!validator.numberOfInvalids()) return;
    $("html, body").animate(
      {
        scrollTop: $(validator.errorList[0].element).offset().top - $(validator.errorList[0].element).innerHeight() * 2,
      },
      300
    );
  },
});

export default v;
