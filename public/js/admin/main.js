// Auther by Mohd AJAM ANSARI

var isEmpty = function (obj) {
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty
  // Is it empty?  Depends on your application.
  if (typeof obj !== "object") return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }

  return true;
};

var serialize_string_to_object = function (postData) {
  let object_data = new Object();
  let array_data = postData.split("&");
  let len = array_data.length;
  for (let i = 0; i < len; i++) {
    let final_array = array_data[i].split("=");
    let key_name = final_array[0];
    let value = final_array[1];
    object_data[key_name] = value;
  }
  return object_data;
};



var loader = function (show) {
  if (show) {
    $("#ajax_loader").removeClass("d-none");
    $("#ajax_loader").show();
  } else {
    $("#ajax_loader").addClass("d-none");
    $("#ajax_loader").hide();
  }
};
var flash_message = function (message, errorType) {
  $(".flash_message").show();
  if (errorType == "success") {
    let html = '<div class="alert alert-success success_flash_message alert-block" align="center">';
    html += '<a href="javascript:" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>';
    html += "<strong>" + message + "</strong> ";
    html += "</div>";
    $(".flash_message").html(html);
    setTimeout(function () {
      $(".flash_message").fadeOut(3000, 0);
    }, 4000);
  }
  if (errorType == "error") {
    let html = '<div class="alert alert-error error_flash_message alert-block" align="center">';
    html += '<a href="javascript:" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>';
    html += "<strong>" + message + "</strong> ";
    html += "</div>";
    $(".flash_message").html(html);
    setTimeout(function () {
      $(".flash_message").fadeOut(3000, 0);
    }, 4000);
  }
};

var countWords = function (str = "") {
  if (str != "") {
    str = str.replace(/(^\s*)|(\s*$)/gi, "");
    str = str.replace(/[ ]{2,}/gi, " ");
    str = str.replace(/\n /, "\n");
    return str.split(" ").length;
  } else {
    return 0;
  }
};

var ajax = function (ajax_url, method, postData, is_loader = false) {
  let data = {};
  $.ajax({
    type: "POST",
    url: ajax_url,
    dataType: "json",
    async: false,
    data: postData,
    beforeSend: function () {
      if (is_loader) {
        loader(true);
      }
    },
    complete: function () {
      if (is_loader) {
        loader(false);
      }
    },
    success: function (res) {
      data = res;
    },
    error: function (jqXHR, exception) {
      data.status = 0;
      data.msg = exception;
      console.log(jqXHR, "jqXHR");
      console.log(exception, "exception");
    },
  });
  return data;
};

var state_list = function (country_id, html_id, select = 0) {
  if (country_id) {
    let option_html = '<option value="0">Select State  </option>';
    let postData = { country_id: country_id };
    let ajax_url = "events/state_list";
    $.ajax({
      url: base_url + ajax_url,
      type: "POST",
      data: postData,
      dataType: "json",
      async: false,
      beforeSend: function () {
        loader(true);
      },
      complete: function () {
        loader(false);
      },
      success: function (data) {
        //var data=JSON.parse(data);
        if (data.response) {
          $.each(data.states, function (key, value) {
            if (value.id == select) {
              option_html += '<option value="' + value.id + '" selected="selected">' + value.name + "</option>";
            } else {
              option_html += '<option value="' + value.id + '">' + value.name + "</option>";
            }
          });
          $(html_id).html(option_html);
        } else {
          $(html_id).html(option_html);
        }
      },
    });
  }
};

var city_list = function (state_id, html_id, select = 0) {
  if (state_id) {
    let option_html = '<option value="0">Select City  </option>';
    let postData = { state_id: state_id };
    let ajax_url = "events/city_list";
    $.ajax({
      url: base_url + ajax_url,
      type: "POST",
      data: postData,
      dataType: "json",
      async: false,
      beforeSend: function () {
        loader(true);
      },
      complete: function () {
        loader(false);
      },
      success: function (data) {
        //var data=JSON.parse(data);
        if (data.response) {
          $.each(data.cities, function (key, value) {
            if (value.id == select) {
              option_html += '<option value="' + value.id + '" selected="selected">' + value.name + "</option>";
            } else {
              option_html += '<option value="' + value.id + '">' + value.name + "</option>";
            }
          });
          $(html_id).html(option_html);
        } else {
          $(html_id).html(option_html);
        }
      },
    });
  }
};

var add_more_remove = function () {
    $(".add_more_remove").unbind('click').click(function() {
        

            let c = $(this).attr('counter');
            let page_gallery_id = $(this).attr('page_gallery_id');
            let banner_id = $(this).attr('banner_id');

            if(page_gallery_id && page_gallery_id!=undefined ||  banner_id && banner_id!=undefined) {
                      Swal.fire({
                            title: 'Are you sure to remove ?',
                            text: "",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Remove'
                        }).then((result) => {
                            if (result.value) {

                                window.location.href = $(this).attr('del_url');
                            }
                        });

            } else {

                $(".remove_div"+c).remove();

            }
            

    });
}


 var add_more_red_driver = function () {


$('#add_more_red_driver').unbind('click').click(function() {

    let limit = 4;
    let counter = $('.remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Banners ', 'error');
    } else {

    let add_more_html = '';

 

add_more_html+= '<div  class="remove_div remove_div'+counter+'" >';
  add_more_html+='<div class="float-left">';
     add_more_html+='<button type="button" class="add_more_remove " page_gallery_id="" id="remove_banner'+counter+'"  counter="'+counter+'"><i class="fa fa-times"></i></button>';
    add_more_html+='</div>';
    add_more_html+='<div class="form-group error_wrapper div_'+counter+'">';
        add_more_html+='<label for="name">Banner Title *</label>';
        add_more_html+='<input class="form-control is_alphabet validate[required]"  type="text" counter="'+counter+'" name="page_gallery[title]['+counter+']" placeholder=" Title" >';
    add_more_html+='</div>';

    add_more_html+='<div class="form-group error_wrapper">';
        add_more_html+='<label for="name">Banner Details *</label>';
        add_more_html+='<textarea class="form-control validate[required] " placeholder="Details" counter="'+counter+'" name="page_gallery[details]['+counter+']"></textarea>';
    add_more_html+='</div>';

    add_more_html+='<input type="hidden" name="page_gallery[banner_id]['+counter+']" id="page_gallery_id'+counter+'" >';

 

    add_more_html+='<div class="form-group error_wrapper div_'+counter+'">';
        add_more_html+='<label for="name">Banner Image *</label> <br/>';
        add_more_html+='<input class="validate[required]"  type="file" counter="'+counter+'" name="page_gallery[image]['+counter+']" accept="image/*" >';
    add_more_html+='</div>';

add_more_html+= '</div>';


    $("#add_more_result").append(add_more_html);
    add_more_remove();
    counter++;
   }

  });


}

 var add_more_red_banner = function () {


$('#add_more_red_banner').unbind('click').click(function() {

    let limit = 10 ;
    let counter = $('.remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Gallery images. ', 'error');
    } else {

    let add_more_html = '';

 

add_more_html+= '<div  class="remove_div remove_div'+counter+'" >';
  add_more_html+='<div class="float-right">';
     add_more_html+='<button type="button" class="add_more_remove " banner_id="" id="remove_banner'+counter+'"  counter="'+counter+'"><i class="fa fa-times"></i></button>';
    add_more_html+='</div>';

    add_more_html+='<br/>';

    

    add_more_html+='<div class="form-group error_wrapper div_'+counter+'">';
        add_more_html+='<label for="name"> Image </label> <br/>';
        add_more_html+='<p>Note : Image dimension should be within min 752x752 pixels.</p>';
        add_more_html+='<input class="gallery_image validate[required,funcCall[validate_gallery_image]]"  type="file" id="gallery_image'+counter+'" counter="'+counter+'" name="gallery[gallery_image]['+counter+']" accept="image/*" >';
    add_more_html+='</div>';

add_more_html+= '</div>';


    $("#add_more_result").append(add_more_html);
    add_more_remove();
    counter++;
   }

  });


}


function validate_gallery_image(field, rules, i, options){
      
      

      let val = field.val(); 
      let counter = field.attr('counter'); 
      let gal_id = 'gallery_image'+counter; 

      console.log(val,'val'); 
      console.log(counter,'counter'); 
      console.log(gal_id,'gal_id'); 

      if(val!='')
      {
          let min_width = 275;
          let min_height = 275;
          let max_width = 275;
          let max_height = 275;

        //Get reference of FileUpload.
          let fileUpload = document.getElementById("gallery_image"+counter);
         console.log(fileUpload,'fileUpload');
       
              //Check whether HTML5 is supported.
              if (typeof (fileUpload.files) != "undefined") {
                  //Initiate the FileReader object.
                  let reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      let image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = this.height;
                          var width = this.width;
                           console.log(height,'height');
                           console.log(width,'width');

                          //if (height >= min_height && height <= max_height && width >=min_width && width <=max_width) {
                          if ( height >= min_height  && width >=min_width ) 
                          {
                            console.log('true')
                              return true;
                          } else {
                              
                              return  "Invalid image dimension."; 
                          }
                          
                      };
       
                  }
              }
        
      }
      else
      {
        return  true; 
      }  
      

      
       
    }




$(document).ready(function () {

  add_more_red_driver() ;
  add_more_remove();
  add_more_red_banner();
  $(".validate_form").validationEngine();

  window._token = $('meta[name="csrf-token"]').attr('content')

  ClassicEditor.create(document.querySelector('.ckeditor'))

  moment.updateLocale('en', {
    week: {dow: 1} // Monday is the first day of the week
  })

  tinymce.init({
    selector: ".tinymce_editor",
    height: 250,
    menubar: false,
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table paste code help wordcount",
    ],
    toolbar:
      "undo redo | formatselect | " +
      "bold italic backcolor | alignleft aligncenter " +
      "alignright alignjustify | bullist numlist outdent indent | " +
      "removeformat",
    //content_css: "//www.tiny.cloud/css/codepen.min.css"
  });
 
tinymce.init({
  selector: '.blog_tinymce_editor',
  plugins: [
        'image code',
        "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table paste code help wordcount",
          ],
  toolbar: 'undo redo | link image | code'+
  "bold italic backcolor | alignleft aligncenter " +
      "alignright alignjustify | bullist numlist outdent indent | " +
      "removeformat",
  /* enable title field in the Image dialog*/
  image_title: true,
  /* enable automatic uploads of images represented by blob or data URIs*/
  automatic_uploads: true,
  /*
    URL of our upload handler (for more details check: https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_url)
    images_upload_url: 'postAcceptor.php',
    here we add custom filepicker only to Image dialog
  */
  file_picker_types: 'image',
  /* and here's our custom image picker*/
  file_picker_callback: function (cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    /*
      Note: In modern browsers input[type="file"] is functional without
      even adding it to the DOM, but that might not be the case in some older
      or quirky browsers like IE, so you might want to add it to the DOM
      just in case, and visually hide it. And do not forget do remove it
      once you do not need it anymore.
    */

    input.onchange = function () {
      var file = this.files[0];

      var reader = new FileReader();
      reader.onload = function () {
        /*
          Note: Now we need to register the blob in TinyMCEs image blob
          registry. In the next release this part hopefully won't be
          necessary, as we are looking to handle it internally.
        */
        var id = 'blobid' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        /* call the callback and populate the Title field with the file name */
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };

    input.click();
  },
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});




   setTimeout(function(){ 
        $(".flash_message").hide(1000,0);
     }, 2000); 


  $('.date').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'en'
  })

  $('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    locale: 'en',
    sideBySide: true
  })

  $('.timepicker').datetimepicker({
    format: 'HH:mm:ss'
  })

  $('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
  })
  $('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
  })

  $('.select2').select2()

  $('.treeview').each(function () {
    var shouldExpand = false
    $(this).find('li').each(function () {
      if ($(this).hasClass('active')) {
        shouldExpand = true
      }
    })
    if (shouldExpand) {
      $(this).addClass('active')
    }
  })


  /// Custom Function ///

  $(".is_alphabet").keypress(function (e) {
    var keyCode = e.keyCode || e.which;

    $("#lblError").html("");

    //Regex for Valid Characters i.e. Alphabets.
    var regex = /^[A-Za-z ]+$/;

    //Validate TextBox value against the Regex.
    var isValid = regex.test(String.fromCharCode(keyCode));

    return isValid;
  });

  $(".is_number").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
      //$("#errmsg").html("Digits Only").show().fadeOut("slow");
      return false;
    }
  });


  $(".blog_delete").unbind('click').click(function() {
    let delete_blog_url = $(this).attr('delete_blog_url');


    Swal.fire({
              title: 'Are you sure to delete ?',
              text: "",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
          }).then((result) => {
              if (result.value) {
                window.location.href = delete_blog_url;
                  
              }
          });
  });


   $(".gallery_delete").unbind('click').click(function() {
    let delete_gallery_url = $(this).attr('delete_gallery_url');


    Swal.fire({
              title: 'Are you sure to delete ?',
              text: "",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
          }).then((result) => {
              if (result.value) {
                window.location.href = delete_gallery_url;
                  
              }
          });
  });

  /// Custom Function ///


  $('.add_more_profile_points').unbind('click').click(function() {

    let limit = 10;
    let counter = $('.profile_point_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Points ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group profile_point_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Profile Point*</label>';
        add_more_html+='<button type="button" class="btn btn-danger add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="profile_points[]" class=" validate[required]  form-control" value="">';
    add_more_html+='</div>';

     $("#add_more_profile_points_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

  $('.add_more_points_section_two').unbind('click').click(function() {

    let limit = 5;
    let counter = $('.point_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Point ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group point_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Point*</label>';
        add_more_html+='<button type="button" class="btn btn-danger  add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="section_three_points[]" class="validate[required]  form-control" value="">';
    add_more_html+='</div>';

     $("#add_more_points_section_two_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

  $('.section_two_feature').unbind('click').click(function() {

    let limit = 8;
    let counter = $('.feature_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Features ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group feature_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Feature*</label>';
        add_more_html+='<button type="button" class="btn btn-danger   add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="features[]" class="validate[required] form-control" value="">';
    add_more_html+='</div>';

     $("#section_two_feature_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

  $('.section_two_benefit').unbind('click').click(function() {

    let limit = 10;
    let counter = $('.benefit_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Benefits ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group benefit_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Benefit*</label>';
        add_more_html+='<button type="button" class="btn btn-danger   add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="section_three_benefits[]" class="validate[required] form-control" value="">';
    add_more_html+='</div>';

     $("#section_two_feature_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

  $('.add_more_services_section_two').unbind('click').click(function() {

    let limit = 16;
    let counter = $('.service_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Services ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group service_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Service*</label>';
        add_more_html+='<button type="button" class="btn btn-danger   add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="section_two_services[]" class="validate[required] form-control" value="">';
    add_more_html+='</div>';

     $("#add_more_services_section_two_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

  $('.add_more_section_third_features').unbind('click').click(function() {

    let limit = 12;
    let counter = $('.feature_remove_div').length;
    counter = counter == 0 ? 1 : counter+1 ;
    if (counter > limit)  {
        flash_message('You can add ony '+limit+' Features ', 'error');
    } else {


    let add_more_html = '';

    add_more_html+='<div class="form-group feature_remove_div remove_div'+counter+'" >';
        add_more_html+='<label for="name"> Feature*</label>';
        add_more_html+='<button type="button" class="btn btn-danger   add_more_remove float-right" counter="'+counter+'"> Remove</button>';
        add_more_html+='<input type="text"  name="section_third_features[]" class="validate[required] form-control" value="">';
    add_more_html+='</div>';

     $("#add_more_section_third_features_result").append(add_more_html);
     add_more_remove();
    counter++;

   }


  });

});
