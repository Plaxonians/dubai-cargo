<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Route::redirect('/', '/login');


 Route::resource('/', 'SiteController');
 Route::get('thankyou', 'SiteController@thank_you');
 Route::post('quote_form_save', 'SiteController@quote_save');
 Route::get('aboutus/profile', 'AboutusController@profile');
 Route::get('aboutus/clientreview', 'AboutusController@clientreview');
 Route::get('aboutus/gallery', 'AboutusController@gallery');
 Route::post('aboutus/client_feedback', 'AboutusController@client_feedback_save');

 Route::get('contact', 'ContactController@index');
 Route::get('contact/address/', 'ContactController@address');
 Route::post('contact/save', 'ContactController@save');
 Route::get('blogs', 'BlogController@index');
 Route::get('blogs/detail/{blog_url}', 'BlogController@detail');

 Route::get('services/{service_url}', 'ServiceController@detail');
 Route::get('get-a-quote', 'QuoteController@index');


////// Admin Route Start  ////////////

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    /////////  Banner Module ////
    Route::get('banner', 'BannerController@index');
    Route::post('banner/update', 'BannerController@update');
    Route::get('banner/delete/{id}', 'BannerController@delete');

    /////////  Home Page Module ////
    Route::get('homepage', 'HomePageController@index');
    Route::post('homepage/update', 'HomePageController@update');

    Route::get('homepage/addclient', 'HomePageController@addclient');
    Route::get('homepage/editclient/{id}', 'HomePageController@editclient');
    Route::post('homepage/updateclient', 'HomePageController@updateclient');

    /////////  Testimonial Module ////
    Route::get('testimonial', 'TestimonialController@index');
    Route::get('testimonial/add/', 'TestimonialController@add');
    Route::get('testimonial/edit/{id}', 'TestimonialController@edit');
    Route::post('testimonial/update', 'TestimonialController@update');
    Route::get('testimonial/delete/{id}', 'TestimonialController@delete');


    /////////  About Us Module ////
   
    Route::get('about/companyprofile', 'AboutController@company_profile');
    Route::post('about/updatecompanyprofile', 'AboutController@update_company_profile');

    Route::get('about/clientreview', 'AboutController@client_review');
    Route::post('about/update_client_review', 'AboutController@update_client_review');

     /////////  Contact Us Module ////
    Route::get('contactus', 'ContactController@index');
    Route::post('contactus/update', 'ContactController@update');

    /////////  Services Module ////
    Route::get('services/residential', 'ServicesController@residential');
    Route::post('services/updateresidential', 'ServicesController@updateresidential');

    Route::get('services/office', 'ServicesController@office');
    Route::post('services/updateoffice', 'ServicesController@updateoffice');

    Route::get('services/furniture', 'ServicesController@furniture');
    Route::post('services/updatefurniture', 'ServicesController@updatefurniture');

    Route::get('services/storage', 'ServicesController@storage');
    Route::post('services/updatestorage', 'ServicesController@updatestorage');

    Route::get('services/frieghtcargo', 'ServicesController@frieghtcargo');
    Route::post('services/updatefrieghtcargo', 'ServicesController@updatefrieghtcargo');

  

    /////////  Form Module ////
    Route::get('form/contact', 'FormController@contact');
    Route::get('form/contact/export', 'FormController@contact_export');
    Route::get('form/quote', 'FormController@quote');
    Route::get('form/quoteview/{id}', 'FormController@quote_view');
    Route::get('form/quote/export', 'FormController@quote_export');
    Route::get('form/feedback', 'FormController@feedback');
    Route::get('form/feedback/export', 'FormController@feedback_export');
    Route::get('form/getquote', 'FormController@get_quote');

    ////////  Blog Form Module ////
    Route::get('blog/', 'BlogController@index');
    Route::get('blog/add', 'BlogController@add');
    Route::post('blog/update', 'BlogController@update');
    Route::get('blog/edit/{id}', 'BlogController@edit');
    Route::get('blog/delete/{id}', 'BlogController@delete');

    ////////  Gallery Module ////
    Route::get('gallery/', 'GalleryController@index');
    Route::get('gallery/add', 'GalleryController@add');
    Route::post('gallery/update', 'GalleryController@update');
    Route::get('gallery/edit/{id}', 'GalleryController@edit');
    Route::get('gallery/delete/{id}', 'GalleryController@delete');

    ////////  Social Media Module ////
    Route::get('social/', 'SocialController@index');
    Route::post('social/update', 'SocialController@update');

    ////////  Change Password ////
    Route::get('changepassword/', 'UserController@index');
     Route::post('update_password', 'UserController@update_password');
});

////// Admin Route End  ////////////



