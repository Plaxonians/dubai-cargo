<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('form_type')->comment('1->Contact, 2->Quote')->nullable();
            $table->tinyInteger('service_type')->nullable();
            $table->dateTime('feedback_date')->nullable();
            $table->string('city_from')->nullable();
            $table->string('city_to')->nullable();
            $table->string('area_from')->nullable();
            $table->string('area_to')->nullable();
            $table->longText('furniture_detail')->nullable();
            
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('area_name')->nullable();
            $table->string('nearest_landmark')->nullable();
            $table->string('address')->nullable();
            $table->string('subject')->nullable();
            $table->longText('message')->nullable();
            $table->string('upload_file')->nullable();

            $table->string('proposer')->nullable();
            $table->string('address_origin')->nullable();
            $table->string('address_destination')->nullable();
            $table->tinyInteger('shipped_by')->comment('1->Sea, 2->Air, 3->Over Land ')->nullable();
            $table->tinyInteger('packing')->comment('1->Professional, 2->Self')->nullable();
            $table->string('date_of_transit')->nullable();
            $table->string('number_of_package')->length(50)->nullable();
            $table->string('packing_name')->nullable();

            $table->longText('living_room')->nullable();
            $table->longText('dining_room')->nullable();
            $table->longText('family_room')->nullable();
            $table->longText('kitchen')->nullable();
            $table->longText('chinaware_porcelatin')->nullable();
            $table->longText('crystal_glassware')->nullable();
            $table->longText('silverware_brassware')->nullable();
            $table->longText('antiques_works_of_art')->nullable();
            $table->longText('sports_equipment')->nullable();
            $table->longText('linen_clothing')->nullable();
            $table->longText('bedroom')->nullable();
            $table->longText('other_bedroom')->nullable();
            $table->longText('bathroom')->nullable();
            $table->longText('miscellaneous')->nullable();
            $table->longText('any_other_items')->nullable();

            $table->tinyInteger('tick_cover')->comment('1->Full Conditions, 2->Restricted Conditions')->nullable();
            $table->longText('note')->nullable();

            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('is_deleted')->length(1)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
