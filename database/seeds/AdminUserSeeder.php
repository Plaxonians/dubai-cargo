<?php
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\UserRole;
use App\Permission;
use App\RolePermission;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
                    'name' => 'Dubai Cargo',
                    'email' => 'dubaicargo@dubaicargo.com',
                    'password' => Hash::make('password'),
                    'created_at' => date("Y-m-d h:i:s"),
                ]);

         $role = Role::create([
                    'name' => 'Admin',
                    'slug' => 'admin',
                    'created_at' => date("Y-m-d h:i:s"),
                ]);

          Role::create([
                    'name' => 'User',
                    'slug' => 'user',
                    'created_at' => date("Y-m-d h:i:s"),
                ]);

           UserRole::create([
                    'user_id' => $user->id,
                    'role_id' => $role->id,
                    'created_at' => date("Y-m-d h:i:s"),
                ]);

 $permissions_data = array () ;
 $permissions_data[]  =  [
                         'name' => 'Home Banner',
                         'slug' => 'home_banner',
                        ];

 $permissions_data[]  =  [
                           'name' => 'About Us',
                           'slug' => 'about_us',
                           'data'=> [
                                     [
                                      'name'=>'About',
                                      'slug'=>'about',
                                     ],
                                    ],
                          ];
 $permissions_data[]  =  [
                           'name' => 'Services ',
                           'slug' => 'services',
                           'data'=> [],
                          ];

 $permissions_data[]  =  [
                           'name' => config('constants.website_name').' Forms ',
                           'slug' => 'forms',
                           'data'=> [[
                                    'name'=>'Contact Forms',
                                    'slug'=>'contact_forms',
                                    ],
                                    [
                                    'name'=>'Quote Forms',
                                    'slug'=>'quote_forms',
                                    ]],
                          ];

$permissions_data[]  =  [
                           'name' => ' Blog ',
                           'slug' => 'blog',
                           'data'=> [[
                                    'name'=>'Add Blog',
                                    'slug'=>'add_blog',
                                    ],
                                    [
                                    'name'=>'Blog Listing',
                                    'slug'=>'blog_listing',
                                    ]],
                          ];

      foreach ($permissions_data as $key => $value) {
                    $permission = Permission::create([
                                    'name' => $value['name'],
                                    'slug' => $value['slug'],
                                    'created_at' => date("Y-m-d h:i:s"),
                                ]);
                    RolePermission::create([
                                    'role_id' => $role->id,
                                    'permission_id' => $permission->id,
                                    'created_at' => date("Y-m-d h:i:s"),
                                ]);

                    if(isset($value['data'])) {
                        foreach ($value['data'] as $key_d => $value_d) {
                                $permission = Permission::create([
                                                'parent_id' => $permission->id,
                                                'name' => $value_d['name'],
                                                'slug' => $value_d['slug'],
                                                'created_at' => date("Y-m-d h:i:s"),
                                            ]);
                                RolePermission::create([
                                    'role_id' => $role->id,
                                    'permission_id' => $permission->id,
                                    'created_at' => date("Y-m-d h:i:s"),
                                ]);
                        }
                    }
        }            
           



    }
}
