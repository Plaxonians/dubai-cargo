<!DOCTYPE html>
<html>
<head>
    <title>Contact us</title>
    <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body> 

	<table>
  <tr >
    <th colspan="2" style="text-align: center;">{{ config('constants.website_name') }} Contact Query </th>
  </tr>
  <tr>
    <th> Name :</th>
    <td>{{ isset($data->first_name) ? $data->first_name : '' }}</td>
  </tr>
 
  
  <tr>
    <th>Email :</th>
    <td>{{ isset($data->email) ? $data->email : '' }}</td>
  </tr>

   <tr>
    <th>Contact number :</th>
    <td>{{ isset($data->phone) ? $data->phone : '' }}</td>
  </tr>

   <tr>
    <th>Message :</th>
    <td>{{ isset($data->message) ? $data->message : '' }}</td>
  </tr>
  
</table>
    
</body>
</html>