@php
$social_media_link = get_social_media_links() ;
$contact_us_data = get_contact_us_data() ;
@endphp

<!-- Footer   -->
<footer>
    <div class="container">
        <div class="footer-row">
            <div class="cus-col">
                <div class="logo">
                    <a href="{{ url('/')}}">
                        <img src="{{ URL::to(asset('images/frontend/logo-white.svg')) }}" alt="">
                    </a>
                </div>
            </div>
            <div class="cus-col">
                <h2 class="title">Contact Information</h2>
                <ul class="footer-list">
                    <li class="footer-item phone">
                        <span class="label">Phone:</span>
                        <a href="tel:+@php echo isset($contact_us_data->phone) ? $contact_us_data->phone : '' @endphp">
                            @php echo isset($contact_us_data->phone) ? $contact_us_data->phone : '' @endphp
                        </a>
                        <!-- <a href="tel:+@php  echo isset($contact_us_data->phone) ? str_replace(['-'],[''],$contact_us_data->phone) : '' @endphp"
                            class="service-title right">
                            <div class="three">@php echo isset($contact_us_data->phone) ?
                                substr($contact_us_data->phone,0,3) : '' @endphp</div>
                            <div class="rest">
                                <span class="top">ASH-CARGO</span>
                                <span class="bottom">@php echo isset($contact_us_data->phone) ?
                                    substr($contact_us_data->phone,4) : '' @endphp</span>
                            </div>
                        </a> -->
                    </li>
                    <li class="footer-item landline">
                        <span class="label">Landline Number:</span>
                        <a href="tel:@php echo isset($contact_us_data->landline_no) ?
                        str_replace(['-'],[''],$contact_us_data->landline_no) : '' @endphp"> @php echo
                            isset($contact_us_data->landline_no) ?
                            ($contact_us_data->landline_no) : '' @endphp</a>
                    </li>
                    <li>

                    </li>
                    <li class="footer-item address">
                        <span class="label">Location:</span> @php echo isset($contact_us_data->address_one_address) ?
                        ($contact_us_data->address_one_address) : '' @endphp
                    </li>
                </ul>
            </div>
            <div class="cus-col">
                <h2 class="title">Services</h2>
                <ul class="footer-list">
                    <li class="footer-item"><a
                            href="{{ url('services/'.create_slugify(get_services_name(1)))}}">{{ get_services_name(1)}}
                        </a></li>
                    <li class="footer-item"><a
                            href="{{ url('services/'.create_slugify(get_services_name(2)))}}">{{ get_services_name(2)}}</a>
                    </li>
                    <li class="footer-item"><a
                            href="{{ url('services/'.create_slugify(get_services_name(4)))}}">{{ get_services_name(4)}}</a>
                    </li>
                    <li class="footer-item"><a
                            href="{{ url('services/'.create_slugify(get_services_name(3)))}}">{{ get_services_name(3)}}</a>
                    </li>
                    <li class="footer-item"><a
                            href="{{ url('services/'.create_slugify(get_services_name(5)))}}">{{ get_services_name(5)}}</a>
                    </li>
                </ul>
            </div>
            <div class="cus-col">
                <h2 class="title">Links</h2>
                <ul class="footer-list">

                    <li class="footer-item"><a href="{{ $social_media_link['facebook_link'] ?? '' }}"
                            target="_blank">Facebbok</a></li>
                    <li class="footer-item"><a href="{{ $social_media_link['instagram_link'] ?? '' }}"
                            target="_blank">Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <p class="text">© Copyright {{ date('Y')}}, Plaxonic Technologies. All Rights Reserved.
        </p>
    </div>
</footer>