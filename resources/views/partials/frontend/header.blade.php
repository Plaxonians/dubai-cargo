<header>
        <div class="container-100">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{ url('/')}}">
                    <img src="{{ URL::to(asset('images/frontend/logo.svg')) }}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item  {{ request()->is('/')  ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('/')}}">HOME</a>
                        </li>
                        <li class="nav-item {{ request()->is('aboutus/profile') || request()->is('aboutus/clientreview') || request()->is('aboutus/gallery')  ? 'active' : '' }}" >
                            <a class="nav-link" href="javascript:">ABOUT US</a>
                             <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('aboutus/profile')}}">Company Profile</a>
                                <a class="dropdown-item" href="{{ url('aboutus/clientreview')}}">Client Review</a>
                                <a class="dropdown-item" href="{{ url('aboutus/gallery')}}">Gallery</a>
                            </div>
                        </li>
                        <li class="nav-item {{ request()->is('services/*')  ? 'active' : '' }}">
                            <a class="nav-link" href="javascript:">SERVICES</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('services/'.create_slugify(get_services_name(1)))}}">{{ get_services_name(1)}}</a>
                                <a class="dropdown-item" href="{{ url('services/'.create_slugify(get_services_name(2)))}}">{{ get_services_name(2)}}</a>
                                <a class="dropdown-item" href="{{ url('services/'.create_slugify(get_services_name(4)))}}">{{ get_services_name(4)}}</a>
                                <a class="dropdown-item" href="{{ url('services/'.create_slugify(get_services_name(3)))}}">{{ get_services_name(3)}}</a>
                                <a class="dropdown-item" href="{{ url('services/'.create_slugify(get_services_name(5)))}}">{{ get_services_name(5)}}</a>
                            </div>
                        </li>
                
                        <li class="nav-item {{ request()->is('blogs*')  ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('blogs')}}">BLOGS</a>
                        </li>
                        <li class="nav-item {{ request()->is('contact*')  ? 'active' : '' }}">
                            <a class="nav-link" href="javascript:">CONTACT US</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('contact')}}">Contact</a>
                                <a class="dropdown-item" href="{{ url('contact/address')}}">Address</a>
                            </div>
                        </li>
                        <li class="nav-item {{ request()->is('get-a-quote*')  ? 'active' : '' }}">
                            <a href="{{ url('get-a-quote')}}" class="btn-primary">
                                Get A Quote
                            </a>
                        </li>
                    </ul>

                </div>
            </nav>
        </div>
    </header>