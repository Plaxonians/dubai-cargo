<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.home') }}" class="brand-link">
      <img src="{{ URL::to(config('constants.website_logo')) }}" alt="Red Trucking" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ config('constants.website_name') }}</span>
    </a>
   

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
       <!--  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ isset(\Auth::user()->image) && !empty(\Auth::user()->image) && file_exists(\Auth::user()->image) ? URL::to(\Auth::user()->image) : URL::to(config('constants.no_user_image')) }}" class="img-circle elevation-2" alt="{{ isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '' }}">
        </div>
        <div class="info">
          <a href="#" class="d-block">  {{ isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '' }} </a>
        </div>
      </div> -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                

                <li class="nav-item">
                    <a href="{{ route('admin.home') }}" class="nav-link {{ request()->is('admin')? 'active' : '' }}">
                        
                            <i class="nav-icon fas fa-tachometer-alt">

                            </i>
                            <p><span>Dashboard </span>
                        </p>
                    </a>
                </li>
               


                <li class="nav-item has-treeview {{ request()->is('admin/about') || request()->is('admin/about/companyprofile') || request()->is('admin/about/clientreview')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-tags">

                            </i>
                            <p>
                                <span>About Us </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/about/companyprofile') }}" class="nav-link {{ request()->is('admin/about/companyprofile')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Company Profile</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/about/clientreview') }}" class="nav-link {{ request()->is('admin/about/clientreview')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Client Review</span>
                                        </p>
                                    </a>
                                </li>

                               
                        </ul>
                </li>

                <li class="nav-item has-treeview {{ request()->is('admin/blog/add') || request()->is('admin/blog') || request()->is('admin/blog/*')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-th-large">

                            </i>
                            <p>
                                <span>Blog </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/blog/add') }}" class="nav-link {{ request()->is('admin/blog/add')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Add Blog</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/blog') }}" class="nav-link {{ request()->is('admin/blog')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Blog Listing</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ URL::to('admin/contactus') }}" class="nav-link {{ request()->is('admin/contactus')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-address-book">

                            </i>
                           <p> <span>Contact Us </span>
                        </p>
                    </a>
                </li>


                 <li class="nav-item has-treeview {{ request()->is('admin/form/contact') || request()->is('admin/form/quote') || request()->is('admin/form/feedback') || request()->is('admin/form/getquote')    ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-address-book">

                            </i>
                            <p>
                                <span> Forms </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/contact') }}" class="nav-link {{ request()->is('admin/form/contact')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Contact Forms</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/feedback') }}" class="nav-link {{ request()->is('admin/form/feedback')? 'active' : '' }}">
                                        <i class=" nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Client Feedback Forms</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/quote') }}" class="nav-link {{ request()->is('admin/form/quote')? 'active' : '' }}">
                                        <i class=" nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Quote Forms</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/form/getquote') }}" class="nav-link {{ request()->is('admin/form/getquote')? 'active' : '' }}">
                                        <i class=" nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Get Quote Forms</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>
 
                <li class="nav-item has-treeview {{ request()->is('admin/gallery/add') || request()->is('admin/gallery') || request()->is('admin/gallery/*')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-book">

                            </i>
                            <p>
                                <span>Gallery </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/gallery/add') }}" class="nav-link {{ request()->is('admin/gallery/add')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Add Gallery</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/gallery') }}" class="nav-link {{ request()->is('admin/gallery')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Gallery Listing</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>
                

                 <li class="nav-item">
                    <a href="{{ URL::to('admin/banner') }}" class="nav-link {{ request()->is('admin/banner')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-window-restore">

                            </i>
                            <p><span>Home Banners </span>
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ URL::to('admin/homepage') }}" class="nav-link {{ request()->is('admin/homepage') || request()->is('admin/homepage/addclient')? 'active' : '' }}">
                       
                            <i class="nav-icon fa fa-file">

                            </i>
                             <p><span>Home Page </span>
                        </p>
                    </a>
                </li>

                


                <li class="nav-item has-treeview {{ request()->is('admin/services') || request()->is('admin/services/residential') || request()->is('admin/services/office') || request()->is('admin/services/furniture') || request()->is('admin/services/storage') || request()->is('admin/services/frieghtcargo') ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-server">

                            </i>
                            <p>
                                <span>Services </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/residential') }}" class="nav-link {{ request()->is('admin/services/residential')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>{{ get_services_name(1)}}</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/office') }}" class="nav-link {{ request()->is('admin/services/office')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>{{ get_services_name(2)}}</span>
                                        </p>
                                    </a>
                                </li>

                                
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/storage') }}" class="nav-link {{ request()->is('admin/services/storage')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>{{ get_services_name(4)}}</span>
                                        </p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/furniture') }}" class="nav-link {{ request()->is('admin/services/furniture')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>{{ get_services_name(3)}}</span>
                                        </p>
                                    </a>
                                </li>
                                

                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/services/frieghtcargo') }}" class="nav-link {{ request()->is('admin/services/frieghtcargo')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>{{ get_services_name(5)}}</span>
                                        </p>
                                    </a>
                                </li>

                        </ul>
                </li>

               

                


               

               

                

                <li class="nav-item">
                    <a href="{{ URL::to('admin/social') }}" class="nav-link {{ request()->is('admin/social')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-share-square">

                            </i>
                           <p> <span>Social Media </span>
                        </p>
                    </a>
                </li>

                 <li class="nav-item has-treeview {{ request()->is('admin/testimonial/add') || request()->is('admin/testimonial') || request()->is('admin/testimonial/*')  ? 'menu-open' : '' }} ">
                        <a class="nav-link nav-dropdown-toggle ">
                            <i class="nav-icon fa fa-align-center">

                            </i>
                            <p>
                                <span>Testimonial </span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                           
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/testimonial/add') }}" class="nav-link {{ request()->is('admin/testimonial/add')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Add Testimonial</span>
                                        </p>
                                    </a>
                                </li>
                          
                                <li class="nav-item">
                                    <a href="{{ URL::to('admin/testimonial') }}" class="nav-link {{ request()->is('admin/testimonial')? 'active' : '' }}">
                                        <i class="nav-icon fa fa-circle">

                                        </i>
                                        <p>
                                            <span>Testimonial Listing</span>
                                        </p>
                                    </a>
                                </li>


                        </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ URL::to('admin/changepassword') }}" class="nav-link {{ request()->is('admin/changepassword')? 'active' : '' }}">
                        
                            <i class="nav-icon fa fa-unlock-alt">

                            </i>
                           <p> <span>Change Password </span>
                        </p>
                    </a>
                </li>

               
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        
                            <i class="nav-icon fas fa-sign-out-alt">

                            </i>
                           <p> <span>Logout</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>