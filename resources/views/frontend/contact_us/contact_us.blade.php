@extends('layouts.frontend')
@section('content')

<div class="common-banner contact-page-wrapper-banner">
    <h2 class="title">GET IN TOUCH</h2>
    <p class="bread-crumb">Home - Contact Us</p>
    <!-- <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a> -->
    <a href="#contact-us-parent" class="floating-quote-btn">Book an
        Appointment</a>
</div>

<section class="contact-page-wrapper scroll-to">
    <div class="top-sec">
        <div class="left-block block">
            <h4 class="primary-title">Get IN TOUCH</h4>
            <h2 class="service-title">Drop Us A Line<span class="stop"></span>
            </h2>
        </div>

        <a href="tel:@php  echo isset($contact_us_data->phone) ? $contact_us_data->phone : '' @endphp"
            class="right-block block">
            <h4 class="primary-title">MOBILE</h4>
            <!-- <h2 class="service-title">@php  echo isset($contact_us_data->phone) ? $contact_us_data->phone : '' @endphp
            </h2> -->
            <h2 class="service-title right">
                <div class="three">@php echo isset($contact_us_data->phone) ? substr($contact_us_data->phone,0,3) : ''
                    @endphp</div>
                <div class="rest">
                    <span class="top">ASH-CARGO</span>
                    <span class="bottom">@php echo isset($contact_us_data->phone) ? substr($contact_us_data->phone,4) :
                        '' @endphp</span>
                </div>
            </h2>
        </a>
    </div>
    <div class="contact-form bottom-sec">
        <div class="floating-form " id="contact-us-parent">
            <form action="{{ URL::to('contact/save') }}" name="contact_us_form" id="contact_us_form" method="POST"
                enctype="multipart/form-data">
                @csrf


                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name*" name="first_name"
                                value="{{ old('first_name', '') }}">
                            @if($errors->has('first_name'))
                            <p class="error">
                                {{ $errors->first('first_name') }}
                            </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your Email*" name="email"
                                value="{{ old('email', '') }}">
                            @if($errors->has('email'))
                            <p class="error">
                                {{ $errors->first('email') }}
                            </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control is_number" placeholder="Contact Number*"
                                name="contact_number" value="{{ old('contact_number', '') }}" maxlength="10">
                            @if($errors->has('contact_number'))
                            <p class="error">
                                {{ $errors->first('contact_number') }}
                            </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea name="message" id="" cols="30" rows="5" placeholder="Message"
                                class="form-control">{{ old('message', '') }}</textarea>
                            @if($errors->has('message'))
                            <p class="error">
                                {{ $errors->first('message') }}
                            </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn-primary">Submit</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</section>



@endsection
@section('scripts')
@parent

@endsection