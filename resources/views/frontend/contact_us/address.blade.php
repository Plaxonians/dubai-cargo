@extends('layouts.frontend')
@section('content')


<div class="common-banner contact-page-wrapper-banner">

    <h2 class="title">OFFICE LOCATIONS</h2>
    <p class="bread-crumb">Home > Contact Us</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>

<section class="address-page scroll-to">
    <div href="javascript:" class="cus-card address" address_type="address_one">
        <h4 class="primary-title">ADDRESS</h4>
        <h2 class="service-title">{{ $contact_us_data->address_one_location_name ?? '' }}</h2>
        <div class="number-block">
            <a href="tel:+80027422746" class="service-title right">
                <div class="three">800</div>
                <div class="rest">
                    <span class="top">ASH-CARGO</span>
                    <span class="bottom">274-22746</span>
                </div>
            </a>
            <div class="land-line">
                <span class="label">LandLine : </span>
                <a href="tel:+02-5626126" class="number"> 02-5626126</a>
            </div>
        </div>
        <p class="para desc">@php echo isset($contact_us_data->address_one_address) ?
            $contact_us_data->address_one_address : '' @endphp</p>
    </div>
    <!--  <a href="javascript:" class="cus-card address" address_type="address_two">
        <h4 class="primary-title">AdDRESS 2</h4>
        <h2 class="service-title">{{ $contact_us_data->address_two_location_name ?? '' }}</h2>
        <p class="para desc">@php echo isset($contact_us_data->address_two_address) ?
            $contact_us_data->address_two_address : '' @endphp</p>
    </a> -->

</section>


<div class="common-banner address-banner" id="address_one">
    @php echo isset($contact_us_data->address_one_embed_map) ? $contact_us_data->address_one_embed_map : '' ; @endphp
</div>

<!-- <div class="common-banner address-banner" id="address_two" style="display: none;">
    @php echo isset($contact_us_data->address_two_embed_map) ? $contact_us_data->address_two_embed_map : '' ; @endphp
</div> -->



@endsection
@section('scripts')
@parent

@endsection