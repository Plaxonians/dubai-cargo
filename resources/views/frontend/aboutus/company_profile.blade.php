@extends('layouts.frontend')
@section('content')

<div class="common-banner about-us-bannner">
    <h2 class="title">Company Profile</h2>
    <p class="bread-crumb">Home > About Us</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>
<section class="about">
    <div class="container">
        <ul class="cus-tab scroll-to">
            <li>
                <a class="active " href="{{ url('aboutus/profile')}}">Company Profile</a>
            </li>
            <li>
                <a class="" href="{{ url('aboutus/clientreview')}}">Client Review</a>
            </li>
            <li>
                <a class="" href="{{ url('aboutus/gallery')}}">Gallery</a>
            </li>
        </ul>
    </div>
    <div class="about-tab-content">
        <div class="about-tab-1-sec-1">
            <div class="container">
                <h4 class="primary-title">Our Company Profile</h4>
                <h2 class="service-title">@php echo isset($company_profile_data->title) ?
                    $company_profile_data->title : '' ; @endphp<span class="stop"></span>
                </h2>
                <div class="text-block">
                    <p class="para">
                        @php echo isset($company_profile_data->first_content) ? $company_profile_data->first_content
                        : '' ; @endphp
                    </p>
                    @if(isset($company_profile_data->left_image) && file_exists($company_profile_data->left_image))
                    <img src="{{ URL::to($company_profile_data->left_image) }}" class="floatin-left">
                    @endif

                    <p class="para">
                        @php echo isset($company_profile_data->right_content) ? $company_profile_data->right_content
                        : '' ; @endphp
                    </p>
                    <p class="para">
                        @php echo isset($company_profile_data->bottom_content) ?
                        $company_profile_data->bottom_content : '' ; @endphp
                    </p>
                </div>
            </div>
        </div>

        @php
        $profile_points = isset($company_profile_data->profile_points) &&
        !empty($company_profile_data->profile_points) ? ($company_profile_data->profile_points) : array() ;
        @endphp
        @php /* @endphp
        <div class="about-tab-1-sec-2">
            <div class="container">
                <ul class="about-list">
                    @foreach($profile_points as $key => $value)
                    <li class="about-item">{{ $value ?? '' }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @php */ @endphp
        @php /* @endphp
        <div class="about-tab-1-sec-3">
            <div class="container">
                <h4 class="primary-title">OUR MISSION</h4>
                <h2 class="service-title"> @php echo isset($company_profile_data->our_mission_title) ?
                    $company_profile_data->our_mission_title : '' ; @endphp <span class="stop"></span>
                </h2>
                <div class="row">
                    <div class="col-md-4">
                        <p class="cus-card para">
                            @php echo isset($company_profile_data->our_mission_first_content) ?
                            $company_profile_data->our_mission_first_content : '' ; @endphp
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="cus-card para">
                            @php echo isset($company_profile_data->our_mission_second_content) ?
                            $company_profile_data->our_mission_second_content : '' ; @endphp
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="cus-card para">
                            @php echo isset($company_profile_data->our_mission_third_content) ?
                            $company_profile_data->our_mission_third_content : '' ; @endphp
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @php */ @endphp
        @include('frontend.home.components.quote_form')
        @include('frontend.home.components.testimonial')



    </div>



</section>


@endsection
@section('scripts')
@parent

@endsection