@extends('layouts.frontend')
@section('content')

<div class="common-banner about-us-bannner">
    <h2 class="title">Gallery</h2>
    <p class="bread-crumb">Home > About Us</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>
<section class="about">
    <div class="container">
        <ul class="cus-tab scroll-to">
            <li>
                <a class="" href="{{ url('aboutus/profile')}}">Company Profile</a>
            </li>
            <li>
                <a class="" href="{{ url('aboutus/clientreview')}}">Client Review</a>
            </li>
            <li>
                <a class="active" href="{{ url('aboutus/gallery')}}">Gallery</a>
            </li>
        </ul>
    </div>
    <div class="about-tab-content">
        @if(isset($gallery_data) && sizeof($gallery_data) )
        <div class="gallery-list gallery-page">
            @foreach($gallery_data as $key => $value)
            <div class="gallery-item">
                <div class="gallery-card">
                    <a href="{{ URL::to($value->image) }}" data-fancybox="gallery">
                        <img src="{{ URL::to($value->image) }}" alt="" data-aos="fade-up" class="aos-init">
                    </a>
                </div>
            </div>
            @endforeach

            <div class="cus-pagination">
                <ul class="pages">
                    {!! $gallery_data->links() !!}

                </ul>
            </div>
        </div>
        @endif


        @include('frontend.home.components.quote_form')
        @include('frontend.home.components.testimonial')


    </div>



</section>


@endsection
@section('scripts')
@parent

@endsection