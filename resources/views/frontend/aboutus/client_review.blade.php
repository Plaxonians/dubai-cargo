@extends('layouts.frontend')
@section('content')

<div class="common-banner about-us-bannner">
    <h2 class="title">Client Review</h2>
    <p class="bread-crumb">Home > About Us</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>
<section class="about">
    <div class="container">
        <ul class="cus-tab scroll-to">
            <li>
                <a class="btn-primary" href="{{ url('aboutus/profile')}}">Company Profile</a>
            </li>
            <li>
                <a class="active btn-primary" href="{{ url('aboutus/clientreview')}}">Client Review</a>
            </li>
            <li>
                <a class="btn-primary" href="{{ url('aboutus/gallery')}}">Gallery</a>
            </li>
        </ul>
    </div>

    <div class="about-tab-content">
        <div class="about-tab-2-sec-1">
            <div class="container">
                <h4 class="primary-title">@php echo isset($about_us_client_review_data->title) ?
                    $about_us_client_review_data->title : '' ; @endphp</h4>
                <h2 class="service-title">@php echo isset($about_us_client_review_data->sub_title) ?
                    $about_us_client_review_data->sub_title : '' ; @endphp<span class="stop"></span>
                </h2>
                <p class="para top">
                    @php echo isset($about_us_client_review_data->review_content) ?
                    $about_us_client_review_data->review_content : '' ; @endphp
                </p>
                @if(isset($all_testimonials) && sizeof($all_testimonials) )
                <div class="row">
                    @foreach($all_testimonials as $key => $value)
                    <div class="col-md-6">
                        <div class="feedback-card">
                            <p class="para">{{ $value->details ?? '' }}</p>
                            <div class="author-block">
                                <img src="{{ URL::to($value->image ?? '') }}" alt="" width="45" height="45">
                                <p class="author">{{ $value->name ?? '' }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                @endif
            </div>
        </div>
        @include('frontend.home.components.quote_form')

    </div>

    <div class="about-tab-2-sec-2">
        <div class="container">
            <h4 class="primary-title">SHARE YOUR FEEDBACK</h4>
            <h2 class="service-title">@php echo isset($about_us_client_review_data->feedback_title) ?
                $about_us_client_review_data->feedback_title : '' ; @endphp <span class="stop"></span>
            </h2>
            <p class="para bottom">
                @php echo isset($about_us_client_review_data->feedback_content) ?
                $about_us_client_review_data->feedback_content : '' ; @endphp
            </p>
            <div id="feedback_result" style="display: none;">
            </div>
            <form action="" id="client_feedback" class="col-md-10 offset-md-1" method="POST"
                enctype="multipart/form-data">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="service_type" id="service_type" class="form-control select2-without-search">
                                <option value="">Select service</option>
                                @foreach(get_services_list() as $key => $value)
                                <option value="{{ $key}}">{{ $value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="date-picker">
                            <div class="form-group">
                                <input type="text" name="feedback_date" class="form-control start_date"
                                    placeholder="When did you avail the service?">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h2 class="service-title bottom">
                            Your Information
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="first_name" placeholder="Your Name*">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control is_number" name="phone" placeholder="Contact No*"
                                maxlength="10">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email Address*">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea id="" cols="30" rows="5" class="form-control" placeholder="Message"
                                name="message"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-wrapper">
                            <label for="attach-file" class="attach-file-label">
                                <i class="fa fa-paperclip icon"></i>
                                Attach Media
                                <input type="file" id="upload_file" name="upload_file" class="attach-file"
                                    accept="application/msword, application/pdf, image/*">
                            </label>


                        </div>
                    </div>
                    <div class="col-md-6">
                        <button class="submit-btn client_feedback  btn-primary" type="submit">SEND ENQUIRY</button>
                    </div>

                </div>
            </form>
        </div>
    </div>



</section>


@endsection
@section('scripts')
@parent

@endsection