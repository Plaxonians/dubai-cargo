@extends('layouts.frontend')
@section('content')

<div class="thank-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 cus-col">
                <h2 class="service">
                    THANK
                    <br>
                    YOU
                </h2>
                <p class="para">@php echo isset($message) ? $message : '' ; @endphp</p>
                <a href="" class="btn-primary">
                    Go To Homepage
                </a>
            </div>
            <div class="col-lg-6 cus-col">
                <div class="img-wrapper">
                    <img src="https://clientapp.brandmydream.com/dubai-cargo/images/frontend/thank-u.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent

@endsection