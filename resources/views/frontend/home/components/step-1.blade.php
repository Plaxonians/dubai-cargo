<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control" name="first_name" placeholder="First Name*">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Email Address*">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control is_number" maxlength="10" name="phone" placeholder="Phone No*">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control" name="area_name"
                placeholder="Area name / Villa / Apartment / Flat no.">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" class="form-control" name="nearest_landmark" placeholder="Nearest Landmark">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control" name="address" placeholder="Address">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <textarea class="form-control" name="message" placeholder="Message" id="message" cols="30" rows="5"></textarea>
        </div>
    </div>
</div>
<p class="note para">Note : Share photos or videos of items you want to move in your home or office to help give you
    accurate
    quotation</p>
<div class="btn-wrapper">
    <label for="attach-file" class="attach-file-label">
        <i class="fa fa-paperclip icon"></i>
        Attach Media
        <input type="file" id="attach-file" name="attach_file" class="attach-file"
            accept="application/msword, application/pdf, image/*" />
    </label>

    <button class="next-btn next-btn1 btn-primary" type="button">Next</button>
</div>