<ul class="instruction-list">
    <li class="instruction-item">All items should be insured for present day market value at destination. Failure to do
        so will result in valuation
        clause being applied.</li>
    <li class="instruction-item">All items being shipped must be declared for insurance.</li>
    <li class="instruction-item">Describe each item and give individual values. Please give details of each set.</li>
    <li class="instruction-item">Please list separately any items exceeding AED 1000/= </li>
</ul>
<div class="table-responsive">
    <table class="table table-bordered long-table">
        <thead>
            <tr>
                <th>No.</th>
                <th>ARTICLES</th>
                <th>No. Items</th>                
                <th>No.</th>
                <th>ARTICLES</th>
                <th>No. Items</th>               
            </tr>
        </thead>
        <tbody>
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>             
            </tr>
            <tr>
                <th>A</th>
                <th>LIVING ROOM</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>             
            </tr>
            <tr>
                <td>1</td>
                <td>Sofas</td>
                <td>
                    <input type="text" name="living_room['sofas']['items']" class="form-control is_number">
                </td>                
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="living_room['chairs']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>3</td>
                <td>Lamps & Shades</td>
                <td>
                    <input type="text" name="living_room['lamps_and_shades']['items']" class="form-control is_number">
                </td>
             
                <td>4</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="living_room['tables']['items']" class="form-control is_number">
                </td>         
            </tr>
            <tr>
                <td>5</td>
                <td>Radios</td>
                <td>
                    <input type="text" name="living_room['radios']['items']" class="form-control is_number">
                </td>          
                <td>6</td>
                <td>TVs</td>
                <td>
                    <input type="text" name="living_room['tvs']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>7</td>
                <td>Rugs & Carpets</td>
                <td>
                    <input type="text" name="living_room['rugs_and_carpets']['items']" class="form-control is_number">
                </td>           
                <td>8</td>
                <td>Curtains</td>
                <td>
                    <input type="text" name="living_room['curtains']['items']" class="form-control is_number">
                </td>           
            </tr>
            <tr>
                <td>9</td>
                <td>Piano & Musical Instruments</td>
                <td>
                    <input type="text" name="living_room['piano_and_musical_instruments']['items']"
                        class="form-control is_number">
                </td>             
                <td>10</td>
                <td>Bookcase / Wall Unit</td>
                <td>
                    <input type="text" name="living_room['bookcase_wall_unit']['items']" class="form-control is_number">
                </td>           
            </tr>
            <tr>
                <td>11</td>
                <td>Cupboards</td>
                <td>
                    <input type="text" name="living_room['cupboards']['items']" class="form-control is_number">
                </td>
              
                <td>12</td>
                <td>Hi-Fi system/Home Theatre</td>
                <td>
                    <input type="text" name="living_room['hi_fi_system_home_theatre']['items']"
                        class="form-control is_number">
                </td>            
            </tr>
            <tr>
                <td>13</td>
                <td>Pictures & Paintings</td>
                <td>
                    <input type="text" name="living_room['pictures_and_paintings']['items']"
                        class="form-control is_number">
                </td>              
                <td>14</td>
                <td>Video Recorder/DVD</td>
                <td>
                    <input type="text" name="living_room['video_recorder_dvd']['items']" class="form-control is_number">
                </td>              
            </tr>
             <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>              
            </tr>
            <tr>
                <th>B</th>
                <th>FAMILY ROOM</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="family_room['tables']['items']" class="form-control is_number">
                </td>                
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="family_room['chairs']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>3</td>
                <td>Cupboards</td>
                <td>
                    <input type="text" name="family_room['cupboards']['items']" class="form-control is_number">
                </td>
                <td>4</td>
                <td>Sideboard</td>
                <td>
                    <input type="text" name="family_room['sideboard']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Lamps & Chandeliers</td>
                <td>
                    <input type="text" name="family_room['lamps_and_chandeliers']['items']"
                        class="form-control is_number">
                </td>
                <td>6</td>
                <td>Rugs & Carpets</td>
                <td>
                    <input type="text" name="family_room['rugs_and_carpets']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>Curtains</td>
                <td>
                    <input type="text" name="family_room['curtains']['items']" class="form-control is_number">
                </td>
                <td>8</td>
                <td>Mirrors</td>
                <td>
                    <input type="text" name="family_room['mirrors']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>9</td>
                <td>Table Linen</td>
                <td>
                    <input type="text" name="family_room['table_linen']['items']" class="form-control is_number">
                </td>              
                <td>10</td>
                <td>Pictures & Paintings</td>
                <td>
                    <input type="text" name="family_room['pictures_and_paintings']['items']"
                        class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>11</td>
                <td>Desk</td>
                <td>
                    <input type="text" name="family_room['desk']['items']" class="form-control is_number">
                </td>             
                <td></td>
                <td></td>
                <td></td>                
            </tr>

            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>              
            </tr>
            <tr>
                <th>C</th>
                <th>DINING ROOM</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <tr>
                <td>1</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="dining_room['table']['items']" class="form-control is_number">
                </td>               
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="dining_room['chairs']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>3</td>
                <td>Cupboards</td>
                <td>
                    <input type="text" name="dining_room['cupboards']['items']" class="form-control is_number">
                </td>                
                <td>4</td>
                <td>Sideboard</td>
                <td>
                    <input type="text" name="dining_room['sideboard']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>5</td>
                <td>Lamps & Chandeliers</td>
                <td>
                    <input type="text" name="dining_room['lamps_and_chandeliers']['items']"
                        class="form-control is_number">
                </td>                
                <td>6</td>
                <td>Rugs & Carpets</td>
                <td>
                    <input type="text" name="dining_room['rugs_and_carpets']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>7</td>
                <td>Curtains</td>
                <td>
                    <input type="text" name="dining_room['curtains']['items']" class="form-control is_number">
                </td>                
                <td>8</td>
                <td>Mirrors</td>
                <td>
                    <input type="text" name="dining_room['mirrors']['items']" class="form-control is_number">
                </td>              
            </tr>
            <tr>
                <td>9</td>
                <td>Table Linen</td>
                <td>
                    <input type="text" name="dining_room['table_linen']['items']" class="form-control is_number">
                </td>                
                <td>10</td>
                <td>Pictures & Paintings</td>
                <td>
                    <input type="text" name="dining_room['pictures_and_paintings']['items']"
                        class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>11</td>
                <td>Desk</td>
                <td>
                    <input type="text" name="dining_room['desk']['items']" class="form-control is_number">
                </td>            
                <td></td>
                <td></td>
                <td></td>                
            </tr>
           
            
            
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>D</th>
                <th>BEDROOM (MAIN)</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Beds</td>
                <td>
                    <input type="text" name="bedroom['beds']['items']" class="form-control is_number">
                </td>
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="bedroom['chairs']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="bedroom['tables']['items']" class="form-control is_number">
                </td>
                <td>4</td>
                <td>Dressing Table</td>
                <td>
                    <input type="text" name="bedroom['dressing_table']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Chest of Drawers</td>
                <td>
                    <input type="text" name="bedroom['chest_of_drawers']['items']" class="form-control is_number">
                </td>              
                <td>6</td>
                <td>Mirrors</td>
                <td>
                    <input type="text" name="bedroom['mirrors']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>7</td>
                <td>Rugs</td>
                <td>
                    <input type="text" name="bedroom['rugs']['items']" class="form-control is_number">
                </td>              
                <td>8</td>
                <td>Lamps</td>
                <td>
                    <input type="text" name="bedroom['lamps']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>9</td>
                <td>Curtains</td>
                <td>
                    <input type="text" name="bedroom['curtains']['items']" class="form-control is_number">
                </td>
                <td>10</td>
                <td>Bookcases</td>
                <td>
                    <input type="text" name="bedroom['bookcases']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>11</td>
                <td>Wardrobes</td>
                <td>
                    <input type="text" name="bedroom['wardrobes']['items']" class="form-control is_number">
                </td>
                <td>12</td>
                <td>Pictures/Paintings</td>
                <td>
                    <input type="text" name="bedroom['pictures_paintings']['items']" class="form-control is_number">
                </td>               
            </tr>
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>            
            </tr>
            <tr>
                <th>E</th>
                <th>BEDROOM(S) OTHERS</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Beds</td>
                <td>
                    <input type="text" name="other_bedroom['beds']['items']" class="form-control is_number">
                </td>               
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="other_bedroom['chairs']['items']" class="form-control is_number">
                </td>              
            </tr>
            <tr>
                <td>3</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="other_bedroom['tables']['items']" class="form-control is_number">
                </td>
                <td>4</td>
                <td>Dressing Table</td>
                <td>
                    <input type="text" name="other_bedroom['dressing_table']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Chest of Drawers</td>
                <td>
                    <input type="text" name="other_bedroom['chest_of_drawers']['items']" class="form-control is_number">
                </td>
                <td>6</td>
                <td>Mirrors</td>
                <td>
                    <input type="text" name="other_bedroom['mirrors']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>Rugs</td>
                <td>
                    <input type="text" name="other_bedroom['rugs']['items']" class="form-control is_number">
                </td>                
                <td>8</td>
                <td>Lamps</td>
                <td>
                    <input type="text" name="other_bedroom['lamps']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>9</td>
                <td>Curtains</td>
                <td>
                    <input type="text" name="other_bedroom['curtains']['items']" class="form-control is_number">
                </td>
                <td>10</td>
                <td>Bookcases</td>
                <td>
                    <input type="text" name="other_bedroom['bookcases']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>11</td>
                <td>Wardrobes</td>
                <td>
                    <input type="text" name="other_bedroom['wardrobes']['items']" class="form-control is_number">
                </td>
                <td>12</td>
                <td>Pictures/Paintings</td>
                <td>
                    <input type="text" name="other_bedroom['pictures_paintings']['items']"
                        class="form-control is_number">
                </td>
            </tr>
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>              
            </tr>
            <tr>
                <th>F</th>
                <th>BATHROOM</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>1</td>
                <td>Rugs, Toilet Covers</td>
                <td>
                    <input type="text" name="bathroom['rugs_toilet_covers']['items']" class="form-control is_number">
                </td>             
                <td>2</td>
                <td>Toiletries</td>
                <td>
                    <input type="text" name="bathroom['toiletries']['items']" class="form-control is_number">
                </td>            
            </tr>
            <tr>
                <td>3</td>
                <td>Medical Supplies</td>
                <td>
                    <input type="text" name="bathroom['medical_supplies']['items']" class="form-control is_number">
                </td>             
                <td>4</td>
                <td>Towels</td>
                <td>
                    <input type="text" name="bathroom['towels']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>5</td>
                <td>Laundry Basket</td>
                <td>
                    <input type="text" name="bathroom['laundry_basket']['items']" class="form-control is_number">
                </td>            
                <td>6</td>
                <td>Perfume / Aftershave</td>
                <td>
                    <input type="text" name="bathroom['perfume_aftershave']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>7</td>
                <td>Cabinets / Shelves</td>
                <td>
                    <input type="text" name="bathroom['cabinets_shelves']['items']" class="form-control is_number">
                </td>             
                <td>8</td>
                <td>Mirrors</td>
                <td>
                    <input type="text" name="bathroom['mirrors']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>9</td>
                <td>Razors</td>
                <td>
                    <input type="text" name="bathroom['razors']['items']" class="form-control is_number">
                </td>               
                <td>10</td>
                <td>Hair Dryers</td>
                <td>
                    <input type="text" name="bathroom['hair_dryers']['items']" class="form-control is_number">
                </td>            
            </tr>
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>               
            </tr>
            <tr>
                <th>G</th>
                <th>KITCHEN</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="kitchen['tables']['items']" class="form-control is_number">
                </td>
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="kitchen['chairs']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Electrical Appliances</td>
                <td>
                    <input type="text" name="kitchen['electrical_and_appliances']['items']"
                        class="form-control is_number">
                </td>
                <td>4</td>
                <td>Cabinets</td>
                <td>
                    <input type="text" name="kitchen['cabinets']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Kitchen Linen</td>
                <td>
                    <input type="text" name="kitchen['kitchen_linen']['items']" class="form-control is_number">
                </td>
                <td>6</td>
                <td>Dishwasher</td>
                <td>
                    <input type="text" name="kitchen['dishwasher']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>Oven/Cooker</td>
                <td>
                    <input type="text" name="kitchen['oven_cooker']['items']" class="form-control is_number">
                </td>               
                <td>8</td>
                <td>Microwave</td>
                <td>
                    <input type="text" name="kitchen['microwave']['items']" class="form-control is_number">
                </td>               
            </tr>
            <tr>
                <td>9</td>
                <td>Crockery</td>
                <td>
                    <input type="text" name="kitchen['crockery']['items']" class="form-control is_number">
                </td>              
                <td>10</td>
                <td>Utensils / Cutlery</td>
                <td>
                    <input type="text" name="kitchen['utensils_cutlery']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>11</td>
                <td>Pots & Pans</td>
                <td>
                    <input type="text" name="kitchen['pots_pans']['items']" class="form-control is_number">
                </td>             
                <td>12</td>
                <td>Bowls, Trays Etc</td>
                <td>
                    <input type="text" name="kitchen['bowls_trays_etc']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>13</td>
                <td>Plastic/Tupperware</td>
                <td>
                    <input type="text" name="kitchen['plastic_tupperware']['items']" class="form-control is_number">
                </td>    
                <td>14</td>
                <td>Food Processor</td>
                <td>
                    <input type="text" name="kitchen['tables']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>15</td>
                <td>Refrigerator</td>
                <td>
                    <input type="text" name="kitchen['refrigerator']['items']" class="form-control is_number">
                </td>             
                <<td>16</td>
                    <td>Freezer</td>
                    <td>
                        <input type="text" name="kitchen['freezer']['items']" class="form-control is_number">
                    </td>               
            </tr>
            <tr>

                <td>17</td>
                <td>Vacuum Cleaner</td>
                <td>
                    <input type="text" name="kitchen['vacuum_cleaner']['items']" class="form-control is_number">
                </td>              
                <td>18</td>
                <td>Washing Machine</td>
                <td>
                    <input type="text" name="kitchen['washing_machine']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>

                <td>19</td>
                <td>Dryer</td>
                <td>
                    <input type="text" name="kitchen['dryer']['items']" class="form-control is_number">
                </td>             
            </tr>

            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>           
            </tr>
            
            <tr>
                <th>H</th>
                <th>LINEN / CLOTHING</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Coats / Jackets</td>
                <td>
                    <input type="text" name="linen_clothing['coats_jackets']['items']" class="form-control is_number">
                </td>             
                <td>2</td>
                <td>Suits</td>
                <td>
                    <input type="text" name="linen_clothing['suits']['items']" class="form-control is_number">
                </td>            
            </tr>
            <tr>
                <td>3</td>
                <td>Dresses</td>
                <td>
                    <input type="text" name="linen_clothing['dresses']['items']" class="form-control is_number">
                </td>          
                <td>4</td>
                <td>Trousers</td>
                <td>
                    <input type="text" name="linen_clothing['trousers']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>5</td>
                <td>Sweaters</td>
                <td>
                    <input type="text" name="linen_clothing['sweaters']['items']" class="form-control is_number">
                </td>            
                <td>6</td>
                <td>Blouses</td>
                <td>
                    <input type="text" name="linen_clothing['blouses']['items']" class="form-control is_number">
                </td>          
            </tr>
            <tr>
                <td>7</td>
                <td>Skirts</td>
                <td>
                    <input type="text" name="linen_clothing['skirts']['items']" class="form-control is_number">
                </td>            
                <td>8</td>
                <td>Sleepware</td>
                <td>
                    <input type="text" name="linen_clothing['sleepware']['items']" class="form-control is_number">
                </td>              
            </tr>
            <tr>
                <td>9</td>
                <td>Shirts</td>
                <td>
                    <input type="text" name="linen_clothing['shirts']['items']" class="form-control is_number">
                </td>           
                <td>10</td>
                <td>Footwear</td>
                <td>
                    <input type="text" name="linen_clothing['footwear']['items']"
                        class="form-control is_number">
                </td>           
            </tr>
            <tr>
                <td>11</td>
                <td>Hosiery / Socks</td>
                <td>
                    <input type="text" name="linen_clothing['hosiery_socks']['items']" class="form-control is_number">
                </td>             
                <td>12</td>
                <td>Ties / Scarves</td>
                <td>
                    <input type="text" name="linen_clothing['ties_scarves']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>13</td>
                <td>Underwear</td>
                <td>
                    <input type="text" name="linen_clothing['underwear']['items']" class="form-control is_number">
                </td>                
                <td>14</td>
                <td>Lingerie</td>
                <td>
                    <input type="text" name="linen_clothing['lingerie']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>15</td>
                <td>Sportswear</td>
                <td>
                    <input type="text" name="linen_clothing['sportswear']['items']" class="form-control is_number">
                </td>
                <td>16</td>
                <td>Sheets/Linen</td>
                <td>
                    <input type="text" name="linen_clothing['sheets_linen']['items']" class="form-control is_number">
                </td>
            </tr>

            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>               
            </tr>
            <tr>
                <th>I</th>
                <th>KITCHEN</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tables</td>
                <td>
                    <input type="text" name="kitchen['tables']['items']" class="form-control is_number">
                </td>
                <td>2</td>
                <td>Chairs</td>
                <td>
                    <input type="text" name="kitchen['chairs']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Electrical Appliances</td>
                <td>
                    <input type="text" name="kitchen['electrical_and_appliances']['items']"
                        class="form-control is_number">
                </td>
                <td>4</td>
                <td>Cabinets</td>
                <td>
                    <input type="text" name="kitchen['cabinets']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Kitchen Linen</td>
                <td>
                    <input type="text" name="kitchen['kitchen_linen']['items']" class="form-control is_number">
                </td>
                <td>6</td>
                <td>Dishwasher</td>
                <td>
                    <input type="text" name="kitchen['dishwasher']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>Oven/Cooker</td>
                <td>
                    <input type="text" name="kitchen['oven_cooker']['items']" class="form-control is_number">
                </td>               
                <td>8</td>
                <td>Microwave</td>
                <td>
                    <input type="text" name="kitchen['microwave']['items']" class="form-control is_number">
                </td>               
            </tr>
            <tr>
                <td>9</td>
                <td>Crockery</td>
                <td>
                    <input type="text" name="kitchen['crockery']['items']" class="form-control is_number">
                </td>              
                <td>10</td>
                <td>Utensils / Cutlery</td>
                <td>
                    <input type="text" name="kitchen['utensils_cutlery']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>11</td>
                <td>Pots & Pans</td>
                <td>
                    <input type="text" name="kitchen['pots_pans']['items']" class="form-control is_number">
                </td>             
                <td>12</td>
                <td>Bowls, Trays Etc</td>
                <td>
                    <input type="text" name="kitchen['bowls_trays_etc']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr>
                <td>13</td>
                <td>Plastic/Tupperware</td>
                <td>
                    <input type="text" name="kitchen['plastic_tupperware']['items']" class="form-control is_number">
                </td>    
                <td>14</td>
                <td>Food Processor</td>
                <td>
                    <input type="text" name="kitchen['tables']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>15</td>
                <td>Refrigerator</td>
                <td>
                    <input type="text" name="kitchen['refrigerator']['items']" class="form-control is_number">
                </td>             
                <<td>16</td>
                    <td>Freezer</td>
                    <td>
                        <input type="text" name="kitchen['freezer']['items']" class="form-control is_number">
                    </td>               
            </tr>
            <tr>

                <td>17</td>
                <td>Vacuum Cleaner</td>
                <td>
                    <input type="text" name="kitchen['vacuum_cleaner']['items']" class="form-control is_number">
                </td>              
                <td>18</td>
                <td>Washing Machine</td>
                <td>
                    <input type="text" name="kitchen['washing_machine']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>

                <td>19</td>
                <td>Dryer</td>
                <td>
                    <input type="text" name="kitchen['dryer']['items']" class="form-control is_number">
                </td>             
            </tr>
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>             
            </tr>
            <tr>
                <th>J</th>
                <th>MISCELLANEOUS</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Clocks</td>
                <td>
                    <input type="text" name="miscellaneous['clocks']['items']" class="form-control is_number">
                </td>
                <td>2</td>
                <td>Telephone / Fax</td>
                <td>
                    <input type="text" name="miscellaneous['telephone_fax']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Plant Holders</td>
                <td>
                    <input type="text" name="miscellaneous['plant_holders']['items']" class="form-control is_number">
                </td>
                <td>4</td>
                <td>Toys & Games</td>
                <td>
                    <input type="text" name="miscellaneous['toys_and_games']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Food (Non-perishable)</td>
                <td>
                    <input type="text" name="miscellaneous['food']['items']" class="form-control is_number">
                </td>
                <td>6</td>
                <td>Computer</td>
                <td>
                    <input type="text" name="miscellaneous['computer']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>Computer Printer</td>
                <td>
                    <input type="text" name="miscellaneous['computer_printer']['items']" class="form-control is_number">
                </td>
                <td>8</td>
                <td>Computer Accessories</td>
                <td>
                    <input type="text" name="miscellaneous['computer_accessories']['items']"
                        class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>9</td>
                <td>Sewing Machine</td>
                <td>
                    <input type="text" name="miscellaneous['sewing_machine']['items']" class="form-control is_number">
                </td>
                <td>10</td>
                <td>Video Camera</td>
                <td>
                    <input type="text" name="miscellaneous['video_camera']['items']" class="form-control is_number">
                </td>
            <tr>
                <td>11</td>
                <td>Cameras / Lens</td>
                <td>
                    <input type="text" name="miscellaneous['cameras_lens']['items']" class="form-control is_number">
                </td>
                <td>12</td>
                <td>Pictures / Paintings</td>
                <td>
                    <input type="text" name="miscellaneous['pictures_paintings']['items']"
                        class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>13</td>
                <td>Bicycles</td>
                <td>
                    <input type="text" name="miscellaneous['bicycles']['items']" class="form-control is_number">
                </td>

                <td>14</td>
                <td>Books</td>                <td>
                    <input type="text" name="miscellaneous['books']['items']" class="form-control is_number">
                </td>
            </tr>
            <tr>
                <td>15</td>
                <td>Audio Tapes</td>
                <td>
                    <input type="text" name="miscellaneous['audio_tapes']['items']" class="form-control is_number">
                </td>              
                <td>16</td>
                <td>CDs</td>
                <td>
                    <input type="text" name="miscellaneous['cds']['items']" class="form-control is_number">
                </td>              
            </tr>
            <tr>
                <td>17</td>
                <td>Video Tapes/DVD's</td>
                <td>
                    <input type="text" name="miscellaneous['video_tapes_dvds']['items']" class="form-control is_number">
                </td>                
                <td>18</td>
                <td>Garden Equipment</td>
                <td>
                    <input type="text" name="miscellaneous['garden_equipment']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>19</td>
                <td>Garden Furniture</td>
                <td>
                    <input type="text" name="miscellaneous['garden_furniture']['items']" class="form-control is_number">
                </td>                
                <td>20</td>
                <td>Suitcases / Trunks</td>
                <td>
                    <input type="text" name="miscellaneous['suitcases_trunks']['items']" class="form-control is_number">
                </td>                
            </tr>
            <tr>
                <td>21</td>
                <td>Tools</td>
                <td>
                    <input type="text" name="miscellaneous['tools']['items']" class="form-control is_number">
                </td>
            </tr>
           
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>           
            </tr>

            <tr>
                <th>K</th>
                <th>CHINAWARE/PORCELAIN</th>
                <td><input type="text" name="chinaware_porcelatin['items']" class="form-control is_number"></td>                
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
           
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <tr>
                <th>L</th>
                <th>CRYSTAL/GLASSWARE</th>
                <td><input type="text" name="crystal_glassware['items']" class="form-control is_number"></td>                                
                <td></td>
                <td></td>
                <td></td>
            </tr>
           
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>M</th>
                <th>SILVERWARE/BRASSWARE</th>
                <td><input type="text" name="silverware_brassware['items']" class="form-control is_number"></td>                
                </td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
           
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>            
            </tr>
            <tr>
                <th>N</th>
                <th>ANTIQUES / WORKS OF ART</th>
                <td><input type="text" name="antiques_works_of_art['items']" class="form-control is_number"></td>                
                </td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>O</th>
                <th>SPORTS EQUIPMENT</th>
                <td><input type="text" name="sports_equipment['items']" class="form-control is_number"></td>                
                <td></td>
                <td></td>
                <td></td> 
            </tr>
           
            <tr class="empty-row">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>P</th>
                <th>ANY OTHER ITEMS</th>
                <td><input type="text" name="any_other_items['items']" class="form-control is_number"></td>                
                <td></td>
                <td></td>
                <td></td>                
            </tr>
        </tbody>
    </table>
</div>
<div class="btn-wrapper">
    <button class="prev-btn prev-btn2 btn-primary" type="button">Prev</button>
    <button class="next-btn next-btn1 btn-primary" type="button">Next</button>
</div>