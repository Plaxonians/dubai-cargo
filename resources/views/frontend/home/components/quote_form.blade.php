<section class="contact-form">

    <!-- <h2 class="service-title">Get a Free
        Quote Today<span class="stop"></span></h2> -->
    <form action="" id="proposal-form" method="POST" enctype="multipart/form-data">
        @if(request()->is('get-a-quote'))
        @include('frontend.get_quote.components.date_picker')
        <h3 class="primary-title">Our Services</h3>
        <input type="hidden" name="service_type" id="service_type" value="{{ create_slugify(get_services_name(1)) }}">
        @include('frontend.get_quote.components.service_type')
        @endif
        @include('frontend.home.components.progress')
        <div class="floating-form" data-aos-anchor="#trigger-parent" data-aos="fade-up"
            data-aos-anchor-placement="top-center">
            @include('frontend.home.components.pdf')

        </div>
    </form>
</section>