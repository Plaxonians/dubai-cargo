<div id="loader" style="display: none">
    <img src="//d2erq0e4xljvr7.cloudfront.net/assets/img/ring.svg" />
</div>
<!-- <form action="" id="proposal-form" method="POST" enctype="multipart/form-data"> -->
    <div id="quote_result" style="display: none">
    </div>
    <div class="tab-content">
        
        <div class="tab-pane" id="step-1">
            @include('frontend.home.components.step-1')
        </div>
        <div class="tab-pane" id="step-2">
            @include('frontend.home.components.step-2')
        </div>
        <div class="tab-pane" id="step-3">
            @include('frontend.home.components.step-3')
        </div>
        <div class="tab-pane" id="step-4">
            @include('frontend.home.components.step-4')
        </div>
    </div>
<!-- </form> -->