@if(isset($all_testimonials) && sizeof($all_testimonials) )
<section class="testimonial">
    @foreach($all_testimonials as $key => $value)
    <div class="testimonial-slider">
        <div class="img-wrapper">
            <img src="{{ URL::to($value->image ?? '') }}" alt="">
        </div>
        <div class="text-wrapper">
            <img src="{{ URL::to('images/frontend/qoute.svg') }}" class="quote" alt="">
            <p class="para">{{ $value->details ?? '' }}</p>
            <p class="author">{{ $value->name ?? '' }}</p>
        </div>
    </div>
     @endforeach        
    
</section>
@endif