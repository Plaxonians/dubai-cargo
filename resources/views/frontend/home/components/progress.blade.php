<div class="progress-wrap">
    <div class="line-progress-bar">
        <div class="line"></div>
        <ul class="checkout-bar">
            <li class="progressbar-dots active"><span>step 1</span></li>
            <li class="progressbar-dots"><span>step 2</span></li>
            <li class="progressbar-dots"><span>step 3</span></li>
            <li class="progressbar-dots"><span>Final step</span></li>
        </ul>
    </div>
</div>