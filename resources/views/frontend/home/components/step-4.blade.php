<div class="row">
    <div class="col-md-12">
        <div class="checkbox-parent">
            <label for="" class="main-label">Please Tick cover required : </label>
            <div class="multi-checkbox-wrapper">
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Full Conditions (professionally packed goods only) </label>
                    <input type="radio" name="tick_cover" class="checkbox" value="1">
                </div>
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Restricted Conditions </label>
                    <input type="radio" name="tick_cover" class="checkbox" value="2">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="">All material facts must be disclosed, failure to do so could invalidate the policy. A material
                fact is one which would be likely to influence an insurer in the
                assessment and acceptance of the proposal. If in any doubt as to whether a fact is material then it
                should be disclosed here.</label>
            <textarea id="" cols="30" rows="5" placeholder="Enter here" name="comment"></textarea>
        </div>
    </div>
    <div class="col-md-12">
        <h6 class="declaration">Declaration: I declare that the details herein are correct according to the best of my
            knowledge and belief
            and agree that the particulars and statements
            given shall form the basis of contract between us.</h6>
    </div>
    <!-- <div class="col-md-5">
        <div class="form-group">
            <label for="">Date :</label>
            <input type="text" class="form-control" placeholder="12/11/2020">
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group">
            <label for="">Proposer Signature :</label>
            <input type="text" class="form-control" placeholder="Signature">
        </div>
    </div> -->
</div>

<div class="btn-wrapper">
    <button class="prev-btn prev-btn2 btn-primary" type="button">Prev</button>
    <button class="submit-btn quote_form_submit btn-primary" type="submit">Request My Free Estimate</button>
</div>