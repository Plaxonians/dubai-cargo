<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" name="proposer" class="form-control" placeholder="Proposer*" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control" name="address_origin" placeholder="Address at Origin*" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control" name="address_destination" placeholder="Address at Destination*" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="checkbox-parent">
            <label for="" class="main-label">Shipped by (Please Tick):</label>
            <div class="multi-checkbox-wrapper">
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Sea </label>
                    <input type="radio" name="shipped_by" class="checkbox" value="1" />
                    <p class="shipped_by_error"></p>
                </div>
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Air </label>
                    <input type="radio" name="shipped_by" class="checkbox" value="2" />
                </div>
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Over Land </label>
                    <input type="radio" name="shipped_by" class="checkbox" value="3" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="checkbox-parent">
            <label for="" class="main-label">Packing (Please Tick):</label>
            <div class="multi-checkbox-wrapper">
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Professional </label>
                    <input type="radio" name="packing" class="checkbox" value="1" />
                </div>
                <div class="checkbox-wrapper">
                    <label for="" class="checkbox-label"> Self (Restricted cover only) </label>
                    <input type="radio" name="packing" class="checkbox" value="2" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <input type="text" class="form-control start_date" name="date_of_transit" placeholder="Approx. commencement date of transit" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input type="text" class="form-control is_number" name="number_of_package" placeholder="No of packages" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control" name="packing_name" placeholder="Packing Company's Name" />
        </div>
    </div>
</div>
<div class="btn-wrapper">
    <button class="prev-btn prev-btn2 btn-primary" type="button">Prev</button>
    <button class="next-btn next-btn1 btn-primary" type="button">Next</button>
</div>