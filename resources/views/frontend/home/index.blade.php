@extends('layouts.frontend')
@section('content')


<section class="banner">
    <img src="{{ URL::to($banner_data->left_image) }}" alt="" class="left-worker" data-aos="fade-left">
    <img src="{{ URL::to($banner_data->right_image) }}" alt="" class="right-worker" data-aos="fade-left">
    <img src="{{ URL::to($banner_data->right_top_image) }}" alt="" class="right-location" data-aos="fade-down">
    <img src="{{ URL::to($banner_data->left_top_image) }}" alt="" class="left-location" data-aos="fade-down">
    <div class="block">
        <h1 class="hero-text"> @php echo isset($banner_data->title) ? $banner_data->title : '' ; @endphp <span
                class="stop"></span></h1>
        <p class="desc">@php echo isset($banner_data->sub_title) ? $banner_data->sub_title : '' ; @endphp</p>
    </div>
    <a data-aos="flip-up" href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="btn-primary">BOOK AN
        APPOINTMENT</a>
</section>

<section class="service">
    <div class="container">
        <div class="top-block">
            <h3 class="primary-title">Our Services</h3>
            <h2 class="service-title">@php echo isset($home_page_data->service_title) ? $home_page_data->service_title :
                '' ; @endphp <span class="stop"></span></h2>
            <p class="desc">@php echo isset($home_page_data->service_content) ? $home_page_data->service_content : '' ;
                @endphp </p>
        </div>

        <div class="cards-wrapper">
            <a href="{{ url('services/'.create_slugify($home_page_data->first_service))}}" class="card-block" data-aos="fade-up" data-aos-delay="200">
                <img src="{{ URL::to($home_page_data->first_service_image) }}" alt="" data-aos="fade-up">
                <h5 class="text">@php echo isset($home_page_data->first_service) ? $home_page_data->first_service : '' ;
                    @endphp</h5>
            </a>
            <a href="{{ url('services/'.create_slugify($home_page_data->second_service))}}" class="card-block" data-aos="fade-up" data-aos-delay="300">
                <img src="{{ URL::to($home_page_data->second_service_image) }}" alt="" data-aos="fade-up">
                <h5 class="text">@php echo isset($home_page_data->second_service) ? $home_page_data->second_service : ''
                    ; @endphp</h5>
            </a>
            <a href="{{ url('services/'.create_slugify($home_page_data->third_service))}}" class="card-block" data-aos="fade-up" data-aos-delay="400">
                <img src="{{ URL::to($home_page_data->third_service_image) }}" alt="" data-aos="fade-up">
                <h5 class="text">@php echo isset($home_page_data->third_service) ? $home_page_data->third_service : '' ;
                    @endphp</h5>
            </a>
            <a href="{{ url('services/'.create_slugify($home_page_data->fourth_service))}}" class="card-block" data-aos="fade-up" data-aos-delay="500">
                <img src="{{ URL::to($home_page_data->fourth_service_image) }}" alt="" data-aos="fade-up">
                <h5 class="text">@php echo isset($home_page_data->fourth_service) ? $home_page_data->fourth_service : ''
                    ; @endphp</h5>
            </a>
            <a href="{{ url('services/'.create_slugify($home_page_data->fifth_service))}}" class="card-block" data-aos="fade-up" data-aos-delay="600">
                <img src="{{ URL::to($home_page_data->fifth_service_image) }}" alt="" data-aos="fade-up">
                <h5 class="text">@php echo isset($home_page_data->fifth_service) ? $home_page_data->fifth_service : '' ;
                    @endphp</h5>
            </a>
        </div>
        <div class="bottom-block">
            <div class="img-wrapper block" data-aos="fade-right">
                <img src="{{ URL::to($home_page_data->about_us_image) }}" alt="">
            </div>
            <div class="text-wrapper block" data-aos="fade-left">
                <h2 class="primary-title">ABOUT US</h2>
                <h4 class="desc">{{ $home_page_data->about_us_title ?? '' }}</h4>
                <p class="para">{{ $home_page_data->about_us_content ?? '' }}</p>
                <a href="{{ URL::to('aboutus/profile/') }}" class="btn-primary">READ MORE </a>
            </div>
        </div>
    </div>
</section>

@include('frontend.home.components.quote_form')
<section class="gallery">
    <div class="top-block">
        <h3 class="primary-title">OUR GALLERY</h3>
        <h2 class="seconday-title">The Work Projects<span class="stop"></span></h2>
    </div>
    @if(isset($gallery_data) && sizeof($gallery_data) )
    <div class="gallery-list">
        @foreach($gallery_data as $key => $value)
        <div class="gallery-item">
            <div class="gallery-card">
                <a href="{{ URL::to($value->image) }}" data-fancybox="gallery">
                    <img src="{{ URL::to($value->image) }}" alt="" data-aos="fade-up">
                </a>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</section>

@include('frontend.home.components.testimonial')


@if(isset($blog_data) && sizeof($blog_data) )
<section class="blog">
    <div class="container">
        <div class="top-block">
            <h3 class="primary-title">Latest Updates</h3>
            <h2 class="seconday-title">Blog & News<span class="stop"></span></h2>
        </div>

        <div class="blogs-row">
            @foreach($blog_data as $key => $value)
            <div class="blog-card">
                <div class="img-wrapper">
                    <div class="date">
                        <h2 class="day">{{ date('d',strtotime($value->created_at)) ?? '' }}</h2>
                        <p class="month">{{ date('M',strtotime($value->created_at)) ?? '' }}</p>
                    </div>
                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                        <img src="{{ URL::to($value->image) }}" alt="">
                    </a>
                </div>
                <div class="text-wrapper">
                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"
                        class="title">{{ $value->title ?? '' }}</a>
                    <p class="desc">{{ $value->sort_description ?? '' }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif



@endsection
@section('scripts')
@parent

@endsection