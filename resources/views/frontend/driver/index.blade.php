@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/driver-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>RED Drivers</li>
                    </ul>
                    <h3 class="topbanner-head">RED Drivers</h3>
                </div>
            </div>
        </div>
    </section>
     @php
    $driver_meta = isset($driver_data->meta_data) && !empty($driver_data->meta_data) ? json_decode($driver_data->meta_data) : array() ;
    @endphp 

    <section class="driving-withred">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="drivewithred-content">
                        <h2 class="left-title wow fadeInDown">{{ $driver_meta->section_one_title ?? '' }}</h2>
                        <p class="wow fadeInUp">{{ $driver_meta->section_one_first_paragraph ?? '' }}</p>

                        <p class="wow fadeInUp">{{ $driver_meta->section_one_second_paragraph ?? '' }}</p>

                        <a href="#careerform" class="redbutton redbtn_arrow wow flipInX">Apply Now</a>

                        <p class="wow fadeInUp">{{ $driver_meta->section_two_title ?? '' }}</p>

                    </div>
                </div>
            </div>
        </div>
    </section>


     @if(isset($banner_data) && !empty($banner_data) )
    <section class="drivwithred-slide mt-5">
        <div class="container">
            <div class="items-slider-container js-items-slider-container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="slider slider-for">
                            @foreach($banner_data as $key => $value)
                            <div>
                                <div class="drivered-img wow fadeInLeft">
                                    <img src="{{ URL::to($value->image) }}" class="img-fluid">
                                    <div class="drivered-imgno">{{ '0'.$sr }}</div>
                                </div>

                            </div>
                             @php
                             $sr++
                             @endphp
                             @endforeach

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="slider slider-nav">
                            @foreach($banner_data as $key => $value)
                            <div>
                                <div class="drivered-content mt-lg-5 ml-md-4">
                                    <h4 class="wow fadeInDown">{{ $value->title}}</h4>
                                    <p class="wow fadeInUp">{{ $value->details}}</p>
                                </div>
                            </div>
                            @endforeach
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    <section class="reddriver-middpoint">
        <div class="container">
            <div class="row">
                <div class="col-md-12 offset-md-0 col-lg-10 offset-lg-1">
                    <div class="reddriver-points">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_first_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown"> @php  echo $driver_meta->facilities_first_title @endphp </h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_first_detail @endphp</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_second_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown">@php  echo $driver_meta->facilities_second_title @endphp </h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_second_detail @endphp </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_third_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown">@php  echo $driver_meta->facilities_third_title @endphp </h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_third_detail @endphp </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_four_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown">@php  echo $driver_meta->facilities_four_title @endphp</h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_four_detail @endphp </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_five_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown">@php  echo $driver_meta->facilities_five_title @endphp</h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_five_detail @endphp </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="reddrivepoint-list">
                                    <img src="{{ URL::to($driver_meta->facilities_six_image) ?? '' }}" class="wow zoomIn">
                                    <h4 class="wow fadeInDown">@php  echo $driver_meta->facilities_six_title @endphp</h4>
                                    <p class="wow fadeInUp">@php  echo $driver_meta->facilities_six_detail @endphp </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <section class="reddriver-carrer" id="careerform">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="redcarrer-content">
                        <h2 class="left-title wow fadeInDown">{{ $driver_meta->section_four_title ?? '' }}</h2>
                        <p class="wow fadeInUp mt-3">{{ $driver_meta->section_four_first_paragraph ?? '' }}</p>
                        <p class="wow fadeInUp mt-4">{{ $driver_meta->section_four_second_paragraph ?? '' }}</p>

                        <ul>
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_first_point ?? '' }}</li>
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_second_point ?? '' }}</li>
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_third_point ?? '' }}</li>
                            @if(isset($driver_meta->section_four_four_point) && !empty($driver_meta->section_four_four_point))
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_four_point ?? '' }}</li>
                            @endif
                            @if(isset($driver_meta->section_four_five_point) && !empty($driver_meta->section_four_five_point))
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_five_point ?? '' }}</li>
                            @endif
                            @if(isset($driver_meta->section_four_six_point) && !empty($driver_meta->section_four_six_point))
                            <li class="wow fadeInUp">{{ $driver_meta->section_four_six_point ?? '' }}</li>
                            @endif
                        </ul>

                    </div>
                </div>
                             
                <div class="col-md-6">
                   
                    <div class="redcarrer-form">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                        @endif
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                        @endif

                        <form action="{{ URL::to('driver/save') }}" name="driverregistration" method="POST" enctype="multipart/form-data">
                        @csrf

                            <div class="form-row">
                                <div class="form-group col-md-6 wow fadeInUp">
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" onkeyup="firstName()" maxlength="20">
                                    @if($errors->has('first_name'))
                                        <p class="error">
                                            {{ $errors->first('first_name') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 wow fadeInUp">
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" onkeyup="lastName()" maxlength="20">
                                    @if($errors->has('last_name'))
                                        <p class="error">
                                            {{ $errors->first('last_name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group wow fadeInUp">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                                @if($errors->has('email'))
                                        <p class="error">
                                            {{ $errors->first('email') }}
                                        </p>
                                @endif
                            </div>
                            <div class="form-group wow fadeInUp">
                                <input type="text" class="form-control is_number" id="phone" name="phone" placeholder="Phone Number"  maxlength="10" minlength="10">
                                @if($errors->has('phone'))
                                    <p class="error">
                                        {{ $errors->first('phone') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-group wow fadeInUp">
                                <label for="ccdl">Do you hold a valid CDL Class A License?</label>
                                <select class="form-control" id="is_cdl" name="is_cdl">
                                    <option value="" disabled selected hidden>Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if($errors->has('is_cdl'))
                                    <p class="error">
                                        {{ $errors->first('is_cdl') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-group wow fadeInUp">
                                <label for="cvtwic">Do you hold a valid TWIC card? * OR Have you had a TWIC card in the past?</label>
                                <select class="form-control" id="is_twice" name="is_twice">
                                    <option value="" disabled selected hidden>Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if($errors->has('is_twice'))
                                    <p class="error">
                                        {{ $errors->first('is_twice') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-group wow fadeInUp">
                                <label for="cdrexp">Do you have 2 or more years of Tractor Trailer driving experience?</label>
                                <select class="form-control" id="is_driving_experience" name="is_driving_experience">
                                    <option value="" disabled selected hidden>Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if($errors->has('is_driving_experience'))
                                    <p class="error">
                                        {{ $errors->first('is_driving_experience') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-group wow fadeInUp">
                                <label for="cdrexp">Have you done INMOTO or DRAYAGE? </label>
                                <select class="form-control" id="is_inmoto_or_drayage" name="is_inmoto_or_drayage">
                                    <option value="" disabled selected hidden>Please Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if($errors->has('is_inmoto_or_drayage'))
                                    <p class="error">
                                        {{ $errors->first('is_inmoto_or_drayage') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-row d-flex align-items-center wow fadeInUp">
                                <div class="form-group col-xl-7">
                                    <div class="file-uploader">
                                        <input id="upload_file" type="file" accept="application/msword, application/pdf, image/*" name="upload_file" />
                                        <label for="upload_file" class="custom-file-upload">
                                            <!--                                            <i class="fas fa-paperclip"></i>-->
                                            <img src="{{ URL::to(asset('images/frontend/svg/attach.svg')) }}" style="width: 25px" class="mr-3">
                                            Upload Document Here
                                        </label>
                                        @if($errors->has('upload_file'))
                                            <p class="error">
                                                {{ $errors->first('upload_file') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group offset-xl-1 col-xl-4">
                                    <input class="mt-0 border-0 submit float-xl-right overlay redbutton redbtn_arrow wow flipInX" type="submit" value="Submit > " />
                                    <div class="submit__overlay"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
@section('scripts')

@parent

@endsection