@extends('layouts.frontend')
@section('content')

<div class="common-banner services-bannner">
    <h2 class="title">{{ $page_title }}</h2>
    <p class="bread-crumb">Home > Services</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>
<section class="services">
    <div class="container">
        <h4 class="primary-title">Our Services</h4>
        <h2 class="service-title top">@php echo isset($service_data->title) ? $service_data->title : '' ; @endphp<span
                class="stop"></span>
        </h2>
        @include('frontend.service.service_tab')
    </div>

    <div class="service-content">
        <img src="{{ URL::to('images/frontend/service/Vector.png') }}" alt="" class="bg-img-right bg-img">
        <img src="{{ URL::to('images/frontend/service/Vector-1.png') }}" alt="" class="bg-img-left bg-img">
        <div class="container">
            <h2 class="service-title">
                {{ $service_data->sub_title ?? ''}}
            </h2>
            <div class="row">
                <div class="col-md-8">
                    <div class="text-block">
                        <p class="para">
                            {{ $service_data->detail ?? ''}}
                        </p>
                        <ul class="service-list">
                            @php
                            $features = isset($service_data->features) && !empty($service_data->features) ?
                            ($service_data->features) : array() ;
                            @endphp
                            @foreach($features as $p_key => $value)
                            <li>{{ $value }}</li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-img">
                        @if(isset($service_data->image) && file_exists($service_data->image))
                        <img src="{{ URL::to($service_data->image) }}" alt="">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('frontend.home.components.quote_form')
    @include('frontend.home.components.testimonial')
</section>


@endsection
@section('scripts')

@parent

@endsection