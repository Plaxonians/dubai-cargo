<ul class="service-tab scroll-to">
    @php
    $service_one = get_services_name(1) ;
    $service_two = get_services_name(2) ;
    $service_three = get_services_name(3) ;
    $service_four = get_services_name(4) ;
    $service_five = get_services_name(5) ;
    @endphp
    <li class="<?php echo $page_title == $service_one ? 'active' : '' ?>">
        <a href="{{ url('services/'.create_slugify($service_one))}}">{{ $service_one }}</a>
    </li>
    <li class="<?php echo ($page_title) == $service_two ? 'active' : '' ?>">
        <a href="{{ url('services/'.create_slugify($service_two))}}">{{ $service_two }}</a>
    </li>
    <li class="<?php echo ($page_title) == $service_four ? 'active' : '' ?>">
        <a href="{{ url('services/'.create_slugify($service_four))}}">{{ $service_four }}</a>
    </li>
    <li class=" <?php echo ($page_title) == $service_three ? 'active' : '' ?>">
        <a href="{{ url('services/'.create_slugify($service_three))}}">{{  $service_three }}</a>
    </li>

    <li class=" <?php echo ($page_title) == $service_five ? 'active' : '' ?>">
        <a href="{{ url('services/'.create_slugify($service_five))}}">{{ $service_five }}</a>
    </li>
</ul>