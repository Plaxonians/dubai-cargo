<div class="date-picker">
    <div class="form-group">
    	<p class="service_date_error" style="display: none; color: red;" align="center">Select Your available service date.</p>
        <input type="text" name="service_date" id="service_date" class="form-control start_date" placeholder="When would you like to avail this service?">
    </div>
</div>