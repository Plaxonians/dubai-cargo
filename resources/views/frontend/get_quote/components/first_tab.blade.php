<div style="display: block;" class=" show_services {{ create_slugify(get_services_name(1)) }}">
    <h4 class="service-title bottom">{{ get_services_name(1) }}</h4>

    <div class="moving-block">
        <div class="left-block block">
            <div class="form-group">
         
                <select name="residential_city_from" class="select2-without-search" id="residential_city_from" class="form-control">
                    <option value="">Select City</option>
                    @if(isset($state_list))
                    @foreach($state_list as $key => $value)
                     <option value="{{ $value->id }}">{{ ucfirst($value->name) }}</option>
                    @endforeach
                    @endif()
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="residential_area_from" class="form-control" placeholder="Area / Building / Community*">
            </div>
        </div>
        <div class="right-block block">
            <div class="form-group">
                
                <select name="residential_city_to" class="select2-without-search" id="residential_city_to" class="form-control">
                    <option value="">Select City</option>
                    @if(isset($state_list))
                    @foreach($state_list as $key => $value)
                     <option value="{{ $value->id }}">{{ ucfirst($value->name) }}</option>
                    @endforeach
                    @endif()
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="residential_area_to" class="form-control" placeholder="Area / Building / Community*">
            </div>
        </div>
    </div>
</div>