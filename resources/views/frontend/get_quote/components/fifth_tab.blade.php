<div style="display: none;" class="show_services {{ create_slugify(get_services_name(5)) }}">
     <h4 class="service-title bottom">{{ get_services_name(5) }}</h4>

    <div class="moving-block">
        <div class="left-block block">
            <div class="form-group">
                <select name="cargo_country_from" class="select2-without-search" id="cargo_country_from" class="form-control">
                    <option value="">Select Country</option>
                        @if(isset($country_list))
                        @foreach($country_list as $key => $value)
                        @php
                        $selected = $value->id == '229' ? 'selected' :'' ; 
                        @endphp
                        <option value="{{ $value->id }}" {{ $selected }}>{{ ucfirst($value->name) }}</option>
                        @endforeach
                        @endif()
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="cargo_city_from" class="form-control" placeholder="Area / Building / Community*">
            </div>
        </div>
        <div class="right-block block">
            <div class="form-group">
                <select name="cargo_country_to" class="select2-without-search" id="cargo_country_to" class="form-control">
                    <option value="">Select Country</option>
                        @if(isset($country_list))
                        @foreach($country_list as $key => $value)
                        @php
                        $selected = $value->id == '229' ? 'selected' :'' ; 
                        @endphp
                        <option value="{{ $value->id }}" {{ $selected }}>{{ ucfirst($value->name) }}</option>
                        @endforeach
                        @endif()
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="cargo_city_to" class="form-control" placeholder="Area / Building / Community*">
            </div>
        </div>
    </div>
</div>
   