@extends('layouts.frontend')
@section('content')


<div class="common-banner get-quote-bannner">
    <h2 class="title">Get Quote</h2>
    <p class="bread-crumb">Home - Get Quote</p>
</div>

<div class="get-quote-page">
    <div class=" top-block  scroll-to">
        <div class="container">
            <ul class="nav get-qoute-nav-tabs nav-tabs" id="aboutTab" role="tablist">
                <li class="nav-item service_type" role="presentation"
                    service_type="{{ create_slugify(isset($home_page_data->first_service) ? $home_page_data->first_service : '')}}">
                    <a class="nav-link active btn-tab" data-aos="fade-up" data-aos-delay="200" data-toggle="tab"
                        href="#get-quote-tab-1" role="tab" aria-controls="home" aria-selected="true">
                        <img src="{{ URL::to($home_page_data->first_service_image) }}" class="tab-img" alt="">
                        <h2 class="tab-text">@php echo isset($home_page_data->first_service) ?
                            $home_page_data->first_service : '' ; @endphp</h2>
                    </a>
                </li>
                <li class="nav-item service_type" role="presentation"
                    service_type="{{ create_slugify(isset($home_page_data->second_service) ? $home_page_data->second_service : '')}}">
                    <a class="nav-link btn-tab" data-toggle="tab" href="#get-quote-tab-2" data-aos="fade-up"
                        data-aos-delay="300" role="tab" aria-controls="profile" aria-selected="false">
                        <img src="{{ URL::to($home_page_data->second_service_image) }}" class="tab-img" alt="">
                        <h2 class="tab-text">@php echo isset($home_page_data->second_service) ?
                            $home_page_data->second_service : '' ; @endphp</h2>
                    </a>
                </li>
                <li class="nav-item service_type" role="presentation"
                    service_type="{{ create_slugify(isset($home_page_data->third_service) ? $home_page_data->third_service : '')}}">
                    <a class="nav-link btn-tab" data-aos="fade-up" data-aos-delay="400" data-toggle="tab"
                        href="#get-quote-tab-3" role="tab" aria-controls="contact" aria-selected="false">
                        <img src="{{ URL::to($home_page_data->third_service_image) }}" class="tab-img" alt="">
                        <h2 class="tab-text">@php echo isset($home_page_data->third_service) ?
                            $home_page_data->third_service : '' ; @endphp</h2>
                    </a>
                </li>
                <li class="nav-item service_type" role="presentation"
                    service_type="{{ create_slugify(isset($home_page_data->fourth_service) ? $home_page_data->fourth_service : '')}}">
                    <a class="nav-link btn-tab" data-aos="fade-up" data-aos-delay="500" data-toggle="tab"
                        href="#get-quote-tab-4" role="tab" aria-controls="contact" aria-selected="false">
                        <img src="{{ URL::to($home_page_data->fourth_service_image) }}" class="tab-img" alt="">
                        <h2 class="tab-text">@php echo isset($home_page_data->fourth_service) ?
                            $home_page_data->fourth_service : '' ; @endphp</h5>
                        </h2>
                    </a>
                </li>
                <li class="nav-item service_type" role="presentation"
                    service_type="{{ create_slugify(isset($home_page_data->fifth_service) ? $home_page_data->fifth_service : '')}}">
                    <a class="nav-link btn-tab" data-aos="fade-up" data-aos-delay="600" data-toggle="tab"
                        href="#get-quote-tab-5" role="tab" aria-controls="contact" aria-selected="false">
                        <img src="{{ URL::to($home_page_data->fifth_service_image) }}" class="tab-img" alt="">
                        <h2 class="tab-text">@php echo isset($home_page_data->fifth_service) ?
                            $home_page_data->fifth_service : '' ; @endphp</h2>
                    </a>
                </li>
            </ul>

        </div>
    </div>


    @include('frontend.home.components.quote_form')
    </form>
</div>


@endsection
@section('scripts')
@parent

@endsection