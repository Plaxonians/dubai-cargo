@extends('layouts.frontend')
@section('content')


<div class="common-banner blogs-banner">
    <h2 class="title">Latest Updates</h2>
    <p class="bread-crumb">Home - News & Blogs</p>
    <a href="{{ URL::to(config('constants.book_an_appointment_link')) }}" class="floating-quote-btn">Book an
        Appointment</a>
</div>


<section class="blog blog-listing scroll-to">
    <div class="container">
        @if(isset($blog_data) && sizeof($blog_data) )
        <div class="blogs-row">
            @foreach($blog_data as $key => $value)
            <div class="blog-card">
                <div class="img-wrapper">
                    <div class="date">
                        <h2 class="day">{{ date('d',strtotime($value->created_at)) ?? '' }}</h2>
                        <p class="month">{{ date('M',strtotime($value->created_at)) ?? '' }}</p>
                    </div>
                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                        <img src="{{ URL::to($value->image) }}" alt="">
                    </a>
                </div>
                <div class="text-wrapper">
                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"
                        class="title">{{ $value->title ?? '' }}</a>
                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}" class="read-more-btn">Read More</a>
                </div>
            </div>
            @endforeach
        </div>

        <div class="cus-pagination">
            {!! $blog_data->links() !!}
        </div>

        @endif
    </div>

    @include('frontend.home.components.quote_form')
    @include('frontend.home.components.testimonial')


</section>


@endsection
@section('scripts')
@parent

@endsection