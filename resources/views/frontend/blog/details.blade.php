@extends('layouts.frontend')
@section('content')

<div class="common-banner blogs-banner">
    <h2 class="title"><b>{{ $blog_data->title ?? '' }}</b></h2>
    <p class="bread-crumb">Home - News & Blogs</p>
</div>


<section class="blog blog-details scroll-to">
    <div class="container">
        <div class="blog-banner">
            <img src="{{ URL::to($blog_data->image) }}" alt="">
        </div>
        <div class="content">
            @php echo $blog_data->description ?? '' @endphp
        </div>

        @if(isset($recent_blog_data) && !empty($recent_blog_data) )

        <div class="recent-articles">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="service-title">RECENT ARTICLE <span class="stop"></span></h2>
                </div>
                @foreach($recent_blog_data as $key => $value)
                <div class="col-md-6">
                    <div class="blog-card">
                        <div class="img-wrapper">
                            <div class="date">
                                <h2 class="day">{{ date('d',strtotime($value->created_at)) ?? '' }}</h2>
                                <p class="month">{{ date('M',strtotime($value->created_at)) ?? '' }}</p>
                            </div>
                            <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                                <img src="{{ URL::to($value->image) }}" alt="">
                            </a>
                        </div>
                        <div class="text-wrapper">
                            <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"
                                class="title">{{ $value->title ?? '' }}</a>
                            <p class="desc">{{ $value->sort_description ?? '' }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
</section>



@endsection
@section('scripts')
@parent

@endsection