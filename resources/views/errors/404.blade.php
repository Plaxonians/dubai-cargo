<!Doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="https://clientapp.brandmydream.com/dubai-cargo/images/logo.png" rel="icon" />
    <title>Dubai Cargo</title>
    <!-- <link rel="stylesheet" href="https://clientapp.brandmydream.com/red_trucking/css/frontend/main.css"> -->

    <style>
    @import url("https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap");

    * {
        margin: 0px;
        padding: 0px;
        -webkit-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        font-family: "Roboto", sans-serif;
        color: #1d355e;
    }

    .error-page {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        height: 100vh;
        text-align: center;
    }

    .hgroup h1 {
        font-size: 250px;
        line-height: 230px;
        color: #1d355e;
    }

    .hgroup h1 .fs01 {
        color: #1e84ff;
        display: inline-block;
        margin: 0 12px;
    }

    .hgroup h1 .fs02 {
        position: relative;
        display: inline-block;
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .hgroup h3 {
        font-size: 55px;
        font-family: 'Roboto';
        text-transform: uppercase;
        font-weight: bold;
    }



    .error-page p {
        font-size: 18px;
        padding: 10px 0 20px;
    }

    .error-page a {
        color: #1e84ff;
        text-decoration: none;
        background: #1e84ff;
        color: #fff;
        padding: 24px 43px;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border: 2px solid transparent;
        font-size: 18px;
        transition: all 0.3s;
        font-weight: 500;
        height: 75px;
        min-width: 200px;
        text-transform: uppercase;
        line-height: 27px;
    }

    .error-page a:hover {
        border: 2px solid #1e84ff;
        background: #fff;
        color: #1e84ff;
    }



    .container {
        width: 1024px;
        margin: 0 auto;
    }

    @media (max-width: 576px) {
        .hgroup h1 {
            font-size: 150px;
            line-height: 150px;
        }

        .hgroup h3 {
            font-size: 32px;
        }

        .error-page a {
            height: 60px;
            padding: 0 20px;
            font-weight: 400;
            letter-spacing: 1px;
        }

        .error-page {
            padding: 25px;
        }
    }
    </style>

</head>

<body>

    <div class="error-page">
        <div id="container" class="container">
            <!-- ###################### -->
            <div class="hgroup">
                <h1>4<span class="fs01">0</span><span class="fs02">4</span></h1>
                <h3><span>Page Not Found</span></h3>
            </div>
            <p>The page you're looking for doesn't exist or has been moved</p>
            <a href="{{ URL::to('/') }}" class="site-btn">GO TO HOMEPAGE</a>
            <!-- ###### -->
        </div>
    </div>

</body>