@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>{{ config('constants.website_name') }} Banners</b></h4>
   </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/banner/update') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($banner_data->meta_data) && !empty($banner_data->meta_data) ? json_decode($banner_data->meta_data) : array() ;
            @endphp 
          
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : '' }}">
                <label for="name">Sub Title*</label>
                <input type="text" id="sub_title" name="sub_title" class="form-control" value="{{ old('sub_title', isset($data->sub_title) ? $data->sub_title : '') }}">
                @if($errors->has('sub_title'))
                    <p class="help-block">
                        {{ $errors->first('sub_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('left_image') ? 'hass-error' : '' }}">
                <label for="left_image">Left Image</label> 

                @if(isset($data->left_image) && file_exists($data->left_image))
                
                <img src="{{ URL::to($data->left_image) }}" height="100" width="100">
                <input type="hidden" name="old_left_image" value="{{ $data->left_image }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 203x451 pixels.</p>
                <input type="file" name="left_image" accept="image/*">

                @if($errors->has('left_image'))
                    <p class="help-block">
                        {{ $errors->first('left_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('left_top_image') ? 'hass-error' : '' }}">
                <label for="left_top_image">Left Top Image</label> 

                @if(isset($data->left_top_image) && file_exists($data->left_top_image))
                
                <img src="{{ URL::to($data->left_top_image) }}" height="100" width="100">
                <input type="hidden" name="old_left_top_image" value="{{ $data->left_top_image }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 100x160 pixels.</p>
                <input type="file" name="left_top_image" accept="image/*">

                @if($errors->has('left_top_image'))
                    <p class="help-block">
                        {{ $errors->first('left_top_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('right_image') ? 'hass-error' : '' }}">
                <label for="right_image">Right Image</label> 

                @if(isset($data->right_image) && file_exists($data->right_image))
                
                <img src="{{ URL::to($data->right_image) }}" height="100" width="100">
                   <input type="hidden" name="old_right_image" value="{{ $data->right_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 388x375 pixels.</p>
                <input type="file" name="right_image" accept="image/*">

                @if($errors->has('right_image'))
                    <p class="help-block">
                        {{ $errors->first('right_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('right_top_image') ? 'hass-error' : '' }}">
                <label for="right_top_image">Right Top Image</label> 

                @if(isset($data->right_top_image) && file_exists($data->right_top_image))
                
                <img src="{{ URL::to($data->right_top_image) }}" height="100" width="100">
                <input type="hidden" name="old_right_top_image" value="{{ $data->right_top_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 100x151 pixels.</p>
                <input type="file" name="right_top_image" accept="image/*">

                @if($errors->has('right_top_image'))
                    <p class="help-block">
                        {{ $errors->first('right_top_image') }}
                    </p>
                @endif
                
            </div>
             
            <div>
                <input type="hidden" name="id" value="{{ isset($banner_data->id) ? $banner_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>


@endsection
@section('scripts')
@parent

@endsection
