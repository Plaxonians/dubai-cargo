@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>{{ get_services_name(4)}}</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/services/updatestorage') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($service_data->meta_data) && !empty($service_data->meta_data) ? json_decode($service_data->meta_data) : array() ;
            @endphp 
           

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="title" name="title" class="form-control validate[required]" value="{{ old('title', isset($data->title) ? $data->title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : '' }}">
                <label for="name">Sub Title*</label>
                <input type="text" id="sub_title" name="sub_title" class="form-control validate[required]" value="{{ old('sub_title', isset($data->sub_title) ? $data->sub_title : '') }}">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('sub_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('detail') ? 'has-error' : '' }}">
                <label for="detail">Detail *</label>
                <textarea id="detail" name="detail" class="form-control validate[required] ">{{ old('detail', isset($data->detail) ? $data->detail : '') }}</textarea>
                @if($errors->has('detail'))
                    <p class="help-block">
                        {{ $errors->first('detail') }}
                    </p>
                @endif
                
            </div>

          
            @php
            $features = isset($data->features) && !empty($data->features) ? ($data->features) : array() ;
            @endphp
            @if(is_array($features) && sizeof($features) == 0 )
            <div class="form-group feature_remove_div  {{ $errors->has('section_three_features') ? 'has-error' : '' }}">
                <label for="name">Feature*</label>
                <input type="text" id="features" name="features[]" class="form-control validate[required]" value="">
            </div>
            @endif

            <div id="section_two_feature_result">
                @foreach($features as $p_key => $value)
                <div class="form-group feature_remove_div remove_div{{$p_key+1}}" >
                    <label for="name"> Feature*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$p_key+1}}"> Remove</button>
                    <input type="text"  name="features[]" class="form-control validate[required]" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('features[]'))
                    <p class="help-block">
                        {{ $errors->first('features[]') }}
                    </p>
            @endif

            <button class="btn btn-info section_two_feature" type="button">Add More Service Feature</button>
            <br />
            <br />
            

            <div class="form-group {{ $errors->has('section_three_image') ? 'has-error' : '' }}">
                            <label for="name"> Image *</label>
                             @if(isset($data->image) && file_exists($data->image))
                            <img src="{{ URL::to($data->image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_image" value="{{ $data->image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 160X140 pixels.</p>
                            <input class="{{isset($data->image) && file_exists($data->image) ?'' : 'validate[required]'}}"  type="file" name="image" accept="image/*">
                            @if($errors->has('image'))
                            <p class="help-block">
                                {{ $errors->first('image') }}
                            </p>
                            @endif
                </div>

            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($service_data->id) ? $service_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection