@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Home Page</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/homepage/update') }}" method="POST" enctype="multipart/form-data">
            @csrf

           
            @php
            $data = isset($home_page_meta_data->meta_data) && !empty($home_page_meta_data->meta_data) ? json_decode($home_page_meta_data->meta_data) : array() ;
            @endphp 

            <label>Services</label>
             <div class="form-group {{ $errors->has('service_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="service_title" name="service_title" class="form-control" value="{{ old('service_title', isset($data->service_title) ? $data->service_title : '') }}">
                @if($errors->has('service_title'))
                    <p class="help-block">
                        {{ $errors->first('service_title') }}
                    </p>
                @endif
                
            </div>

         
             <div class="form-group {{ $errors->has('service_content') ? 'has-error' : '' }}">
                <label for="service_content">Sort Content *</label>
                <textarea rows="10"  id="service_content" name="service_content" class="form-control ">{{ old('service_content', isset($data) && !empty($data->service_content) ? $data->service_content : '') }}</textarea>
                @if($errors->has('service_content'))
                    <p class="help-block">
                        {{ $errors->first('service_content') }}
                    </p>
                @endif
                
            </div>


             <div class="form-group {{ $errors->has('first_service') ? 'has-error' : '' }}">
                <label for="name">First Service*</label>
                <input type="text" id="first_service" name="first_service" class="form-control" value="{{ old('first_service', isset($data->first_service) ? get_services_name(1) : get_services_name(1)) }}" readonly="readonly">
                @if($errors->has('first_service'))
                    <p class="help-block">
                        {{ $errors->first('first_service') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('first_service_image') ? 'hass-error' : '' }}">
                <label for="first_service_image">  Image</label> 

                @if(isset($data->first_service_image) && file_exists($data->first_service_image))
                
                <img src="{{ URL::to($data->first_service_image) }}" height="100" width="100">
                   <input type="hidden" name="old_first_service_image" value="{{ $data->first_service_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 160x160 pixels.</p>
                <input type="file" name="first_service_image" accept="image/*">

                @if($errors->has('first_service_image'))
                    <p class="help-block">
                        {{ $errors->first('first_service_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('second_service') ? 'has-error' : '' }}">
                <label for="name">Second Service*</label>
                <input type="text" id="second_service" name="second_service" class="form-control" value="{{ old('second_service', isset($data->second_service) ? get_services_name(2) : get_services_name(2)) }}" readonly="readonly">
                @if($errors->has('second_service'))
                    <p class="help-block">
                        {{ $errors->first('second_service') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('second_service_image') ? 'hass-error' : '' }}">
                <label for="first_service_image">  Image</label> 

                @if(isset($data->second_service_image) && file_exists($data->second_service_image))
                
                <img src="{{ URL::to($data->second_service_image) }}" height="100" width="100">
               <input type="hidden" name="old_second_service_image" value="{{ $data->second_service_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 160x143 pixels.</p>
                <input type="file" name="second_service_image" accept="image/*">

                @if($errors->has('second_service_image'))
                    <p class="help-block">
                        {{ $errors->first('second_service_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('third_service') ? 'has-error' : '' }}">
                <label for="name">Third Service*</label>
                <input type="text" id="third_service" name="third_service" class="form-control" value="{{ old('third_service', isset($data->third_service) ? get_services_name(4) : get_services_name(4)) }}" readonly="readonly">
                @if($errors->has('third_service'))
                    <p class="help-block">
                        {{ $errors->first('third_service') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('third_service_image') ? 'hass-error' : '' }}">
                <label for="third_service_image">  Image</label> 

                @if(isset($data->third_service_image) && file_exists($data->third_service_image))
                
                <img src="{{ URL::to($data->third_service_image) }}" height="100" width="100">
                <input type="hidden" name="old_third_service_image" value="{{ $data->third_service_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 160x160 pixels.</p>
                <input type="file" name="third_service_image" accept="image/*">

                @if($errors->has('third_service_image'))
                    <p class="help-block">
                        {{ $errors->first('third_service_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('fourth_service') ? 'has-error' : '' }}">
                <label for="name">Fourth Service*</label>
                <input type="text" id="fourth_service" name="fourth_service" class="form-control" value="{{ old('fourth_service', isset($data->fourth_service) ? get_services_name(3) : get_services_name(3)) }}" readonly="readonly">
                @if($errors->has('fourth_service'))
                    <p class="help-block">
                        {{ $errors->first('fourth_service') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('fourth_service_image') ? 'hass-error' : '' }}">
                <label for="fourth_service_image">  Image</label> 

                @if(isset($data->fourth_service_image) && file_exists($data->fourth_service_image))
                
                <img src="{{ URL::to($data->fourth_service_image) }}" height="100" width="100">
                   <input type="hidden" name="old_fourth_service_image" value="{{ $data->fourth_service_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 160x160 pixels.</p>
                <input type="file" name="fourth_service_image" accept="image/*">

                @if($errors->has('fourth_service_image'))
                    <p class="help-block">
                        {{ $errors->first('fourth_service_image') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('fifth_service') ? 'has-error' : '' }}">
                <label for="name">Fifth Service*</label>
                <input type="text" id="fifth_service" name="fifth_service" class="form-control" value="{{ old('fifth_service', isset($data->fifth_service) ? get_services_name(5) : get_services_name(5)) }}" readonly="readonly">
                @if($errors->has('fifth_service'))
                    <p class="help-block">
                        {{ $errors->first('fifth_service') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('fifth_service_image') ? 'hass-error' : '' }}">
                <label for="fifth_service_image">  Image</label> 

                @if(isset($data->fifth_service_image) && file_exists($data->fifth_service_image))
                
                <img src="{{ URL::to($data->fifth_service_image) }}" height="100" width="100">
                <input type="hidden" name="old_fifth_service_image" value="{{ $data->fifth_service_image }}">
                @endif

                <br/>
                <p>Note : Image dimension should be within min 160x160 pixels.</p>
                <input type="file" name="fifth_service_image" accept="image/*">

                @if($errors->has('fifth_service_image'))
                    <p class="help-block">
                        {{ $errors->first('fifth_service_image') }}
                    </p>
                @endif
                
            </div>


            <hr/>
            <label>About Us</label>

             <div class="form-group {{ $errors->has('about_us_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="about_us_title" name="about_us_title" class="form-control" value="{{ old('about_us_title', isset($data->about_us_title) ? $data->about_us_title : '') }}">
                @if($errors->has('about_us_title'))
                    <p class="help-block">
                        {{ $errors->first('about_us_title') }}
                    </p>
                @endif
                
            </div>

         
             <div class="form-group {{ $errors->has('about_us_content') ? 'has-error' : '' }}">
                <label for="about_us_content">Sort Content *</label>
                <textarea rows="10"  id="about_us_content" name="about_us_content" class="form-control ">{{ old('about_us_content', isset($data) && !empty($data->about_us_content) ? $data->about_us_content : '') }}</textarea>
                @if($errors->has('about_us_content'))
                    <p class="help-block">
                        {{ $errors->first('about_us_content') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('about_us_image') ? 'hass-error' : '' }}">
                <label for="about_us_image"> Upload Image</label> 

                @if(isset($data->about_us_image) && file_exists($data->about_us_image))
                <img src="{{ URL::to($data->about_us_image) }}" height="100" width="100">
                <input type="hidden" name="old_about_us_image" value="{{ $data->about_us_image }}">
                @endif

               

                <br/>
                <p>Note : Image dimension should be within min 575x386 pixels.</p>
                <input type="file" name="about_us_image" accept="image/*">

                @if($errors->has('about_us_image'))
                    <p class="help-block">
                        {{ $errors->first('about_us_image') }}
                    </p>
                @endif
                
            </div>



            <div>
                 <input type="hidden" name="id" value="{{ isset($home_page_meta_data->id) ? $home_page_meta_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection