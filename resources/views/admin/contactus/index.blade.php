@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Contact Us</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/contactus/update') }}" method="POST" enctype="multipart/form-data">
            @csrf
           
            @php
            $contact_us_meta = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : array() ;
            @endphp 

            

           
            
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                <label for="phone">Phone No*</label>
                <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone', isset($contact_us_meta->phone) ? $contact_us_meta->phone : '') }}">
                @if($errors->has('phone'))
                    <p class="help-block">
                        {{ $errors->first('phone') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('landline_no') ? 'has-error' : '' }}">
                <label for="landline_no">Landline No*</label>
                <input type="text" id="landline_no" name="landline_no" class="form-control" value="{{ old('landline_no', isset($contact_us_meta->landline_no) ? $contact_us_meta->landline_no : '') }}">
                @if($errors->has('landline_no'))
                    <p class="help-block">
                        {{ $errors->first('landline_no') }}
                    </p>
                @endif
                
            </div>

            

            <hr/>

             <label for="address_one_location_name">Address </label>

            <div class="form-group {{ $errors->has('address_one_location_name') ? 'has-error' : '' }}">
                <label for="address_one_location_name">Location Name*</label>
                <input type="text" id="address_one_location_name" name="address_one_location_name" class="form-control" value="{{ old('address_one_location_name', isset($contact_us_meta->address_one_location_name) ? $contact_us_meta->address_one_location_name : '') }}">
                @if($errors->has('address_one_location_name'))
                    <p class="help-block">
                        {{ $errors->first('address_one_location_name') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('address_one_address') ? 'has-error' : '' }}">
                <label for="address_one_address">Address*</label>
                <input type="text" id="address_one_address" name="address_one_address" class="form-control" value="{{ old('address_one_address', isset($contact_us_meta->address_one_address) ? $contact_us_meta->address_one_address : '') }}">
                @if($errors->has('address_one_address'))
                    <p class="help-block">
                        {{ $errors->first('address_one_address') }}
                    </p>
                @endif
                
            </div>

            <hr/>
            <label for="designation">Google Location Map</label>

           
            <div class="form-group {{ $errors->has('address_one_embed_map') ? 'has-error' : '' }}">
                <label for="address_one_embed_map">Embed Map *</label>
                <textarea id="address_one_embed_map" name="address_one_embed_map" class="form-control">{{ old('address_one_embed_map', isset($contact_us_meta->address_one_embed_map) ? $contact_us_meta->address_one_embed_map : '') }}</textarea>
                
                @if($errors->has('address_one_embed_map'))
                    <p class="help-block">
                        {{ $errors->first('address_one_embed_map') }}
                    </p>
                @endif
                
            </div>


            <!-- <hr/>

             <label for="address_two_location_name">Address 2</label>

            <div class="form-group {{ $errors->has('address_two_location_name') ? 'has-error' : '' }}">
                <label for="address_two_location_name">Location Name*</label>
                <input type="text" id="address_two_location_name" name="address_two_location_name" class="form-control" value="{{ old('address_two_location_name', isset($contact_us_meta->address_two_location_name) ? $contact_us_meta->address_two_location_name : '') }}">
                @if($errors->has('address_two_location_name'))
                    <p class="help-block">
                        {{ $errors->first('address_two_location_name') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('address_two_address') ? 'has-error' : '' }}">
                <label for="address_two_address">Address*</label>
                <input type="text" id="address_two_address" name="address_two_address" class="form-control" value="{{ old('address_two_address', isset($contact_us_meta->address_two_address) ? $contact_us_meta->address_two_address : '') }}">
                @if($errors->has('address_two_address'))
                    <p class="help-block">
                        {{ $errors->first('address_two_address') }}
                    </p>
                @endif
                
            </div>


            <hr/>
            <label for="designation">Google Location Map</label>

           
            <div class="form-group {{ $errors->has('address_two_embed_map') ? 'has-error' : '' }}">
                <label for="address_two_embed_map">Embed Map *</label>
                <textarea id="address_two_embed_map" name="address_two_embed_map" class="form-control">{{ old('address_two_embed_map', isset($contact_us_meta->address_two_embed_map) ? $contact_us_meta->address_two_embed_map : '') }}</textarea>
                
                @if($errors->has('address_two_embed_map'))
                    <p class="help-block">
                        {{ $errors->first('address_two_embed_map') }}
                    </p>
                @endif
                
            </div> -->

           

           

            
            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($contact_us_data->id) ? $contact_us_data->id:'0' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection