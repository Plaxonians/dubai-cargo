@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Gallery</b></h4>
   </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/gallery/update') }}" method="POST" enctype="multipart/form-data" class="validate_form">
            @csrf
            <div class="form-group">
                <div id="add_more_result">

                    <div  class="remove_div remove_div1" >
                            <div class="float-right">
                                 <button type="button" class="add_more_remove " banner_id="" id="remove_banner1"  counter="1"><i class="fa fa-times"></i></button>
                            </div>
                             <br/>


                        <div class="form-group error_wrapper div_'+counter+'">
                            <label for="name"> Image </label> <br/>
                            <p>Note : Image dimension should be within min 752x752 pixels.</p>
                            <input class="gallery_image validate[required,funcCall[validate_gallery_image]]"  type="file" id="gallery_image1" counter="1" name="gallery[gallery_image][1]" accept="image/*" >
                        </div>

                    </div>

                </div>                   
               <input type="button" class="add_more_btn btn btn-info" name="add_more" id="add_more_red_banner" value="Add More Gallery">
            </div>
 
            <div>
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection