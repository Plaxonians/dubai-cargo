@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Gallery Listing</b></h4>
    </div>
        <div class="card-body p-0">
           @if(!empty($gallery_data) && sizeof($gallery_data))
                <div class="row">
                 @foreach($gallery_data as $key => $value)
                  <div class="card col-md-3" style="position: relative;">
                    <img class="card-img-top" src="{{ URL::to($value->image) }}" height="200" width="250">

                    <div class="card-body" style="position: absolute;top: 0px;right: 0;">
                      <a href="javascript:" class="btn btn-danger gallery_delete" title="Delete" delete_gallery_url="{{ URL::to('admin/gallery/delete/'.$value->id) }}"><i class="fas fa-trash"></i></a>
                    </div>
                   
                  </div>
                 @endforeach

                @else
                    <div align="center">
                      <h3 align="center">No Record found !</h3>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $gallery_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection