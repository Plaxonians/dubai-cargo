@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Get Quote Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
          

           @if(!empty($form_data) && sizeof($form_data))
            <!-- <div class="float-right">
              <a href="{{ URL::to('admin/form/quote/export') }}" class="btn btn-info">Export to CSV</a>          
            </div> -->

                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Service_Date</th> 
                      <th>Service_Type</th>
                      <th>City From</th>
                      <th>City To</th>
                      <th>Area/Building/Community</th>
                      <th>Area/Building/Community</th>
                      <th>Furniture Detail</th>
                      <th>Action</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ $value->feedback_date ? date('d-M-Y',strtotime($value->feedback_date)) : '' }}</td>
                      <td>{{ get_services_name($value->service_type) ?? '' }} </td>
                      <td>
                        @php 
                        if($value->service_type == 5)
                        {
                          echo get_country_name_by_id($value->city_from) ;
                        }
                        else
                        {
                           echo get_state_name_by_id($value->city_from) ;
                        }
                        @endphp
                      </td>
                      <td>
                        @php 
                        if($value->service_type == 5)
                        {
                          echo get_country_name_by_id($value->city_to) ;
                        }
                        else
                        {
                           echo get_state_name_by_id($value->city_to) ;
                        }
                        @endphp
                      </td>
                      <td>{{ $value->area_from ?? '' }}</td>
                      <td>{{ $value->area_to ?? '' }}</td>
                      <td>{{ $value->furniture_detail ?? '' }}</td>
                     
                      <td><a href="{{ URL::to('admin/form/quoteview/'.$value->id) }}" class="btn btn-success" title="View"><i class="fas fa fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                   <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection