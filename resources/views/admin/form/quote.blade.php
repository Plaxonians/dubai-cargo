@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Quote Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
          

           @if(!empty($form_data) && sizeof($form_data))
            <!-- <div class="float-right">
              <a href="{{ URL::to('admin/form/quote/export') }}" class="btn btn-info">Export to CSV</a>          
            </div> -->

                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Quote_Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone_No</th>
                      <th>Area_name/Villa/Apartment/Flat_no</th>
                      <th>Nearest_Landmark</th>
                      <th>Address</th>
                      <th>Message</th>
                      <th>Upload_File</th>
                      <th>Action</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ $value->created_at ? date('d-M-Y',strtotime($value->created_at)) : '' }}</td>
                      <td>{{ $value->first_name ?? '' }} {{ $value->last_name ?? '' }}</td>
                      <td>{{ $value->email ?? '' }}</td>
                      <td>{{ $value->phone ?? '' }}</td>
                      <td>{{ $value->area_name ?? '' }}</td>
                      <td>{{ $value->nearest_landmark ?? '' }}</td>
                      <td>{{ $value->address ?? '' }}</td>
                      <td>{{ $value->message ?? '' }}</td>
                      <td>
                        @if($value->upload_file && !empty($value->upload_file) && file_exists($value->upload_file))
                        <a href="{{URL::to($value->upload_file)}}" target="_blank">View File</a>
                        @else
                        NA
                        @endif

                       </td>
                      <td><a href="{{ URL::to('admin/form/quoteview/'.$value->id) }}" class="btn btn-success" title="View"><i class="fas fa fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                   <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection