@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Client Feedback Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
          

           @if(!empty($form_data) && sizeof($form_data))
            <!-- <div class="float-right">
              <a href="{{ URL::to('admin/form/quote/export') }}" class="btn btn-info">Export to CSV</a>          
            </div> -->

                <table class="table table-bordered">
                  <thead>
                    <tr>
                      
                      <th>Service_Type</th>
                      <th>Feedback_Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone_No</th>
                      <th>Message</th>
                      <th>Upload_File</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ get_services_name($value->service_type) ?? '' }} </td>
                      <td>{{ $value->feedback_date ? date('d-M-Y',strtotime($value->feedback_date)) : '' }}</td>
                      <td>{{ $value->first_name ?? '' }} </td>
                      <td>{{ $value->email ?? '' }}</td>
                      <td>{{ $value->phone ?? '' }}</td>
                      <td>{{ $value->message ?? '' }}</td>
                      <td>
                        @if($value->upload_file && !empty($value->upload_file) && file_exists($value->upload_file))
                        <a href="{{URL::to($value->upload_file)}}" target="_blank">View File</a>
                        @else
                        NA
                        @endif

                       </td>
                     
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                   <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection