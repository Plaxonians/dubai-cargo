@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Quote Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
          

           @if(!empty($form_data) && sizeof($form_data))
            <div class="float-right">
              <a href="{{ URL::to('admin/form/career/export') }}" class="btn btn-info">Export to CSV</a>          
            </div>

                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Query_Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone_No</th>
                      <!-- <th>Subject</th> -->
                      <th>CDL</th>
                      <th>TWICE</th>
                      <th>2 Year/Above_Experience</th>
                      <th>INMOTO or DRAYAGE </th>
                      <th>Upload_File</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ $value->created_at ? date('d-M-Y',strtotime($value->created_at)) : '' }}</td>
                      <td>{{ $value->first_name ?? '' }} {{ $value->last_name ?? '' }}</td>
                      <td>{{ $value->email ?? '' }}</td>
                      <td>{{ $value->phone ?? '' }}</td>
                     <!--  <td>{{ $value->subject ?? '' }}</td> -->
                      <td>{{ $value->is_cdl && !empty($value->is_cdl) ? 'Yes' :'No'  }}</td>
                      <td>{{ $value->is_twice && !empty($value->is_twice) ? 'Yes' :'No'  }}</td>
                      <td>{{ $value->is_driving_experience && !empty($value->is_driving_experience) ? 'Yes' :'No'  }}</td>
                      <td>{{ $value->is_inmoto_or_drayage && !empty($value->is_inmoto_or_drayage) ? 'Yes' :'No'  }}</td>
                      <td>
                        @if($value->upload_file && !empty($value->upload_file) && file_exists($value->upload_file))
                        <a href="{{URL::to($value->upload_file)}}" target="_blank">View File</a>
                        @else
                        NA
                        @endif

                       </td>
                      
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                   <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection