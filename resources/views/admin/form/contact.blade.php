@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Contact Forms</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
           @if(!empty($form_data) && sizeof($form_data))
           <!-- <div class="float-right">
              <a href="{{ URL::to('admin/form/contact/export') }}" class="btn btn-info">Export to CSV</a>          
            </div> -->

                <table class="table table-responsive table-bordered">
                  <thead>
                    <tr>
                       <th>Query_Date</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Contact number</th>
                      <th>Message</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($form_data as $key => $value)
                    <tr>
                      <td>{{ $value->created_at ? date('d-M-Y',strtotime($value->created_at)) : '' }}</td>
                      <td>{{ $value->first_name ?? '' }} {{ $value->last_name ?? '' }}</td>
                      <td>{{ $value->email ?? '' }}</td>
                      <td>{{ $value->phone ?? '' }}</td>
                      <td>{{ $value->message ?? '' }}</td>
                      
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                    <div align="center">
                      <h2>No Record found !</h2>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $form_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection