@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Quote View</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
          

                <table class="table table-bordered">
                  <thead>
                    @if($form_data->form_type == config('constants.form_type.get_quote') )
                    <tr>
                      <th>Service Date</th>
                      <td>{{ $form_data->feedback_date ? date('d-M-Y',strtotime($form_data->feedback_date)) : '' }}</td>
                    </tr>
                    <tr>
                      <th>Service Type</th>
                      <td>{{ get_services_name($form_data->service_type) ?? '' }}</td>
                    </tr>
                    <tr>
                      <th>City From</th>
                      <td>@php 
                        if($form_data->service_type == 5)
                        {
                          echo get_country_name_by_id($form_data->city_from) ;
                        }
                        else
                        {
                           echo get_state_name_by_id($form_data->city_from) ;
                        }
                        @endphp</td>
                    </tr>
                    <tr>
                      <th>City To</th>
                      <td>@php 
                        if($form_data->service_type == 5)
                        {
                          echo get_country_name_by_id($form_data->city_to) ;
                        }
                        else
                        {
                           echo get_state_name_by_id($form_data->city_to) ;
                        }
                        @endphp</td>
                    </tr>
                    <tr>
                      <th>Area/Building/Community</th>
                      <td>{{ $form_data->area_from ?? '' }}</td>
                    </tr>
                    <tr>
                      <th>Area/Building/Community</th>
                      <td>{{ $form_data->area_to ?? '' }}</td>
                    </tr>
                    <tr>
                      <th>Furniture Detail</th>
                      <td>{{ $form_data->furniture_detail ?? '' }}</td>
                    </tr>
                    @else
                    <tr>
                      <th>Quote Date </th>
                      <td>{{ $form_data->created_at ? date('d-M-Y',strtotime($form_data->created_at)) : '' }}</td>
                    </tr>
                    @endif
                    
                     <tr>
                       <th>Name</th>
                      <td>{{ $form_data->first_name ?? '' }} {{ $form_data->last_name ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Email</th>
                      <td>{{ $form_data->email ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Phone No</th>
                      <td>{{ $form_data->phone ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Area name / Villa / Apartment / Flat no</th>
                      <td>{{ $form_data->area_name ?? '' }} </td>
                    </tr>
                    <tr>
                       <th>Nearest Landmark</th>
                      <td>{{ $form_data->nearest_landmark ?? '' }} </td>
                    </tr>
                    <tr>
                       <th>Address</th>
                      <td>{{ $form_data->address ?? '' }} </td>
                    </tr>
                    <tr>
                       <th>Message</th>
                      <td>{{ $form_data->message ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Upload File</th>
                      <td>
                        @if($form_data->upload_file && !empty($form_data->upload_file) && file_exists($form_data->upload_file))
                        <a href="{{URL::to($form_data->upload_file)}}" target="_blank">View File</a>
                        @else
                        NA
                        @endif
                      </td>
                    </tr>
                    <tr>
                       <th>Proposer</th>
                      <td>{{ $form_data->proposer ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Address Origin</th>
                      <td>{{ $form_data->address_origin ?? '' }} </td>
                    </tr>
                    <tr>
                       <th>Address at Destination</th>
                      <td>{{ $form_data->address_destination ?? '' }}</td>
                    </tr>

                    <tr>
                       <th>Shipped by</th>
                      <td>
                        @if($form_data->shipped_by==1)
                        {{ 'Sea' }}
                        @elseif($form_data->shipped_by == 2)
                        {{ 'Air' }}
                        @elseif($form_data->shipped_by == 3)
                        {{ 'Over Land' }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                       <th>Packing</th>
                      <td>
                        @if($form_data->packing==1)
                        {{ 'Professional' }}
                        @elseif($form_data->packing == 2)
                        {{ 'Self' }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                       <th>Approx. commencement date of transit</th>
                      <td>{{ $form_data->date_of_transit ?? '' }} </td>
                    </tr>
                    <tr>
                       <th>No of Packages</th>
                      <td>{{ $form_data->number_of_package ?? '' }}</td>
                    </tr>
                    <tr>
                       <th>Packing Company's Name</th>
                      <td>{{ $form_data->packing_name ?? '' }}</td>
                    </tr>

                    <tr>
                       <th>LIVING ROOM</th>
                      <td>
                          <table>
                            @php
                            $living_room = !empty($form_data->living_room) ? json_decode($form_data->living_room,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($living_room as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'sofas' )
                               {{ 'Sofas' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'lamps_and_shades')
                               {{ 'Lamps & Shades' }}
                               @elseif($key == 'tables')
                               {{ 'Tables' }}
                               @elseif($key == 'radios')
                               {{ 'Radios' }}
                               @elseif($key == 'tvs')
                               {{ 'TVs' }}
                               @elseif($key == 'rugs_and_carpets')
                               {{ 'Rugs & Carpets ' }}
                               @elseif($key == 'curtains')
                               {{ 'Curtains' }}
                               @elseif($key == 'piano_and_musical_instruments')
                               {{ 'Piano & Musical Instruments' }}
                               @elseif($key == 'bookcase_wall_unit')
                               {{ 'Bookcase / Wall Unit' }}
                               @elseif($key == 'cupboards')
                               {{ 'Cupboards' }}
                               @elseif($key == 'hi_fi_system_home_theatre')
                               {{ 'Hi-Fi system/Home Theatre' }}
                               @elseif($key == 'pictures_and_paintings')
                               {{ 'Pictures & Paintings' }}
                                @elseif($key == 'video_recorder_dvd')
                               {{ 'Video Recorder/DVD' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>DINING ROOM</th>
                      <td>
                          <table>
                            @php
                            $dining_room = !empty($form_data->dining_room) ? json_decode($form_data->dining_room,true) : []  ;
                            
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($dining_room as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'table' )
                               {{ 'Tables' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'cupboards')
                               {{ 'Cupboards' }}
                               @elseif($key == 'sideboard')
                               {{ 'Sideboard' }}
                               @elseif($key == 'lamps_and_chandeliers')
                               {{ 'Lamps & Chandeliers' }}
                               @elseif($key == 'rugs_and_carpets')
                               {{ 'Rugs & Carpets' }}
                               @elseif($key == 'curtains')
                               {{ 'Curtains ' }}
                               @elseif($key == 'mirrors')
                               {{ 'Mirrors' }}
                               @elseif($key == 'table_linen')
                               {{ 'Table Linen' }}
                               @elseif($key == 'pictures_and_paintings')
                               {{ 'Pictures & Paintings' }}
                               @elseif($key == 'desk')
                               {{ 'Desk' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>FAMILY ROOM</th>
                      <td>
                          <table>
                            @php
                            $family_room = !empty($form_data->family_room) ? json_decode($form_data->family_room,true) : []  ;
                          
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($family_room as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'tables' )
                               {{ 'Tables' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'cupboards')
                               {{ 'Cupboards' }}
                               @elseif($key == 'sideboard')
                               {{ 'Sideboard' }}
                               @elseif($key == 'lamps_and_chandeliers')
                               {{ 'Lamps & Chandeliers' }}
                               @elseif($key == 'rugs_and_carpets')
                               {{ 'Rugs & Carpets' }}
                               @elseif($key == 'curtains')
                               {{ 'Curtains ' }}
                               @elseif($key == 'mirrors')
                               {{ 'Mirrors' }}
                               @elseif($key == 'table_linen')
                               {{ 'Table Linen' }}
                               @elseif($key == 'pictures_and_paintings')
                               {{ 'Pictures & Paintings' }}
                               @elseif($key == 'desk')
                               {{ 'Desk' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>KITCHEN</th>
                      <td>
                          <table>
                            @php
                            $kitchen = !empty($form_data->kitchen) ? json_decode($form_data->kitchen,true) : []  ;
                            
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($kitchen as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'tables' )
                               {{ 'Tables' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'electrical_and_appliances')
                               {{ 'Electrical Appliances' }}
                               @elseif($key == 'cabinets')
                               {{ 'Cabinets' }}
                               @elseif($key == 'kitchen_linen')
                               {{ 'Kitchen Linen' }}
                               @elseif($key == 'dishwasher')
                               {{ 'Dishwasher' }}
                               @elseif($key == 'oven_cooker')
                               {{ 'Oven/Cooker ' }}
                               @elseif($key == 'microwave')
                               {{ 'Microwave' }}
                               @elseif($key == 'crockery')
                               {{ 'Crockery' }}
                               @elseif($key == 'utensils_cutlery')
                               {{ 'Utensils / Cutlery' }}
                               @elseif($key == 'pots_pans')
                               {{ 'Pots & Pans' }}
                               @elseif($key == 'bowls_trays_etc')
                               {{ 'Bowls, Trays Etc' }}
                               @elseif($key == 'plastic_tupperware')
                               {{ 'Plastic/Tupperware ' }}
                               @elseif($key == 'refrigerator')
                               {{ 'Refrigerator' }}
                               @elseif($key == 'freezer')
                               {{ 'Freezer' }}
                               @elseif($key == 'vacuum_cleaner')
                               {{ 'Vacuum Cleaner' }}
                               @elseif($key == 'washing_machine')
                               {{ 'Washing Machine' }}
                               @elseif($key == 'dryer')
                               {{ 'Dryer' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>CHINAWARE/PORCELAIN</th>
                      <td>
                          <table>
                            @php
                            $chinaware_porcelatin = !empty($form_data->chinaware_porcelatin) ? json_decode($form_data->chinaware_porcelatin,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $chinaware_porcelatin["'items'"] ?? '' }}</td>
                               <td>{{ $chinaware_porcelatin["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>
                    <tr>
                       <th>CRYSTAL/GLASSWARE</th>
                      <td>
                          <table>
                            @php
                            $crystal_glassware = !empty($form_data->crystal_glassware) ? json_decode($form_data->crystal_glassware,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $crystal_glassware["'items'"] ?? '' }}</td>
                               <td>{{ $crystal_glassware["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>
                    <tr>
                       <th>SILVERWARE/BRASSWARE</th>
                      <td>
                          <table>
                            @php
                            $silverware_brassware = !empty($form_data->silverware_brassware) ? json_decode($form_data->silverware_brassware,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $silverware_brassware["'items'"] ?? '' }}</td>
                               <td>{{ $silverware_brassware["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>ANTIQUES / WORKS OF ART</th>
                      <td>
                          <table>
                            @php
                            $antiques_works_of_art = !empty($form_data->antiques_works_of_art) ? json_decode($form_data->antiques_works_of_art,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $antiques_works_of_art["'items'"] ?? '' }}</td>
                               <td>{{ $antiques_works_of_art["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>SPORTS EQUIPMENT</th>
                      <td>
                          <table>
                            @php
                            $sports_equipment = !empty($form_data->sports_equipment) ? json_decode($form_data->sports_equipment,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $sports_equipment["'items'"] ?? '' }}</td>
                               <td>{{ $sports_equipment["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>LINEN / CLOTHING</th>
                      <td>
                          <table>
                            @php
                            $linen_clothing = !empty($form_data->linen_clothing) ? json_decode($form_data->linen_clothing,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($linen_clothing as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'coats_jackets' )
                               {{ 'Coats / Jackets' }}
                               @elseif($key == 'suits')
                               {{ 'Suits' }}
                               @elseif($key == 'dresses')
                               {{ 'Dresses' }}
                               @elseif($key == 'trousers')
                               {{ 'Trousers' }}
                               @elseif($key == 'sweaters')
                               {{ 'Sweaters' }}
                               @elseif($key == 'blouses')
                               {{ 'Blouses' }}
                               @elseif($key == 'skirts')
                               {{ 'Skirts ' }}
                               @elseif($key == 'sleepware')
                               {{ 'Sleepware' }}
                               @elseif($key == 'shirts')
                               {{ 'Shirts' }}
                               @elseif($key == 'footwear')
                               {{ 'Footwear' }}
                               @elseif($key == 'hosiery_socks')
                               {{ 'Hosiery / Socks' }}
                               @elseif($key == 'ties_scarves')
                               {{ 'Ties / Scarves' }}
                               @elseif($key == 'underwear')
                               {{ 'Underwear ' }}
                               @elseif($key == 'lingerie')
                               {{ 'Lingerie' }}
                               @elseif($key == 'sportswear')
                               {{ 'Sportswear' }}
                               @elseif($key == 'sheets_linen')
                               {{ 'Sheets/Linen' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>BEDROOM (MAIN)</th>
                      <td>
                          <table>
                            @php
                            $bedroom = !empty($form_data->bedroom) ? json_decode($form_data->bedroom,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($bedroom as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'beds' )
                               {{ 'Beds' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'tables')
                               {{ 'Tables' }}
                               @elseif($key == 'dressing_table')
                               {{ 'Dressing Table' }}
                               @elseif($key == 'chest_of_drawers')
                               {{ 'Chest of Drawers' }}
                               @elseif($key == 'mirrors')
                               {{ 'Mirrors' }}
                               @elseif($key == 'rugs')
                               {{ 'Rugs ' }}
                               @elseif($key == 'lamps')
                               {{ 'Lamps' }}
                               @elseif($key == 'curtains')
                               {{ 'Curtains' }}
                               @elseif($key == 'bookcases')
                               {{ 'Bookcases' }}
                               @elseif($key == 'wardrobes')
                               {{ 'Wardrobes' }}
                               @elseif($key == 'pictures_paintings')
                               {{ 'Pictures/Paintings ' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>BEDROOM(S) OTHERS</th>
                      <td>
                          <table>
                            @php
                            $other_bedroom = !empty($form_data->other_bedroom) ? json_decode($form_data->other_bedroom,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($other_bedroom as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'beds' )
                               {{ 'Beds' }}
                               @elseif($key == 'chairs')
                               {{ 'Chairs' }}
                               @elseif($key == 'tables')
                               {{ 'Tables' }}
                               @elseif($key == 'dressing_table')
                               {{ 'Dressing Table' }}
                               @elseif($key == 'chest_of_drawers')
                               {{ 'Chest of Drawers' }}
                               @elseif($key == 'mirrors')
                               {{ 'Mirrors' }}
                               @elseif($key == 'rugs')
                               {{ 'Rugs ' }}
                               @elseif($key == 'lamps')
                               {{ 'Lamps' }}
                               @elseif($key == 'curtains')
                               {{ 'Curtains' }}
                               @elseif($key == 'bookcases')
                               {{ 'Bookcases' }}
                               @elseif($key == 'wardrobes')
                               {{ 'Wardrobes' }}
                               @elseif($key == 'pictures_paintings')
                               {{ 'Pictures/Paintings ' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>BATHROOM</th>
                      <td>
                          <table>
                            @php
                            $bathroom = !empty($form_data->bathroom) ? json_decode($form_data->bathroom,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($bathroom as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'rugs_toilet_covers' )
                               {{ 'Rugs, Toilet Covers' }}
                               @elseif($key == 'toiletries')
                               {{ 'Toiletries' }}
                               @elseif($key == 'medical_supplies')
                               {{ 'Medical Supplies' }}
                               @elseif($key == 'towels')
                               {{ 'Towels' }}
                               @elseif($key == 'laundry_basket')
                               {{ 'Laundry Basket' }}
                               @elseif($key == 'perfume_aftershave')
                               {{ 'Perfume / Aftershave' }}
                               @elseif($key == 'cabinets_shelves')
                               {{ 'Cabinets / Shelves ' }}
                               @elseif($key == 'mirrors')
                               {{ 'Mirrors' }}
                               @elseif($key == 'razors')
                               {{ 'Razors' }}
                               @elseif($key == 'hair_dryers')
                               {{ 'Hair Dryers  ' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>MISCELLANEOUS</th>
                      <td>
                          <table>
                            @php
                            $miscellaneous = !empty($form_data->miscellaneous) ? json_decode($form_data->miscellaneous,true) : []  ;
                            //pa($miscellaneous);
                            @endphp
                            
                            <tr>
                              <th>Item Name</th>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            @foreach($miscellaneous as $key => $value)
                            @php
                            $key = str_replace("'",'',$key);
                            @endphp
                            
                            <tr>
                              <td>
                               @if($key == 'clocks' )
                               {{ 'Clocks' }}
                               @elseif($key == 'telephone_fax')
                               {{ 'Telephone / Fax' }}
                               @elseif($key == 'plant_holders')
                               {{ 'Plant Holders' }}
                               @elseif($key == 'toys_and_games')
                               {{ 'Toys & Gamest' }}
                               @elseif($key == 'food')
                               {{ 'Food (Non-perishable)' }}
                               @elseif($key == 'computer')
                               {{ 'Computer ' }}
                               @elseif($key == 'computer_printer')
                               {{ 'Computer Printer' }}
                               @elseif($key == 'computer_accessories')
                               {{ 'Computer Accessories' }}
                               @elseif($key == 'sewing_machine')
                               {{ 'Sewing Machine  ' }}
                               @elseif($key == 'video_camera')
                               {{ 'Video Camera ' }}
                               @elseif($key == 'cameras_lens')
                               {{ 'Cameras / Lens ' }}
                               @elseif($key == 'pictures_paintings')
                               {{ 'Pictures / Paintings  ' }}
                               @elseif($key == 'bicycles')
                               {{ 'Bicycles  ' }}
                               @elseif($key == 'books')
                               {{ 'Books ' }}
                               @elseif($key == 'audio_tapes')
                               {{ 'Audio Tapes' }}
                               @elseif($key == 'cds')
                               {{ 'CDs' }}
                               @elseif($key == 'video_tapes_dvds')
                               {{ "Video Tapes/DVD's" }}
                               @elseif($key == 'garden_equipment')
                               {{ 'Garden Furniture  ' }}
                               @elseif($key == 'garden_furniture')
                               {{ ' Garden Furniture  ' }}
                               @elseif($key == 'suitcases_trunks')
                               {{ 'Suitcases / Trunks ' }}
                               @elseif($key == 'tools')
                               {{ 'Tools' }}
                               @endif
                              </td>
                              <td>{{ $value["'items'"] ?? '' }}</td>
                              <td>{{ $value["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            @endforeach
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>ANY OTHER ITEMS</th>
                      <td>
                          <table>
                            @php
                            $any_other_items = !empty($form_data->any_other_items) ? json_decode($form_data->any_other_items,true) : []  ;
                            @endphp
                            
                            <tr>
                              <th>No. Items</th>
                              <th>Replacement cost</th>
                            </tr>
                            
                            <tr>
                               <td>{{ $any_other_items["'items'"] ?? '' }}</td>
                               <td>{{ $any_other_items["'replacement_cost'"] ?? '' }}</td>
                            </tr>
                            

                          </table>
                      </td>
                    </tr>

                    <tr>
                       <th>Tick cover</th>
                      <td>
                        @if($form_data->tick_cover==1)
                        {{ 'Full Conditions' }}
                        @elseif($form_data->tick_cover == 2)
                        {{ 'Restricted Conditions ' }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                       <th>Note</th>
                      <td>{{ $form_data->note ?? '' }}</td>
                    </tr>


                  </thead>
                  
                </table>
                
                   
        </div>

       
</div>
@endsection
@section('scripts')
@parent

@endsection