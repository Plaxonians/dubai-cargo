@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Blog Listing</b></h4>
    </div>
        <div class="card-body table-responsive p-0">
           @if(!empty($blog_data) && sizeof($blog_data))
                <table class="table table-hover">
                  <thead>
                    <tr>
                      
                      <th>Image</th>
                      <th>Title</th>
                      <th>Publish Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($blog_data as $key => $value)
                    <tr>
                      <td>
                        @if( $value->image && !empty($value->image) && file_exists($value->image) )
                        <img src="{{ URL::to($value->image) }}" height="50" width="50">
                         @endif
                       </td>
                       <td>{{ $value->title ?? '' }} </td>
                     
                      <!-- <td>{{ strip_tags($value->description) ?? '' }}</td> -->
                      <td>{{ $value->created_at ? date('d-M-Y',strtotime($value->created_at)) : '' }}</td>
                      <td>
                        <div class="btn-group btn-group-sm">
                          <a href="{{ URL::to('admin/blog/edit/'.$value->id) }}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                          &nbsp;
                          <a href="javascript:" class="btn btn-danger blog_delete" title="Delete" delete_blog_url="{{ URL::to('admin/blog/delete/'.$value->id) }}"><i class="fas fa-trash"></i></a>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
                @else
                    <div align="center">
                      <h3 align="center">No Record found !</h3>                      
                    </div>
                    @endif
        </div>

        <div class="card-footer clearfix">
                 {!! $blog_data->links() !!}
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection