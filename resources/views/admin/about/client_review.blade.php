@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Client Review </b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/update_client_review') }}" method="POST" enctype="multipart/form-data" class="validate_form">
            @csrf

             @php
            $data = isset($about_us_client_review_data->meta_data) && !empty($about_us_client_review_data->meta_data) ? json_decode($about_us_client_review_data->meta_data) : array() ;
            @endphp 

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}" maxlength="150">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : '' }}">
                <label for="sub_title">Sub Title*</label>
                <input type="text" id="sub_title" name="sub_title" class="form-control" value="{{ old('sub_title', isset($data->sub_title) ? $data->sub_title : '') }}" maxlength="150">
                @if($errors->has('sub_title'))
                    <p class="help-block">
                        {{ $errors->first('sub_title') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('review_content') ? 'has-error' : '' }}">
                <label for="review_content">Review Content *</label>
                <textarea rows="5"  id="review_content" name="review_content" class="form-control ">{{ old('review_content', isset($data->review_content) ? $data->review_content : '') }}</textarea>
                @if($errors->has('review_content'))
                    <p class="help-block">
                        {{ $errors->first('review_content') }}
                    </p>
                @endif
                
            </div>

            <hr/>
            <label for="review_content">Feedback</label>
             <div class="form-group {{ $errors->has('feedback_title') ? 'has-error' : '' }}">
                <label for="feedback_title">Title*</label>
                <input type="text" id="feedback_title" name="feedback_title" class="form-control" value="{{ old('feedback_title', isset($data->feedback_title) ? $data->feedback_title : '') }}" maxlength="150">
                @if($errors->has('feedback_title'))
                    <p class="help-block">
                        {{ $errors->first('feedback_title') }}
                    </p>
                @endif
                
            </div>

            
            <div class="form-group {{ $errors->has('feedback_content') ? 'has-error' : '' }}">
                <label for="feedback_content">Feedback Content *</label>
                <textarea rows="5"  id="feedback_content" name="feedback_content" class="form-control ">{{ old('feedback_content', isset($data->feedback_content) ? $data->feedback_content : '') }}</textarea>
                @if($errors->has('feedback_content'))
                    <p class="help-block">
                        {{ $errors->first('feedback_content') }}
                    </p>
                @endif
                
            </div>


            
            <div>
                <input class="btn btn-danger" type="hidden" name="id" value="{{ isset($about_us_client_review_data->id) ? $about_us_client_review_data->id : '0'}}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection