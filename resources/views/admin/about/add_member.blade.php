@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Red Family Member</b></h4>
   </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/updatemember') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Name*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($data->name) ? $data->name : '') }}">
                @if($errors->has('name'))
                    <p class="help-block">
                        {{ $errors->first('name') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                <label for="name">Designation*</label>
                <input type="text" id="designation" name="designation" class="form-control" value="{{ old('designation', isset($data->designation) ? $data->designation : '') }}">
                @if($errors->has('designation'))
                    <p class="help-block">
                        {{ $errors->first('designation') }}
                    </p>
                @endif
                
            </div>
           

             
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Upload Member Image</label> 

                @if(isset($data->image) && file_exists($data->image))
                
                <img src="{{ URL::to($data->image) }}" height="100" width="100">
                @endif

                <br/>
                <input type="file" name="images" accept="image/*">
                @if($errors->has('images'))
                    <p class="help-block">
                        {{ $errors->first('images') }}
                    </p>
                @endif
                
            </div>
            
            <div>
                <input type="hidden" name="id" value="{{ isset($data->id) ? $data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection