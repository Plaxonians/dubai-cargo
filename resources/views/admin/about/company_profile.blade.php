@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Company Profile </b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/updatecompanyprofile') }}" method="POST" enctype="multipart/form-data" class="validate_form">
            @csrf

             @php
            $data = isset($company_profile_data->meta_data) && !empty($company_profile_data->meta_data) ? json_decode($company_profile_data->meta_data) : array() ;
            @endphp 

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($data->title) ? $data->title : '') }}" maxlength="150">
                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('first_content') ? 'has-error' : '' }}">
                <label for="first_content">First Content *</label>
                <textarea rows="5"  id="left_content" name="first_content" class="form-control ">{{ old('first_content', isset($data->first_content) ? $data->first_content : '') }}</textarea>
                @if($errors->has('first_content'))
                    <p class="help-block">
                        {{ $errors->first('first_content') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('left_image') ? 'has-error' : '' }}">
                <label for="description">Left Image</label> 

                @if(isset($data->left_image) && file_exists($data->left_image))
                
                <img src="{{ URL::to($data->left_image) }}" height="100" width="100">
                <input type="hidden" name="old_left_image" value="{{ $data->left_image }}">
                @endif


                <br/>
                <p>Note : Image dimension should be within min 575X386 pixels.</p>
                <input type="file" name="left_image" accept="image/*">
                @if($errors->has('left_image'))
                    <p class="help-block">
                        {{ $errors->first('left_image') }}
                    </p>
                @endif
                
            </div>
            

            


             <div class="form-group {{ $errors->has('right_content') ? 'has-error' : '' }}">
                <label for="description">Right Content *</label>
                <textarea rows="10"  id="right_content" name="right_content" class="form-control ">{{ old('description', isset($data->right_content) ? $data->right_content : '') }}</textarea>
                @if($errors->has('right_content'))
                    <p class="help-block">
                        {{ $errors->first('right_content') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('bottom_content') ? 'has-error' : '' }}">
                <label for="bottom_content">Bottom Content *</label>
                <textarea rows="10"  id="bottom_content" name="bottom_content" class="form-control ">{{ old('bottom_content', isset($data->bottom_content) ? $data->bottom_content : '') }}</textarea>
                @if($errors->has('bottom_content'))
                    <p class="help-block">
                        {{ $errors->first('bottom_content') }}
                    </p>
                @endif
                
            </div>

            <div id="add_more_profile_points_result">
                @php
            $profile_points = isset($data->profile_points) && !empty($data->profile_points) ? ($data->profile_points) : array() ;
            @endphp
             @foreach($profile_points as $key => $value)
                <div class="form-group profile_point_remove_div remove_div{{$key+1}}" >
                    <label for="name"> Point*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$key+1}}"> Remove</button>
                    <input type="text"  name="profile_points[]" class=" validate[required] form-control" value="{{ $value ?? '' }}">
                </div>
                @endforeach
             </div>
             <button class="btn btn-success add_more_profile_points" type="button">Add More Profile Points</button>

            <hr/>
             <label for="bottom_content">Our Mission</label>

             <div class="form-group {{ $errors->has('our_mission_title') ? 'has-error' : '' }}">
                <label for="our_mission_title">Title*</label>
                <input type="text" id="our_mission_title" name="our_mission_title" class="form-control" value="{{ old('our_mission_title', isset($data->our_mission_title) ? $data->our_mission_title : '') }}" maxlength="150">
                @if($errors->has('our_mission_title'))
                    <p class="help-block">
                        {{ $errors->first('our_mission_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('our_mission_first_content') ? 'has-error' : '' }}">
                <label for="our_mission_first_content">First Content *</label>
                <textarea rows="10"  id="our_mission_first_content" name="our_mission_first_content" class="form-control ">{{ old('our_mission_first_content', isset($data->our_mission_first_content) ? $data->our_mission_first_content : '') }}</textarea>
                @if($errors->has('our_mission_first_content'))
                    <p class="help-block">
                        {{ $errors->first('our_mission_first_content') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('our_mission_second_content') ? 'has-error' : '' }}">
                <label for="our_mission_second_content">Second Content *</label>
                <textarea rows="10"  id="our_mission_second_content" name="our_mission_second_content" class="form-control ">{{ old('our_mission_second_content', isset($data->our_mission_second_content) ? $data->our_mission_second_content : '') }}</textarea>
                @if($errors->has('our_mission_second_content'))
                    <p class="help-block">
                        {{ $errors->first('our_mission_second_content') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('our_mission_third_content') ? 'has-error' : '' }}">
                <label for="our_mission_third_content">Third Content *</label>
                <textarea rows="10"  id="our_mission_third_content" name="our_mission_third_content" class="form-control ">{{ old('our_mission_third_content', isset($data->our_mission_third_content) ? $data->our_mission_third_content : '') }}</textarea>
                @if($errors->has('our_mission_third_content'))
                    <p class="help-block">
                        {{ $errors->first('our_mission_third_content') }}
                    </p>
                @endif
                
            </div>

           

            
            <div>
                <input class="btn btn-danger" type="hidden" name="id" value="{{ isset($company_profile_data->id) ? $company_profile_data->id : '0'}}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection