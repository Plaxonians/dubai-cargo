@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Sustainability </b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/about/updatesustainability') }}" method="POST" enctype="multipart/form-data">
            @csrf

            @php
            $data = isset($sustainability_data->meta_data) && !empty($sustainability_data->meta_data) ? json_decode($sustainability_data->meta_data) : array() ;
            @endphp 
           
            <div class="form-group {{ $errors->has('first_content') ? 'has-error' : '' }}">
                <label for="description">First Content *</label>
                <textarea rows="5"  id="first_content" name="first_content" class="form-control ">{{ old('first_content', isset($data->first_content) ? $data->first_content : '') }}</textarea>
                @if($errors->has('first_content'))
                    <p class="help-block">
                        {{ $errors->first('first_content') }}
                    </p>
                @endif
                
            </div>

             

            <div class="form-group {{ $errors->has('first_image') ? 'has-error' : '' }}">
                <label for="description">Upload Image*</label> 

                @if(isset($data->first_image) && file_exists($data->first_image))
                <img src="{{ URL::to($data->first_image) }}" height="100" width="100">
                <input type="hidden" name="old_first_image" value="{{ $data->first_image }}">
                @endif

                

                <br/>
                <p>Note : Image dimension should be within min 470X286 pixels.</p>
                <input type="file" name="first_image" accept="image/*">
                @if($errors->has('first_image'))
                    <p class="help-block">
                        {{ $errors->first('first_image') }}
                    </p>
                @endif
                
            </div>

             <hr />

             <div class="form-group {{ $errors->has('second_content') ? 'has-error' : '' }}">
                <label for="description">Second Content *</label>
                <textarea rows="5"  id="second_content" name="second_content" class="form-control ">{{ old('second_content', isset($data->second_content) ? $data->second_content : '') }}</textarea>
                @if($errors->has('second_content'))
                    <p class="help-block">
                        {{ $errors->first('second_content') }}
                    </p>
                @endif
                
            </div>

             

            <div class="form-group {{ $errors->has('second_image') ? 'has-error' : '' }}">
                <label for="second_image">Upload Image*</label> 

                @if(isset($data->second_image) && file_exists($data->second_image))
                <img src="{{ URL::to($data->second_image) }}" height="100" width="100">
                <input type="hidden" name="old_second_image" value="{{ $data->second_image }}">
                @endif

                

                <br/>
                <p>Note : Image dimension should be within min 544X226 pixels.</p>
                <input type="file" name="second_image" accept="image/*">
                @if($errors->has('second_image'))
                    <p class="help-block">
                        {{ $errors->first('second_image') }}
                    </p>
                @endif
                
            </div>

             <hr />

             <div class="form-group {{ $errors->has('third_content') ? 'has-error' : '' }}">
                <label for="third_content">Third Content *</label>
                <textarea rows="5"  id="third_content" name="third_content" class="form-control ">{{ old('third_content', isset($data->third_content) ? $data->third_content : '') }}</textarea>
                @if($errors->has('third_content'))
                    <p class="help-block">
                        {{ $errors->first('third_content') }}
                    </p>
                @endif
                
            </div>

             

           <div class="form-group {{ $errors->has('third_image') ? 'has-error' : '' }}">
                <label for="third_image">Upload Image*</label> 

                @if(isset($data->third_image) && file_exists($data->third_image))
                <img src="{{ URL::to($data->third_image) }}" height="100" width="100">
                <input type="hidden" name="old_third_image" value="{{ $data->third_image }}">
                @endif

                

                <br/>
                <p>Note : Image dimension should be within min 470X286 pixels.</p>
                <input type="file" name="third_image" accept="image/*">
                @if($errors->has('third_image'))
                    <p class="help-block">
                        {{ $errors->first('third_image') }}
                    </p>
                @endif
                
            </div>

             <hr />

             <div class="form-group {{ $errors->has('bottom_content') ? 'has-error' : '' }}">
                <label for="description">Bottom Content *</label>
                <textarea rows="5"  id="bottom_content" name="bottom_content" class="form-control ">{{ old('bottom_content', isset($data->bottom_content) ? $data->bottom_content : '') }}</textarea>
                @if($errors->has('bottom_content'))
                    <p class="help-block">
                        {{ $errors->first('bottom_content') }}
                    </p>
                @endif
                
            </div>
            
            
            <div>
                <input class="btn btn-danger" type="hidden" name="id" value="{{ isset($sustainability_data->id) ? $sustainability_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection