<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="{{ URL::to(config('constants.website_favicon_image')) }}" rel="icon" />
    <title>{{ isset($page_title) && !empty($page_title) ? $page_title.'|'.config('constants.website_name') :config('constants.website_name') }}
    </title>


    <link rel="stylesheet" href="{{ asset('css/frontend/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/aos.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/slick.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/jquery.fancybox.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/font/flaticon.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/common.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/frontend/responsive-common.css') }}" />

    @if(isset($top_css) && sizeof($top_css) )
    @foreach($top_css as $key => $value)
    <link rel="stylesheet" href="{{ asset('css/frontend/'.$value) }}" />
    @endforeach
    @endif

    @yield('styles')

    <script type="text/javascript">
    var base_url = '{{ url('/')}}/';
    </script>

    @if(isset($top_js) && sizeof($top_js) )
    @foreach($top_js as $key => $value)
    <script type="text/javascript" src="{{ asset('js/frontend/'.$value) }}"></script>
    @endforeach
    @endif

</head>

<body>

       
        @if ($message = Session::get('error'))
        <div class="flash_message alert alert-danger alert-block" role="alert" align="center">
           {!! $message !!}
        </div>
        @endif

        <div class="flash_message">
        </div>


    @include('partials.frontend.header')
    @yield('content')
    @include('partials.frontend.footer')


    <script type="text/javascript" src="{{ asset('js/frontend/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/aos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/daterangepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/jquery.fancybox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/main.js?reload=true') }}"></script>
    <script type="module" src="{{ asset('js/frontend/get-qoute-script.js') }}"></script>

    @if(isset($bottom_js) && sizeof($bottom_js) )
    @foreach($bottom_js as $key => $value)
    <script src="{{ asset('js/frontend/'.$value) }}"></script>
    @endforeach
    @endif



</body>

</html>