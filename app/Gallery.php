<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Gallery extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        
        'image',
        'status',
        'is_deleted',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

       protected $table = 'gallery';

       


    public static function update_Or_insert($postData,$id=0){
      
     

      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('gallery')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
         $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('gallery')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getAllGalleryData($page=10) {

       $result = DB::table('gallery')->
                where('is_deleted',0)->
                orderBy('id','DESC')->
                paginate($page);

       return $result;
    }

    

   


    public static function get_data_by_id($id){

       $result = DB::table('gallery')->
                    select('gallery.*')->
                    where('gallery.id',$id)->
                    where('gallery.is_deleted',0)->
                    first();

       return $result;
    }
    

   

}
