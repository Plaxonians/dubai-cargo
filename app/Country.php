<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Country extends Model
{

       protected $table = 'countries';



       public static function get_country_name_by_id ($id)
       {

       	$result = DB::table('countries')->
                    select('countries.name')->
                    where('countries.id',$id)->
                    where('countries.is_deleted',0)->
                    first();

       return !empty($result) ? $result->name  : '' ;
       }

   

}
