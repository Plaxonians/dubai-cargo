<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
     protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'role_id',
        'permission_id',
    ];

       protected $table = 'roles_permissions';
}
