<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class State extends Model
{

       protected $table = 'states';


       public static function get_state_name_by_id ($id)
       {

       	$result = DB::table('states')->
                    select('states.name')->
                    where('states.id',$id)->
                    where('states.is_deleted',0)->
                    first();

       return !empty($result) ? $result->name  : '' ;
       }

   

}
