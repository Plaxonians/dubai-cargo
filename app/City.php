<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class City extends Model
{

       protected $table = 'cities';


       public static function get_city_name_by_id ($id)
       {

       	$result = DB::table('cities')->
                    select('cities.name')->
                    where('cities.id',$id)->
                    where('cities.is_deleted',0)->
                    first();

       return !empty($result) ? $result->name  : '' ;
       }

   

}
