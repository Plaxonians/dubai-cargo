<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Testimonials;
use App\MetaData;
use App\Country;
use App\State;
use App\City;
class QuoteController   extends Controller
{
    //

    public function index(Request $request)
    {   
     $page_title     = 'Get a quote' ;
     $all_testimonials = Testimonials::getAllTestimonialsData();
      $home_page_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.home_page_content'));
      $home_page_data      = isset($home_page_meta_data->meta_data) && !empty($home_page_meta_data->meta_data) ? json_decode($home_page_meta_data->meta_data) : [] ;

      $country_list = Country::where('is_deleted',0)->get();
      $state_list = State::where(['country_id'=>229,'is_deleted'=>0])->get();


     $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'get_quote/style.css',
                                 'get_quote/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                  'get_quote/quote.js',
                                 ],
                    'country_list'=>$country_list,
                    'state_list'=>$state_list,
                    'home_page_data'=>$home_page_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
        
     return view('frontend.get_quote.get_quote',$view_data);

    }
   
    
}
