<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Testimonials;
use App\Gallery;
use App\Form;
class AboutusController   extends Controller
{
    //

    

    public function profile(Request $request)
    {   
    
    $page_title     = 'Company Profile' ;

    $company_profile_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_company_profile'));
    $company_profile_data = isset($company_profile_data->meta_data) && !empty($company_profile_data->meta_data) ? json_decode($company_profile_data->meta_data) : array() ;
    $all_testimonials = Testimonials::getAllTestimonialsData();
    $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'aboutus/style.css',
                                 'aboutus/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                 ],
                    'company_profile_data'=> $company_profile_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
     //pa($view_data);
      return view('frontend.aboutus.company_profile',$view_data);

    }

    public function clientreview(Request $request)
    {   
    
    $page_title     = 'Client Review' ;

    $about_us_client_review_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_client_review'));
    $about_us_client_review_data = isset($about_us_client_review_data->meta_data) && !empty($about_us_client_review_data->meta_data) ? json_decode($about_us_client_review_data->meta_data) : array() ;
    $all_testimonials = Testimonials::getAllTestimonialsData();
    $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'aboutus/style.css',
                                 'aboutus/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                  'aboutus/client_feedback.js'
                                 ],
                    'about_us_client_review_data'=> $about_us_client_review_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
     //pa($view_data);
      return view('frontend.aboutus.client_review',$view_data);

    }

     public function gallery(Request $request)
    {   
    
    $page_title     = 'Gallery' ;

   
    $gallery_data = Gallery::getAllGalleryData(9);
    $all_testimonials = Testimonials::getAllTestimonialsData();
    $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'aboutus/style.css',
                                 'aboutus/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                 ],
                    'gallery_data'=> $gallery_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
     //pa($view_data);
      return view('frontend.aboutus.gallery',$view_data);

    }

     public function client_feedback_save(Request $request)
    {
      $response = ['status'=>0] ;
      $post   = $request->all();
      \Log::info($post);
      $validation = [
                    'service_type' => 'required',
                    'feedback_date' => 'required',
                    'first_name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required|digits:10',
                    
                ] ;
      $customMessages = [] ;
      $validator =  \Validator::make($request->all(),$validation);

        if ($validator->fails())
        {
            $response['errors'] = $validator->errors()->all();
        }
        else
        {

         
         
          $postData =  array(
                    'service_type'=>isset($post['service_type'])? $post['service_type'] : '',
                    'feedback_date'=>isset($post['feedback_date'])? date('Y-m-d',strtotime($post['feedback_date'])) : '',
                    'first_name'=>isset($post['first_name'])? $post['first_name'] : '',
                    'email'=>isset($post['email'])? $post['email'] : '',
                    'phone'=>isset($post['phone'])? $post['phone'] : '',
                    'message'=>isset($post['message'])? $post['message'] : '',

                   
                 );
          $upload_file = $request->file('upload_file'); 

          if(isset($upload_file) && !empty($upload_file) )
            {
              $fileName = time().'.'.$upload_file->getClientOriginalExtension();  
              $upload_file->move('uploads/feedback_attach_file', $fileName);
              $uploded_file_name = 'uploads/feedback_attach_file/'.$fileName;
              $postData['upload_file'] = $uploded_file_name ; 

            }

             $postData['form_type'] = config('constants.form_type.feedback') ; 
             $res = Form::update_Or_insert($postData);

              if($res)
              {
                $response['status'] = 1 ;
                $response['msg'] = 'Thank you for your feedback. ' ;
              }
               else 
               {
                  $response['status'] = 0 ;
                  $response['msg']   = 'some thing went to wrong  please try again !' ;
              } 

        }

        return response()->json($response);

    }

    
    
}
