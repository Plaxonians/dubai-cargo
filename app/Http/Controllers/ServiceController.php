<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Banner;
use App\Form;
use App\Blog;
use App\Testimonials;

class ServiceController   extends Controller
{
    //

    

     public function detail($service_url)
    {   
     
     
      if($service_url == create_slugify(get_services_name(1)))
      {
        $page_title     = get_services_name(1) ;
        $service_type   =  17;
        $view = 'frontend.service.service_one';

      }
      else if($service_url == create_slugify(get_services_name(2)))
      {
        $page_title     = get_services_name(2) ;
        $service_type   =  18;
        $view = 'frontend.service.service_two';
      }
      else if($service_url == create_slugify(get_services_name(3)))
      {
        $page_title     = get_services_name(3) ;
        $service_type   =  19;
        $view = 'frontend.service.service_three';
      }
      else if($service_url == create_slugify(get_services_name(4)))
      {
        $page_title     = get_services_name(4) ;
        $service_type   =  20;
        $view = 'frontend.service.service_four';
      }
      else if($service_url == create_slugify(get_services_name(5)))
      {
        $page_title     = get_services_name(5) ;
        $service_type   =  24;
        $view = 'frontend.service.service_five';
      }

      $all_testimonials = Testimonials::getAllTestimonialsData();
      $blog_data = Blog::getAllBlogData(3);
      $service_data = MetaData::get_data_by_meta_type($service_type);
      $service_data      = isset($service_data->meta_data) && !empty($service_data->meta_data) ? json_decode($service_data->meta_data) : [] ;

     $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'service/style.css',
                                 'service/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                  'services/script.js'
                                 ],
                    'service_data'=> $service_data,
                    'blog_data'=> $blog_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
     //pad($view_data);

     return view($view,$view_data);

    }

    
   
    
}
