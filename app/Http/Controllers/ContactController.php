<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\Rules\ContactCapture;
use App\Form;

use App\Traits\EmailsTrait;

class ContactController   extends Controller
{
    //
  use EmailsTrait;

    public static function index(Request $request)
    {   

     $page_title     = 'Contact Us' ;  

      $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));
      $contact_us_data      = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : [] ;


      $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'contact_us/style.css',
                                 'contact_us/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                  'contact_us/validate.js'
                                 ],
                    'contact_us_data'=> $contact_us_data,
                    ];


     return view('frontend.contact_us.contact_us',$view_data);
    }


    public static function save(Request $request)
    { 

      $post = $request ;
       $flush_data =  array('key'=>'','msg'=>'') ;

      $validation = [
                    'first_name' => 'required',
                    'email' => 'required|email',
                    'contact_number' => 'required',
                ] ;
       

     
      $customMessages = [
                          'first_name.required' => 'The Name field is required.',
                          'contact_number.required' => 'The Contact number field is required.',
                        ];
    $request->validate($validation,$customMessages);
    

    $postData =  array(
                    'first_name'=>isset($post['first_name'])? $post['first_name'] : '',
                    'email'=>isset($post['email'])? $post['email'] : '',
                    'phone'=>isset($post['contact_number'])? $post['contact_number'] : '',
                    'message'=>isset($post['message'])? $post['message'] : '',
                    'form_type'=>config('constants.form_type.contact'),
                 );
     
    
    $res = Form::update_Or_insert($postData);

            if($res){
                EmailsTrait::contact_us_email($res) ;
              $flush_data['key'] = 'success' ;
              $flush_data['msg'] = 'Thank you for contacting us.
You are very important to us, all information received will always remain confidential.<br/> We will contact you as soon as we review your message.' ;
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong  please try again !' ;
            } 

    return $res ? redirect('thankyou')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

       
    }

    public static function address()
    {   

     $page_title     = 'Address' ;  

      $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));
      $contact_us_data      = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : [] ;


      $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'contact_us/style.css',
                                 'contact_us/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                  //'contact_us/address.js'
                                 ],
                    'contact_us_data'=> $contact_us_data,
                    ];


     return view('frontend.contact_us.address',$view_data);
    }
    
}
