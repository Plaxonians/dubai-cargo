<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Blog;
use App\MetaData;
use App\Testimonials;
use App\Gallery;
use App\Form;
class SiteController   extends Controller
{
    //

    public function index(Request $request)
    {  

      $page_title          = '' ;
      $banner_data         = MetaData::get_data_by_meta_type(config('constants.meta_type.banner_data'));
      $banner_data      = isset($banner_data->meta_data) && !empty($banner_data->meta_data) ? json_decode($banner_data->meta_data) : [] ;

  	  $home_page_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.home_page_content'));
      $home_page_data      = isset($home_page_meta_data->meta_data) && !empty($home_page_meta_data->meta_data) ? json_decode($home_page_meta_data->meta_data) : [] ;

      $gallery_data = Gallery::getAllGalleryData(10);
      $all_testimonials = Testimonials::getAllTestimonialsData();
      $blog_data = Blog::getAllBlogData(3);


      $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'home/style.css',
                                 'home/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                   //'home/script.js',
                                 ],
                    'banner_data'=> $banner_data,
                    'home_page_data'=> $home_page_data,
                    'gallery_data'=> $gallery_data,
                    'all_testimonials'=> $all_testimonials,
                    'blog_data'=> $blog_data,
                    ];
     //pa($view_data);
      return view('frontend.home.index',$view_data);

    }

     public function quote_save(Request $request)
    {
      $response = ['status'=>0] ;
      $post   = $request->all();
      \Log::info($post);
      $validation = [
                    'first_name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required|digits:10',
                    'proposer' => 'required',
                    'address_origin' => 'required',
                    'address_destination' => 'required',
                    'shipped_by' => 'required',
                    'packing' => 'required',
                    'tick_cover' => 'required',
                ] ;

      if(isset($post['service_type']) && !empty($post['service_type']))
      {

        $service_type = get_services_type_id_by_services_slug($post['service_type']) ;
        $validation['service_type']='required';
        $validation['service_date']='required';

        if($service_type == '1' )
        {
          $validation['residential_city_from']='required';
          $validation['residential_city_to']='required';
          $validation['residential_area_from']='required';
          $validation['residential_area_to']='required';
        }
        if($service_type == '2' )
        {
          $validation['office_city_from']='required';
          $validation['office_city_to']='required';
          $validation['office_area_from']='required';
          $validation['office_area_to']='required';
        }
        if($service_type == '4' )
        {
          $validation['storage_city_from']='required';
          $validation['storage_city_to']='required';
          $validation['storage_area_from']='required';
          $validation['storage_area_to']='required';
        }
        if($service_type == '3' )
        {
          //$validation['furniture_detail']='required';
        }
        if($service_type == '5' )
        {
          $validation['cargo_country_from']='required';
          $validation['cargo_country_to']='required';
          $validation['cargo_city_from']='required';
          $validation['cargo_city_to']='required';
        }
      }
      $customMessages = [] ;
      
      $validator =  \Validator::make($request->all(),$validation);

        if ($validator->fails())
        {
            $response['errors'] = $validator->errors()->all();
        }
        else
        {

          $living_room                  =  isset($post['living_room'])? $post['living_room'] : [] ;
          $dining_room                  =  isset($post['dining_room'])? $post['dining_room'] : [] ;
          $family_room                  =  isset($post['family_room'])? $post['family_room'] : [] ;
          $kitchen                      =  isset($post['kitchen'])? $post['kitchen'] : [] ;
          $chinaware_porcelatin         =  isset($post['chinaware_porcelatin'])? $post['chinaware_porcelatin'] : [] ;
          $crystal_glassware            =  isset($post['crystal_glassware'])? $post['crystal_glassware'] : [] ;
          $silverware_brassware         =  isset($post['silverware_brassware'])? $post['silverware_brassware'] : [] ;
          $antiques_works_of_art        =  isset($post['antiques_works_of_art'])? $post['antiques_works_of_art'] : [] ;
          $sports_equipment             =  isset($post['sports_equipment'])? $post['sports_equipment'] : [] ;
          $linen_clothing               =  isset($post['linen_clothing'])? $post['linen_clothing'] : [] ;
          $bedroom                      =  isset($post['bedroom'])? $post['bedroom'] : [] ;
          $other_bedroom                =  isset($post['other_bedroom'])? $post['other_bedroom'] : [] ;
          $bathroom                     =  isset($post['bathroom'])? $post['bathroom'] : [] ;
          $miscellaneous                =  isset($post['miscellaneous'])? $post['miscellaneous'] : [] ;
          $any_other_items              =  isset($post['any_other_items'])? $post['any_other_items'] : [] ;
         
          $postData =  array(
                    /// First Step ////

                    'first_name'=>isset($post['first_name'])? $post['first_name'] : '',
                    'last_name'=>isset($post['last_name'])? $post['last_name'] : '',
                    'email'=>isset($post['email'])? $post['email'] : '',
                    'phone'=>isset($post['phone'])? $post['phone'] : '',
                    'area_name'=>isset($post['area_name'])? $post['area_name'] : '',
                    'nearest_landmark'=>isset($post['nearest_landmark'])? $post['nearest_landmark'] : '',
                    'address'=>isset($post['address'])? $post['address'] : '',
                    'message'=>isset($post['message'])? $post['message'] : '',

                    /// Second Step ////

                     'proposer'=>isset($post['proposer'])? $post['proposer'] : '',
                     'address_origin'=>isset($post['address_origin'])? $post['address_origin'] : '',
                     'address_destination'=>isset($post['address_destination'])? $post['address_destination'] : '',
                     'shipped_by'=>isset($post['shipped_by'])? $post['shipped_by'] : '',
                     'packing'=>isset($post['packing'])? $post['packing'] : '',
                     'date_of_transit'=>isset($post['date_of_transit']) && !empty($post['date_of_transit'])? $post['date_of_transit'] : '',
                     'number_of_package'=>isset($post['number_of_package']) && !empty($post['number_of_package'])? $post['number_of_package'] : '',
                     'packing_name'=>isset($post['packing_name']) && !empty($post['packing_name'])? $post['packing_name'] : '',

                     /// Third Step ////

                    'living_room'=>json_encode($living_room),
                    'dining_room'=>json_encode($dining_room),
                    'family_room'=>json_encode($family_room),
                    'kitchen'=>json_encode($kitchen),
                    'chinaware_porcelatin'=>json_encode($chinaware_porcelatin),
                    'crystal_glassware'=>json_encode($crystal_glassware),
                    'silverware_brassware'=>json_encode($silverware_brassware),
                    'antiques_works_of_art'=>json_encode($antiques_works_of_art),
                    'sports_equipment'=>json_encode($sports_equipment),
                    'linen_clothing'=>json_encode($linen_clothing),
                    'bedroom'=>json_encode($bedroom),
                    'other_bedroom'=>json_encode($other_bedroom),
                    'bathroom'=>json_encode($bathroom),
                    'miscellaneous'=>json_encode($miscellaneous),
                    'any_other_items'=>json_encode($any_other_items),

                    /// Fourth Step ////

                    'tick_cover'=>isset($post['tick_cover'])? $post['tick_cover'] : '',
                    'note'=>isset($post['comment']) && !empty($post['comment'])? $post['comment'] : '',
                 );

        $postData['form_type'] = config('constants.form_type.quote') ; 

      if(isset($post['service_type']) && !empty($post['service_type']))
      {
        $service_type = get_services_type_id_by_services_slug($post['service_type']) ;
        $postData['form_type'] = config('constants.form_type.get_quote') ; 
       

         $postData['service_type']  = $service_type ;
         $postData['feedback_date'] = isset($post['service_date']) && !empty($post['service_date'])? date('Y-m-d',strtotime($post['service_date'])) : '' ; 

        if($service_type == '1' )
        {
           $postData['city_from'] = isset($post['residential_city_from'])? $post['residential_city_from'] : '' ; 
           $postData['city_to'] = isset($post['residential_city_to'])? $post['residential_city_to'] : '' ; 
           $postData['area_from'] = isset($post['residential_area_from'])? $post['residential_area_from'] : '' ; 
           $postData['area_to'] = isset($post['residential_area_to'])? $post['residential_area_to'] : '' ; 
        }
        if($service_type == '2' )
        {
          $postData['city_from'] = isset($post['office_city_from'])? $post['office_city_from'] : '' ; 
          $postData['city_to'] = isset($post['office_city_to'])? $post['office_city_to'] : '' ; 
          $postData['area_from'] = isset($post['office_area_from'])? $post['office_area_from'] : '' ; 
          $postData['area_to'] = isset($post['office_area_to'])? $post['office_area_to'] : '' ; 
        }
        if($service_type == '4' )
        {
          $postData['city_from'] = isset($post['storage_city_from'])? $post['storage_city_from'] : '' ; 
          $postData['city_to'] = isset($post['storage_city_to'])? $post['storage_city_to'] : '' ; 
          $postData['area_from'] = isset($post['storage_area_from'])? $post['storage_area_from'] : '' ; 
          $postData['area_to'] = isset($post['storage_area_to'])? $post['storage_area_to'] : '' ; 
        }
        if($service_type == '3' )
        {
          $postData['furniture_detail'] = isset($post['furniture_detail'])? $post['furniture_detail'] : '' ; 
        }
        if($service_type == '5' )
        {
          $postData['city_from'] = isset($post['cargo_country_from'])? $post['cargo_country_from'] : '' ; 
          $postData['city_to'] = isset($post['cargo_country_to'])? $post['cargo_country_to'] : '' ; 
          $postData['area_from'] = isset($post['cargo_city_from'])? $post['cargo_city_from'] : '' ; 
          $postData['area_to'] = isset($post['cargo_city_to'])? $post['cargo_city_to'] : '' ; 
        }
      }

          $attach_file = $request->file('attach_file'); 
          if(isset($attach_file) && !empty($attach_file) )
            {
              $fileName = time().'.'.$attach_file->getClientOriginalExtension();  
              $attach_file->move('uploads/attach_file', $fileName);
              $uploded_file_name = 'uploads/attach_file/'.$fileName;
              $postData['upload_file'] = $uploded_file_name ; 

            }

            
             //dd($postData);
             $res = Form::update_Or_insert($postData);

              if($res)
              {
                  //EmailsTrait::contact_us_email($res) ;
                $response['status']     = 1 ;
                $response['csrf_token'] =  csrf_token() ;

                $response['msg'] = 'Thank you for contacting us directly. ' ;
              }
               else 
               {
                  $response['status'] = 0 ;
                  $response['msg']   = 'some thing went to wrong  please try again !' ;
              } 

        }

        return response()->json($response);

    }


    public function thank_you()
    {   

      if (!Session::get('success'))
      {
        return redirect('/');
      }
      $message        = Session::get('success') ;
      
     $page_title     = 'Thank You' ;


         $view_data = [
                    'page_title' => $page_title,
                    'message' => $message,
                    'top_css'=> [
                                 'home/thank_you.css',
                                 'home/thank_you_responsive.css'
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                 ],
                    
                    ];
     //pa($view_data);
      return view('frontend.home.thank_you',$view_data);


   }


    
}
