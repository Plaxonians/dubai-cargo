<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Testimonials;

class TestimonialController extends Controller
{
    //

     public function add(Request $request)
    {   
      return view('admin.testimonial.add_testimonial');
    }

    

      public function edit($id)
    {   
        if(! Testimonials::is_exists($id) ){
          return redirect('admin/testimonial')->with('error', 'Testimonial does not exist');
        }
        $data = Testimonials::get_data_by_id($id);
        return view('admin.testimonial.add_testimonial',compact('data'));
    }



    public function index(Request $request)
    {   
      $all_testimonials = Testimonials::getAllTestimonialsData();
      return view('admin.testimonial.index',compact('all_testimonials'));
    }

       public function update(Request $request)
    {

       $flush_data =  array('key'=>'','msg'=>'') ;
       $post   = $request->all();
       //pad(strlen($post['details']));

       $validation =  array(
                    'name' => 'required',
                    //'details' => 'required|max:150',
                    'details' => 'required',
                     ) ;
       
       $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
       if(!$id || !empty($request->file('images'))){
           $validation['images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=200,min_height=200' ;
         } 

          $customMessages = [
                            'images.dimensions' => 'Select :attribute image must be at least min 200x200 pixels.',
                        ]; 


        $request->validate($validation,$customMessages);

        $postData =  array(
                    'name'=>isset($post['name'])? $post['name'] : '',
                    'details'=>isset($post['details'])? $post['details'] : '',
                 );

      

      $images = $request->file('images'); 
          if(isset($images) && !empty($images) ) {
              $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/testimonials', $fileName);
              $uploded_file_name = 'uploads/testimonials/'.$fileName;
              $postData['image'] = $uploded_file_name ; 
              
          }

         $res = Testimonials::update_Or_insert($postData,$id);


         
          
          if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Testimonial updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Testimonial added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/testimonial')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.homepage.add')->with($flush_data['key'], $flush_data['msg']);
    


    }


     public function delete($id)
    {   
       if(!Testimonials::is_exists($id)){
         return redirect()->back()->with('error', 'Testimonial does not exist');
       }
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = Testimonials::update_Or_insert($postData,$id);

         if($res){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Testimonial deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }

}
