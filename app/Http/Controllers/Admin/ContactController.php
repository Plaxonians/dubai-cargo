<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;

class ContactController   extends Controller
{
    //

    public function index(Request $request)
    {   

        
        $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));

        return view('admin.contactus.index',compact('contact_us_data'));
    }



   

    public function update(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
      $post   = $request->all();

      $validation = [
                    'phone' => 'required',
                    'landline_no' => 'required',
                    'address_one_location_name' => 'required',
                    'address_one_address' => 'required',
                    //'address_two_location_name' => 'required',
                    //'address_two_address' => 'required',
                    'address_one_embed_map' => 'required',
                    //'address_two_embed_map' => 'required',
                ] ;

        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

        $customMessages = [
                            'address_one_location_name.required' => 'The Location name field is required.',
                            'address_one_address.required' => 'The Address field is required.',
                            //'address_two_location_name.required' => 'The Location name field is required.',
                            //'address_two_address.required' => 'The Address field is required.',
                            'address_one_embed_map.required' => 'The Embed Map is required.',
                            //'address_two_embed_map.required' => 'The Embed Map is required.',
                        ];
          
       $request->validate($validation,$customMessages);
    
      $postData =  array(
                    'phone'=>isset($post['phone'])? $post['phone'] : '',
                    'landline_no'=>isset($post['landline_no'])? $post['landline_no'] : '',
                    'address_one_location_name'=>isset($post['address_one_location_name'])? $post['address_one_location_name'] : '',
                    'address_one_address'=>isset($post['address_one_address'])? $post['address_one_address'] : '',
                    //'address_two_location_name'=>isset($post['address_two_location_name'])? $post['address_two_location_name'] : '',
                    //'address_two_address'=>isset($post['address_two_address'])? $post['address_two_address'] : '',
                    'address_one_embed_map'=>isset($post['address_one_embed_map'])? $post['address_one_embed_map'] : '',
                    //'address_two_embed_map'=>isset($post['address_two_embed_map'])? $post['address_two_embed_map'] : '',
                    
                 );

     

      
                $metaData =  array(
                                    'meta_type'=>config('constants.meta_type.contact_us_page_details'),
                                    'meta_title'=> 'Contact Us',
                                    'meta_data'=>json_encode($postData),
                            );
               
            $res = MetaData::update_Or_insert($metaData,$id);

            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Contact Us updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Contact Us added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/contactus')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }


    
}
