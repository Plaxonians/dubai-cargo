<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\ActivityLog;
use App\MetaData;

class BannerController extends Controller
{
    //

     public function index(Request $request)
    {   
      //pad(env('APP_URL'));
    	$banner_data = MetaData::get_data_by_meta_type(config('constants.meta_type.banner_data'));
        return view('admin.banner.index',compact('banner_data'));
    }

     public function update(Request $request)
    {

    
        $post   = $request->all();
        $flush_data =  array('key'=>'','msg'=>'') ;

        $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                ] ;
            
        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;


        if(!$id || !empty($request->file('left_image')))
        {
           $validation['left_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=203,min_height=451' ;
        }

        if(!$id || !empty($request->file('left_top_image')))
        {
           $validation['left_top_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=100,min_height=160' ;
        }

        if(!$id || !empty($request->file('right_image')))
        {
           $validation['right_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=388,min_height=375' ;
        }

        if(!$id || !empty($request->file('right_top_image')))
        {
           $validation['right_top_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=100,min_height=150' ;
        }


         $customMessages = [
                            'left_image.dimensions' => 'Select :attribute  must be at least min 203x451 pixels.',
                            'left_top_image.dimensions' => 'Select :attribute  must be at least min 100x160 pixels.',
                            'right_image.dimensions' => 'Select :attribute  must be at least min 388x375 pixels.',
                            'right_top_image.dimensions' => 'Select :attribute  must be at least min 100x160 pixels.',
                        ];


        $request->validate($validation,$customMessages);


        $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                 );


          $left_image = $request->file('left_image'); 
          if(isset($left_image) && !empty($left_image) ) 
          {
              $fileName = time().'-.'.$left_image->getClientOriginalExtension();  
              $left_image->move('uploads/banner', $fileName);
              $uploded_file_name = 'uploads/banner/'.$fileName;
              $postData['left_image'] = $uploded_file_name ; 
              
               if(isset($post['old_left_image']) && !empty($post['old_left_image'])) 
               {
                  \File::delete($post['old_left_image']);
               }

          }
          else if(isset($post['old_left_image']) && !empty($post['old_left_image'])) 
          {
            $postData['left_image'] = $post['old_left_image'] ; 
          }

          $left_top_image = $request->file('left_top_image'); 
          if(isset($left_top_image) && !empty($left_top_image) ) 
          {
              $fileName = time().'--.'.$left_top_image->getClientOriginalExtension();  
              $left_top_image->move('uploads/banner', $fileName);
              $uploded_file_name = 'uploads/banner/'.$fileName;
              $postData['left_top_image'] = $uploded_file_name ; 
              
               if(isset($post['old_left_top_image']) && !empty($post['old_left_top_image'])) 
               {
                  \File::delete($post['old_left_top_image']);
               }

          }
          else if(isset($post['old_left_top_image']) && !empty($post['old_left_top_image'])) 
          {
            $postData['left_top_image'] = $post['old_left_top_image'] ; 
          }

          $right_image = $request->file('right_image'); 
          if(isset($right_image) && !empty($right_image) ) 
          {
              $fileName = time().'---.'.$right_image->getClientOriginalExtension();  
              $right_image->move('uploads/banner', $fileName);
              $uploded_file_name = 'uploads/banner/'.$fileName;
              $postData['right_image'] = $uploded_file_name ; 
              
               if(isset($post['old_right_image']) && !empty($post['old_right_image'])) 
               {
                  \File::delete($post['old_right_image']);
               }

          }
          else if(isset($post['old_right_image']) && !empty($post['old_right_image'])) 
          {
            $postData['right_image'] = $post['old_right_image'] ; 
          }

          $right_top_image = $request->file('right_top_image'); 
          if(isset($right_top_image) && !empty($right_top_image) ) 
          {
              $fileName = time().'----.'.$right_top_image->getClientOriginalExtension();  
              $right_top_image->move('uploads/banner', $fileName);
              $uploded_file_name = 'uploads/banner/'.$fileName;
              $postData['right_top_image'] = $uploded_file_name ; 
              
               if(isset($post['old_right_top_image']) && !empty($post['old_right_top_image'])) 
               {
                  \File::delete($post['old_right_top_image']);
               }

          }
          else if(isset($post['old_right_top_image']) && !empty($post['old_right_top_image'])) 
          {
            $postData['right_top_image'] = $post['old_right_top_image'] ; 
          }

          $BannerData =  array(
                    'meta_type'=>config('constants.meta_type.banner_data'),
                    'meta_title'=> 'Banner Data',
                    'meta_data'=>json_encode($postData),
            );
          $res = MetaData::update_Or_insert($BannerData,$id);
          
        
           if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Banner updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Banner added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }
         
 
      return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function delete($id)
    {  
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = Banner::update_Or_insert($postData,$id);

         if($res){  
                    //////// ****************** Log Activity ****************///////////////////
                    $extra_data = ['server_data'=>$_SERVER,'session_data'=>Session::all()];
                    $log_data = [
                                'user_id'              => \Auth::user()->id,
                                'item_id'              => $id,
                                'item_type'            => config('constants.activity_item_type.banner'), 
                                'log_title'            =>  'Banner Deleted',
                                'log_text'             => ' Banner Deleted #'.$id ,
                                'extra_data'            => $extra_data,
                                'ip_address'            => get_client_ip_server(),
                                ]; 
                    ActivityLog::create_log($log_data);
                    //////////////// ********************************///////////////////

                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Banner deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }


}
