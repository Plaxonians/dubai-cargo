<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\MetaData;

class ServicesController extends Controller
{
    //

     public function residential(Request $request)
    {   

    	 $service_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_residential_page'));
    	
        return view('admin.services.residential',compact('service_data'));
    }

     public function updateresidential(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                    'detail' => 'required',
                    //'features[]' => 'required',
                ];

      if(!$id || !empty($request->file('image'))){
           $validation['image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=150' ;
         }

     $customMessages = [
                      'image.dimensions' => 'Select :attribute  must be at least min 160x150 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'detail'=>isset($post['detail'])? $post['detail'] : '',
                    'features'=>isset($post['features'])? $post['features'] : '',
            );

        $image = $request->file('image'); 
        if(isset($image) && !empty($image) ) {

              $fileName = time().'-.'.$image->getClientOriginalExtension();  
              $image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['image'] =  $uploded_file_name ;
               if(isset($post['old_image']) && !empty($post['old_image'])) {
                    \File::delete($post['old_image']);
               }
               
        } else {
              if(isset($post['old_image']) && !empty($post['old_image'])){
                $postData['image'] =  $post['old_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_residential_page'),
                    'meta_title'=> get_services_name(1),
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(1).' updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(1).' save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/residential')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.residential')->with($flush_data['key'], $flush_data['msg']);
    

    }

     public function office(Request $request)
    {   

       $service_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_office_page'));
      
        return view('admin.services.office',compact('service_data'));
    }

     public function updateoffice(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                    'detail' => 'required',
                    //'features[]' => 'required',
                ];

      if(!$id || !empty($request->file('image'))){
           $validation['image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=140' ;
         }

     $customMessages = [
                      'image.dimensions' => 'Select :attribute  must be at least min 160x140 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'detail'=>isset($post['detail'])? $post['detail'] : '',
                    'features'=>isset($post['features'])? $post['features'] : '',
            );

        $image = $request->file('image'); 
        if(isset($image) && !empty($image) ) {

              $fileName = time().'-.'.$image->getClientOriginalExtension();  
              $image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['image'] =  $uploded_file_name ;
               if(isset($post['old_image']) && !empty($post['old_image'])) {
                    \File::delete($post['old_image']);
               }
               
        } else {
              if(isset($post['old_image']) && !empty($post['old_image'])){
                $postData['image'] =  $post['old_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_office_page'),
                    'meta_title'=> get_services_name(1),
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(2).' updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(2).' save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/office')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.office')->with($flush_data['key'], $flush_data['msg']);
    

    }

     public function furniture(Request $request)
    {   

       $service_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_furniture_page'));
      
        return view('admin.services.furniture',compact('service_data'));
    }

     public function updatefurniture(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                    'detail' => 'required',
                    //'features[]' => 'required',
                ];

      if(!$id || !empty($request->file('image'))){
           $validation['image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=140' ;
         }

     $customMessages = [
                      'image.dimensions' => 'Select :attribute  must be at least min 160x140 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'detail'=>isset($post['detail'])? $post['detail'] : '',
                    'features'=>isset($post['features'])? $post['features'] : '',
            );

        $image = $request->file('image'); 
        if(isset($image) && !empty($image) ) {

              $fileName = time().'-.'.$image->getClientOriginalExtension();  
              $image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['image'] =  $uploded_file_name ;
               if(isset($post['old_image']) && !empty($post['old_image'])) {
                    \File::delete($post['old_image']);
               }
               
        } else {
              if(isset($post['old_image']) && !empty($post['old_image'])){
                $postData['image'] =  $post['old_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_furniture_page'),
                    'meta_title'=> get_services_name(1),
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(3).' updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(3).' save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/furniture')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.furniture')->with($flush_data['key'], $flush_data['msg']);
    

    }

     public function storage(Request $request)
    {   

       $service_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_storage_page'));
      
        return view('admin.services.storage',compact('service_data'));
    }

     public function updatestorage(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                    'detail' => 'required',
                    //'features[]' => 'required',
                ];

      if(!$id || !empty($request->file('image'))){
           $validation['image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=140' ;
         }

     $customMessages = [
                      'image.dimensions' => 'Select :attribute  must be at least min 160x140 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'detail'=>isset($post['detail'])? $post['detail'] : '',
                    'features'=>isset($post['features'])? $post['features'] : '',
            );

        $image = $request->file('image'); 
        if(isset($image) && !empty($image) ) {

              $fileName = time().'-.'.$image->getClientOriginalExtension();  
              $image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['image'] =  $uploded_file_name ;
               if(isset($post['old_image']) && !empty($post['old_image'])) {
                    \File::delete($post['old_image']);
               }
               
        } else {
              if(isset($post['old_image']) && !empty($post['old_image'])){
                $postData['image'] =  $post['old_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_storage_page'),
                    'meta_title'=> get_services_name(1),
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(4).' updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(4).' save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/storage')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.storage')->with($flush_data['key'], $flush_data['msg']);
    

    }

 public function frieghtcargo(Request $request)
    {   

       $service_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_frieghtcargo_page'));
      
        return view('admin.services.frieghtcargo',compact('service_data'));
    }

     public function updatefrieghtcargo(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
      $flush_data =  array('key'=>'','msg'=>'') ;
      
      $validation = [
                    'title' => 'required',
                    'sub_title' => 'required',
                    'detail' => 'required',
                    //'features[]' => 'required',
                ];

      if(!$id || !empty($request->file('image'))){
           $validation['image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=140' ;
         }

     $customMessages = [
                      'image.dimensions' => 'Select :attribute  must be at least min 160x140 pixels.',
                  ];
          
      $request->validate($validation,$customMessages); 

 
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'detail'=>isset($post['detail'])? $post['detail'] : '',
                    'features'=>isset($post['features'])? $post['features'] : '',
            );

        $image = $request->file('image'); 
        if(isset($image) && !empty($image) ) {

              $fileName = time().'-.'.$image->getClientOriginalExtension();  
              $image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['image'] =  $uploded_file_name ;
               if(isset($post['old_image']) && !empty($post['old_image'])) {
                    \File::delete($post['old_image']);
               }
               
        } else {
              if(isset($post['old_image']) && !empty($post['old_image'])){
                $postData['image'] =  $post['old_image'] ;
              }
        }

         $metaData =  array(
                    'meta_type'=>config('constants.meta_type.service_frieghtcargo_page'),
                    'meta_title'=> get_services_name(1),
                    'meta_data'=>json_encode($postData),
         );
           
         //pad($postData);
        $res = MetaData::update_Or_insert($metaData,$id);
   
            
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(5).' updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = get_services_name(5).' save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/services/frieghtcargo')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.services.frieghtcargo')->with($flush_data['key'], $flush_data['msg']);
    

    }

    

}
