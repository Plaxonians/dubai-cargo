<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\MetaData;
use App\Banner;

class DriverController extends Controller
{
    //

     public function index(Request $request)
    {   
     
    	$driver_data = MetaData::get_data_by_meta_type(config('constants.meta_type.red_driver_page'));

    	$image_data = Banner::get_banner_data(config('constants.banner_type.red_driver_banner'));
    	$sr = 1 ;
        return view('admin.driver.driver',compact('driver_data','image_data','sr'));
    }

     public function update(Request $request)
    {


    $post   = $request->all();
    $meta_id     =  isset($post['meta_id']) && !empty($post['meta_id']) ? $post['meta_id'] : 0;
    $gallery_id  =  isset($post['gallery_id']) && !empty($post['gallery_id']) ? $post['gallery_id'] : 0;

      
      $validation = [
                    'section_one_title' => 'required',
                    'section_one_first_paragraph' => 'required',
                    'section_one_second_paragraph' => 'required',
                    'section_two_title' => 'required',
                    'facilities_first_title' => 'required',
                    'facilities_first_detail' => 'required',
                    'facilities_second_title' => 'required',
                    'facilities_second_detail' => 'required',
                    'facilities_third_title' => 'required',
                    'facilities_third_detail' => 'required',
                    'facilities_four_title' => 'required',
                    'facilities_four_detail' => 'required',
                    'facilities_five_title' => 'required',
                    'facilities_five_detail' => 'required',
                    'facilities_six_title' => 'required',
                    'facilities_six_detail' => 'required',
                    'section_four_title' => 'required',
                    'section_four_title' => 'required',
                    'section_four_first_paragraph' => 'required',
                    'section_four_second_paragraph' => 'required',
                    'section_four_first_point' => 'required',
                    'section_four_second_point' => 'required',
                    'section_four_third_point' => 'required',
                ];
                
        if(!$meta_id || !empty($request->file('facilities_first_image'))){
           $validation['facilities_first_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        if(!$meta_id || !empty($request->file('facilities_second_image'))){
           $validation['facilities_second_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        if(!$meta_id || !empty($request->file('facilities_third_image'))){
           $validation['facilities_third_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        if(!$meta_id || !empty($request->file('facilities_four_image'))){
           $validation['facilities_four_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        if(!$meta_id || !empty($request->file('facilities_five_image'))){
           $validation['facilities_five_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        if(!$meta_id || !empty($request->file('facilities_six_image'))){
           $validation['facilities_six_image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150' ;
        }
        
         $customMessages = [
                            'facilities_first_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                            'facilities_second_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                            'facilities_third_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                            'facilities_four_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                            'facilities_five_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                            'facilities_six_image.dimensions' => 'Select :attribute image must be at least min 150x150 pixels.',
                           ];
      
      $request->validate($validation);

        $postData =  array(
                    'section_one_title'=>isset($post['section_one_title'])? $post['section_one_title'] : '',
                    'section_one_first_paragraph'=>isset($post['section_one_first_paragraph'])? $post['section_one_first_paragraph'] : '',
                    'section_one_second_paragraph'=>isset($post['section_one_second_paragraph'])? $post['section_one_second_paragraph'] : '',
                    'section_two_title'=>isset($post['section_two_title'])? $post['section_two_title'] : '',
                    'facilities_first_title'=>isset($post['facilities_first_title'])? $post['facilities_first_title'] : '',
                    'facilities_first_detail'=>isset($post['facilities_first_detail'])? $post['facilities_first_detail'] : '',
                    'facilities_second_title'=>isset($post['facilities_second_title'])? $post['facilities_second_title'] : '',
                    'facilities_second_detail'=>isset($post['facilities_second_detail'])? $post['facilities_second_detail'] : '',
                    'facilities_third_title'=>isset($post['facilities_third_title'])? $post['facilities_third_title'] : '',
                    'facilities_third_detail'=>isset($post['facilities_third_detail'])? $post['facilities_third_detail'] : '',
                    'facilities_four_title'=>isset($post['facilities_four_title'])? $post['facilities_four_title'] : '',
                    'facilities_four_detail'=>isset($post['facilities_four_detail'])? $post['facilities_four_detail'] : '',
                    'facilities_five_title'=>isset($post['facilities_five_title'])? $post['facilities_five_title'] : '',
                    'facilities_five_detail'=>isset($post['facilities_five_detail'])? $post['facilities_five_detail'] : '',
                    'facilities_six_title'=>isset($post['facilities_six_title'])? $post['facilities_six_title'] : '',
                    'facilities_six_detail'=>isset($post['facilities_six_detail'])? $post['facilities_six_detail'] : '',
                    'section_four_title'=>isset($post['section_four_title'])? $post['section_four_title'] : '',
                    'section_four_first_paragraph'=>isset($post['section_four_first_paragraph'])? $post['section_four_first_paragraph'] : '',
                    'section_four_second_paragraph'=>isset($post['section_four_second_paragraph'])? $post['section_four_second_paragraph'] : '',
                    'section_four_first_point'=>isset($post['section_four_first_point'])? $post['section_four_first_point'] : '',
                    'section_four_second_point'=>isset($post['section_four_second_point'])? $post['section_four_second_point'] : '',
                    'section_four_third_point'=>isset($post['section_four_third_point'])? $post['section_four_third_point'] : '',
                    'section_four_four_point'=>isset($post['section_four_four_point'])? $post['section_four_four_point'] : '',
                    'section_four_five_point'=>isset($post['section_four_five_point'])? $post['section_four_five_point'] : '',
                     );
        $facilities_first_image = $request->file('facilities_first_image'); 
        if(isset($facilities_first_image) && !empty($facilities_first_image) ) {

              $fileName = time().'-.'.$facilities_first_image->getClientOriginalExtension();  
              $facilities_first_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_first_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_first_image']) && !empty($post['old_facilities_first_image'])) {
                    \File::delete($post['old_facilities_first_image']);
               }

        } else {
              if(isset($post['old_facilities_first_image']) && !empty($post['old_facilities_first_image'])){
                $postData['facilities_first_image'] =  $post['old_facilities_first_image'] ;
              }
        }
        $facilities_second_image = $request->file('facilities_second_image'); 
        if(isset($facilities_second_image) && !empty($facilities_second_image) ) {

              $fileName = time().'--.'.$facilities_second_image->getClientOriginalExtension();  
              $facilities_second_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_second_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_second_image']) && !empty($post['old_facilities_second_image'])) {
                    \File::delete($post['old_facilities_second_image']);
               }
               
        } else {
              if(isset($post['old_facilities_second_image']) && !empty($post['old_facilities_second_image'])){
                $postData['facilities_second_image'] =  $post['old_facilities_second_image'] ;
              }
        }
        $facilities_third_image = $request->file('facilities_third_image'); 
        if(isset($facilities_third_image) && !empty($facilities_third_image) ) {

              $fileName = time().'---.'.$facilities_third_image->getClientOriginalExtension();  
              $facilities_third_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_third_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_third_image']) && !empty($post['old_facilities_third_image'])) {
                    \File::delete($post['old_facilities_third_image']);
               }
               
        } else {
              if(isset($post['old_facilities_third_image']) && !empty($post['old_facilities_third_image'])){
                $postData['facilities_third_image'] =  $post['old_facilities_third_image'] ;
              }
        }
        $facilities_four_image = $request->file('facilities_four_image'); 
        if(isset($facilities_four_image) && !empty($facilities_four_image) ) {

              $fileName = time().'----.'.$facilities_four_image->getClientOriginalExtension();  
              $facilities_four_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_four_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_four_image']) && !empty($post['old_facilities_four_image'])) {
                    \File::delete($post['old_facilities_four_image']);
               }
               
        } else {
              if(isset($post['old_facilities_four_image']) && !empty($post['old_facilities_four_image'])){
                $postData['facilities_four_image'] =  $post['old_facilities_four_image'] ;
              }
        }
        $facilities_five_image = $request->file('facilities_five_image'); 
        if(isset($facilities_five_image) && !empty($facilities_five_image) ) {

              $fileName = time().'-----.'.$facilities_five_image->getClientOriginalExtension();  
              $facilities_five_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_five_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_five_image']) && !empty($post['old_facilities_five_image'])) {
                    \File::delete($post['old_facilities_five_image']);
               }
               
        } else {
              if(isset($post['old_facilities_five_image']) && !empty($post['old_facilities_five_image'])){
                $postData['facilities_five_image'] =  $post['old_facilities_five_image'] ;
              }
        }
        $facilities_six_image = $request->file('facilities_six_image'); 
        if(isset($facilities_six_image) && !empty($facilities_six_image) ) {

              $fileName = time().'------.'.$facilities_six_image->getClientOriginalExtension();  
              $facilities_six_image->move('uploads/facilities', $fileName);
              $uploded_file_name = 'uploads/facilities/'.$fileName;
              $postData['facilities_six_image'] =  $uploded_file_name ;
               if(isset($post['old_facilities_six_image']) && !empty($post['old_facilities_six_image'])) {
                    \File::delete($post['old_facilities_six_image']);
               }
               
        } else {
              if(isset($post['old_facilities_six_image']) && !empty($post['old_facilities_six_image'])){
                $postData['facilities_six_image'] =  $post['old_facilities_six_image'] ;
              }
        }
          

        $metaData =  array(
                    'meta_type'=>config('constants.meta_type.red_driver_page'),
                    'meta_title'=> 'Red Trucking Driver  ',
                    'meta_data'=>json_encode($postData),
            );

        $res = MetaData::update_Or_insert($metaData,$meta_id);

        $page_gallery = isset($post['page_gallery']) && sizeof($post['page_gallery']) ? $post['page_gallery'] : array() ; 
        
        foreach ($page_gallery['title'] as $key => $value) {

          $gallery_id = isset($page_gallery['gallery_id'][$key]) && !empty($page_gallery['gallery_id'][$key]) ? $page_gallery['gallery_id'][$key] : 0 ;

          $images = $request->file("page_gallery.image.$key"); 

          $gallery_data = [
            'title'=>isset($page_gallery['title'][$key]) ? $page_gallery['title'][$key] : 0 ,
            'details'=>isset($page_gallery['details'][$key]) ? $page_gallery['details'][$key] : '' ,
            'type'=>config('constants.banner_type.red_driver_banner'),
            ];

          if(isset($images) && !empty($images) && $res ) {
              $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/driverbanner', $fileName);
              $uploded_file_name = 'uploads/driverbanner/'.$fileName;
              $gallery_data['image'] = $uploded_file_name ;
          }

          $gal_id  = Banner::update_Or_insert($gallery_data,$gallery_id);
          
        }

        

     
   
            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($meta_id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Red Driver updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Red Driver  save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/driver')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.driver.index')->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function page_gallery_delete($id)
    {  
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = PageGallery::update_Or_insert($postData,$id);

         if($res){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Banner deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }


}
