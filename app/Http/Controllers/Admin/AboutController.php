<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\MetaData;
use App\CompanyMembers;

class AboutController   extends Controller
{
    //

   

    

    public function company_profile(Request $request)
    {   

       
       $company_profile_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_company_profile'));
      
        return view('admin.about.company_profile',compact('company_profile_data'));
    }

    public function update_company_profile(Request $request)
    {
      
      $validation  =
                      [
                          'title' => 'required|max:200',
                          'first_content' => 'required',
                          'right_content' => 'required',
                          'bottom_content' => 'required',
                          'our_mission_title' => 'required',
                          'our_mission_first_content' => 'required',
                          'our_mission_second_content' => 'required',
                          'our_mission_third_content' => 'required',
                      ];

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

      
       if(!$id || !empty($request->file('left_image'))){
        $validation['left_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=575,min_height=386' ;
       }

        $customMessages = [
                            'left_image.dimensions' => 'Select :attribute image must be at least min 575X386 pixels.',
                        ];
          
       $request->validate($validation,$customMessages); 

             
            
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'first_content'=>isset($post['first_content'])? $post['first_content'] : '',
                    'right_content'=>isset($post['right_content'])? $post['right_content'] : '',
                    'bottom_content'=>isset($post['bottom_content'])? $post['bottom_content'] : '',
                    'our_mission_title'=>isset($post['our_mission_title'])? $post['our_mission_title'] : '',
                    'our_mission_first_content'=>isset($post['our_mission_first_content'])? $post['our_mission_first_content'] : '',
                    'our_mission_second_content'=>isset($post['our_mission_second_content'])? $post['our_mission_second_content'] : '',
                    'our_mission_third_content'=>isset($post['our_mission_third_content'])? $post['our_mission_third_content'] : '',
            );

          $left_image = $request->file('left_image'); 

          $left_image = $request->file('left_image'); 
          
          if(isset($left_image) && !empty($left_image) ) {

              $fileName = time().'right.'.$left_image->getClientOriginalExtension();  
              $left_image->move('uploads/aboutus', $fileName);
              $uploded_file_name = 'uploads/aboutus/'.$fileName;
              $postData['left_image'] =  $uploded_file_name ;

               if(isset($post['old_left_image']) && !empty($post['old_left_image'])) {
                    \File::delete($post['old_left_image']);
               }

          } else {
              if(isset($post['old_left_image']) && !empty($post['old_left_image'])){
                $postData['left_image'] =  $post['old_left_image'] ;
              }

            }
          if(isset($post['profile_points']) && sizeof($post['profile_points']))
          {
             $postData['profile_points'] = $post['profile_points'];
          }
            
          $metaData =  array(
                    'meta_type'=>config('constants.meta_type.about_us_company_profile'),
                    'meta_title'=> 'Company Profile',
                    'meta_data'=>json_encode($postData),
            );
            $res = MetaData::update_Or_insert($metaData,$id);

            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Company Profile updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Company Profile saved successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

          


 
        return $res ? redirect('admin/about/companyprofile')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.companyprofile')->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function client_review(Request $request)
    {   

       
       $about_us_client_review_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_client_review'));
      
        return view('admin.about.client_review',compact('about_us_client_review_data'));
    }

    public function update_client_review(Request $request)
    {
      
      $validation  =
                      [
                          'title' => 'required|max:100',
                          'sub_title' => 'required',
                          'review_content' => 'required',
                          'feedback_title' => 'required',
                          'feedback_content' => 'required',
                      ];

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

      
      
          
       $request->validate($validation); 

             
            
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sub_title'=>isset($post['sub_title'])? $post['sub_title'] : '',
                    'review_content'=>isset($post['review_content'])? $post['review_content'] : '',
                    'feedback_title'=>isset($post['feedback_title'])? $post['feedback_title'] : '',
                    'feedback_content'=>isset($post['feedback_content'])? $post['feedback_content'] : '',
            );

          
            
          $metaData =  array(
                    'meta_type'=>config('constants.meta_type.about_us_client_review'),
                    'meta_title'=> 'About us  Client Review ',
                    'meta_data'=>json_encode($postData),
            );
            $res = MetaData::update_Or_insert($metaData,$id);

            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Client Review updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Client Review saved successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

          


 
        return $res ? redirect('admin/about/clientreview')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.clientreview')->with($flush_data['key'], $flush_data['msg']);
    

    }


    
}
