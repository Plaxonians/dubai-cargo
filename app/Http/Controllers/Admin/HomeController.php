<?php

namespace App\Http\Controllers\Admin;

use App\Form;

class HomeController
{
    public function index()
    { 

    	 $contact_form_data = Form::getAllFormtData(config('constants.form_type.contact'));
    	 $total_contact = $contact_form_data->count();
    	 $career_form_data = Form::getAllFormtData(config('constants.form_type.quote'));
    	 $total_career = $career_form_data->count();
    	 $feedback_form_data = Form::getAllFormtData(config('constants.form_type.feedback'));
    	 $total_feedback = $feedback_form_data->count();
         $get_quote_form_data = Form::getAllFormtData(config('constants.form_type.get_quote'));
         $total_get_quote = $get_quote_form_data->count();
        return view('admin.home.index',compact('total_contact','total_career','total_feedback','total_get_quote'));
    }
}
