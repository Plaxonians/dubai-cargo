<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\HappyClients;
use App\Testimonials;



class HomePageController   extends Controller
{
    //
    public function index(Request $request)
    {   
      $home_page_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.home_page_content'));

      

    	  return view('admin.home_page.index',compact('home_page_meta_data'));
    }



     public function update(Request $request)
    {

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

      $validation =  array(
                    'service_title' => 'required',
                    'service_content' => 'required|max:200',
                    'first_service' => 'required',
                    'second_service' => 'required',
                    'third_service' => 'required',
                    'fourth_service' => 'required',
                    'fifth_service' => 'required',
                    'about_us_title' => 'required',
                    'about_us_content' => 'required',
                     ) ;

      if(!$id || !empty($request->file('first_service_image')))
        {
           $validation['first_service_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=160' ;
        }

      if(!$id || !empty($request->file('second_service_image')))
        {
           $validation['second_service_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=143' ;
        }

      if(!$id || !empty($request->file('third_service_image')))
        {
           $validation['third_service_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=140' ;
        }

      if(!$id || !empty($request->file('fourth_service_image')))
        {
           $validation['fourth_service_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=150' ;
        }

      if(!$id || !empty($request->file('fifth_service_image')))
        {
           $validation['fifth_service_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=160,min_height=160' ;
        }

      if(!$id || !empty($request->file('about_us_image')))
        {
           $validation['about_us_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=575,min_height=386' ;
        }

       $customMessages = [
                            'first_service_image.dimensions' => 'Select :attribute  must be at least min 160x160 pixels.',
                            'second_service_image.dimensions' => 'Select :attribute  must be at least min 160x143 pixels.',
                            'third_service_image.dimensions' => 'Select :attribute  must be at least min 160x160 pixels.',
                            'fourth_service_image.dimensions' => 'Select :attribute  must be at least min 160x156 pixels.',
                            'fifth_service_image.dimensions' => 'Select :attribute  must be at least min 160x160 pixels.',
                            'about_us_image.dimensions' => 'Select :attribute  must be at least min 575x386 pixels.',
                        ];


        $request->validate($validation,$customMessages);


        $postData =  array(
                    'service_title'=>isset($post['service_title'])? $post['service_title'] : '',
                    'service_content'=>isset($post['service_content'])? $post['service_content'] : '',
                    'first_service'=>isset($post['first_service'])? $post['first_service'] : '',
                    'second_service'=>isset($post['second_service'])? $post['second_service'] : '',
                    'third_service'=>isset($post['third_service'])? $post['third_service'] : '',
                    'fourth_service'=>isset($post['fourth_service'])? $post['fourth_service'] : '',
                    'fifth_service'=>isset($post['fifth_service'])? $post['fifth_service'] : '',
                    'about_us_title'=>isset($post['about_us_title'])? $post['about_us_title'] : '',
                    'about_us_content'=>isset($post['about_us_content'])? $post['about_us_content'] : '',
                 );


          $first_service_image = $request->file('first_service_image'); 
          if(isset($first_service_image) && !empty($first_service_image) ) 
          {
              $fileName = time().'-.'.$first_service_image->getClientOriginalExtension();  
              $first_service_image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['first_service_image'] = $uploded_file_name ; 
              
               if(isset($post['old_first_service_image']) && !empty($post['old_first_service_image'])) 
               {
                  \File::delete($post['old_first_service_image']);
               }

          }
          else if(isset($post['old_first_service_image']) && !empty($post['old_first_service_image'])) 
          {
            $postData['first_service_image'] = $post['old_first_service_image'] ; 
          }

          $second_service_image = $request->file('second_service_image'); 
          if(isset($second_service_image) && !empty($second_service_image) ) 
          {
              $fileName = time().'--.'.$second_service_image->getClientOriginalExtension();  
              $second_service_image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['second_service_image'] = $uploded_file_name ; 
              
               if(isset($post['old_second_service_image']) && !empty($post['old_second_service_image'])) 
               {
                  \File::delete($post['old_second_service_image']);
               }

          }
          else if(isset($post['old_second_service_image']) && !empty($post['old_second_service_image'])) 
          {
            $postData['second_service_image'] = $post['old_second_service_image'] ; 
          }

          $third_service_image = $request->file('third_service_image'); 
          if(isset($third_service_image) && !empty($third_service_image) ) 
          {
              $fileName = time().'---.'.$third_service_image->getClientOriginalExtension();  
              $third_service_image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['third_service_image'] = $uploded_file_name ; 
              
               if(isset($post['old_third_service_image']) && !empty($post['old_third_service_image'])) 
               {
                  \File::delete($post['old_third_service_image']);
               }

          }
          else if(isset($post['old_third_service_image']) && !empty($post['old_third_service_image'])) 
          {
            $postData['third_service_image'] = $post['old_third_service_image'] ; 
          }

          $fourth_service_image = $request->file('fourth_service_image'); 
          if(isset($fourth_service_image) && !empty($fourth_service_image) ) 
          {
              $fileName = time().'----.'.$fourth_service_image->getClientOriginalExtension();  
              $fourth_service_image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['fourth_service_image'] = $uploded_file_name ; 
              
               if(isset($post['old_fourth_service_image']) && !empty($post['old_fourth_service_image'])) 
               {
                  \File::delete($post['old_fourth_service_image']);
               }

          }
          else if(isset($post['old_fourth_service_image']) && !empty($post['old_fourth_service_image'])) 
          {
            $postData['fourth_service_image'] = $post['old_fourth_service_image'] ; 
          }

          $fifth_service_image = $request->file('fifth_service_image'); 
          if(isset($fifth_service_image) && !empty($fifth_service_image) ) 
          {
              $fileName = time().'-----.'.$fifth_service_image->getClientOriginalExtension();  
              $fifth_service_image->move('uploads/services', $fileName);
              $uploded_file_name = 'uploads/services/'.$fileName;
              $postData['fifth_service_image'] = $uploded_file_name ; 
              
               if(isset($post['old_fifth_service_image']) && !empty($post['old_fifth_service_image'])) 
               {
                  \File::delete($post['old_fifth_service_image']);
               }

          }
          else if(isset($post['old_fifth_service_image']) && !empty($post['old_fifth_service_image'])) 
          {
            $postData['fifth_service_image'] = $post['old_fifth_service_image'] ; 
          }

          $about_us_image = $request->file('about_us_image'); 
          if(isset($about_us_image) && !empty($about_us_image) ) 
          {
              $fileName = time().'aboutus.'.$about_us_image->getClientOriginalExtension();  
              $about_us_image->move('uploads/aboutus', $fileName);
              $uploded_file_name = 'uploads/aboutus/'.$fileName;
              $postData['about_us_image'] = $uploded_file_name ; 
              
               if(isset($post['old_about_us_image']) && !empty($post['old_about_us_image'])) 
               {
                  \File::delete($post['old_about_us_image']);
               }

          }
          else if(isset($post['old_about_us_image']) && !empty($post['old_about_us_image'])) 
          {
            $postData['about_us_image'] = $post['old_about_us_image'] ; 
          }
     
       $HomepageData =  array(
                    'meta_type'=>config('constants.meta_type.home_page_content'),
                    'meta_title'=> 'Home Page',
                    'meta_data'=>json_encode($postData),
            );
          $res = MetaData::update_Or_insert($HomepageData,$id);
          
        
           if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Home page updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Home page added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }
 
        return $res ? redirect('admin/homepage')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.index')->with($flush_data['key'], $flush_data['msg']);
    

    }

   

    







    
}
