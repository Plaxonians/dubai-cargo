<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends Controller
{
    //

     public function add(Request $request)
    {   

        return view('admin.gallery.add');
    }

     public function edit($id)
    {   
       
       $data = Blog::get_data_by_id($id);

        return view('admin.gallery.add',compact('data'));
    }

    public function index(Request $request)
    {   

      $gallery_data = Gallery::getAllGalleryData();
      return view('admin.gallery.index',compact('gallery_data'));
    }

    public function update(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
    	$post   = $request->all();
      $gallery_data = [] ;

      $gallery_image = isset($post['gallery']['gallery_image']) && sizeof($post['gallery']['gallery_image']) ? $post['gallery']['gallery_image'] : array() ; 

      foreach ($gallery_image as $key => $value) {

         $gal_id = isset($post['gallery']['gal_id'][$key]) && !empty($post['gallery']['gal_id'][$key]) ? $post['gallery']['gal_id'][$key] : 0 ;
         $upload_gallery_image = '' ;

        $images = $request->file("gallery.gallery_image.$key"); 
        if(isset($images) && !empty($images)) {
              $fileName = time().rand().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/gallery', $fileName);
              $uploded_file_name = 'uploads/gallery/'.$fileName;
              $upload_gallery_image = $uploded_file_name ;
          }

           $res  = Gallery::update_Or_insert(['image'=>$upload_gallery_image],$gal_id);

      }
        

        
            $flush_data['key'] = 'success' ;
            $flush_data['msg'] = 'Gallery saved successfully' ;
                
           

        return $res ? redirect('admin/gallery')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }

     public function delete($id)
    {   
      
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = Gallery::update_Or_insert($postData,$id);

         if($res){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Gallery deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }

}
