<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Blog;
use App\Testimonials;
class BlogController   extends Controller
{
    //

    public function index(Request $request)
    {   
     $page_title     = 'Blogs' ;
     $blog_data       = Blog::getAllBlogData(12);
     $all_testimonials = Testimonials::getAllTestimonialsData();

     $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'blogs/style.css',
                                 'blogs/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                   //'blogs/script.js',
                                 ],
                    'blog_data'=> $blog_data,
                    'all_testimonials'=> $all_testimonials,
                    ];
     //pa($view_data);

     return view('frontend.blog.index',$view_data);

    }
    public function detail($blog_url)
    {   
      $page_title     = 'Blog Details' ;
      $blog_data = Blog::get_data_by_blog_url($blog_url);
      $id = isset($blog_data->id) ? $blog_data->id :  0 ; 
      if(!Blog::is_exists($id)){
       	 return redirect()->back();
       }
       $recent_blog_data = Blog::whereNotIn('id', [$id])->where('is_deleted',0)->where('status',1)->orderBy('id', 'asc')->take(2)->get();


       $view_data = [
                    'page_title' => $page_title,
                    'top_css'=> [
                                 'blogs/style.css',
                                 'blogs/responsive.css',
                                ],
                    'top_js'=>  [
                                ],
                    'bottom_js'=>[
                                   //'blogs/script.js',
                                 ],
                    'recent_blog_data'=> $recent_blog_data,
                    'blog_data'=> $blog_data,
                    ];
     //pa($view_data);


     return view('frontend.blog.details',$view_data);

    }
    
}
